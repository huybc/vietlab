﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class RedirectBlogList2ViewComponent : ViewComponent
    {
        private readonly IArticleDao _articleDao;

        public RedirectBlogList2ViewComponent(IArticleDao articleDao)
        {
            _articleDao = articleDao;
        }

        public async Task<IViewComponentResult> InvokeAsync(ZoneDetail zone, List<ArticleMinify> lstArticle, int total, int? pageIndex, int? pageSize)
        {
            if (zone != null)
            {
                pageIndex = pageIndex ?? 1;
                pageSize = pageSize ?? 15;
                //Get ra zone by Id
                ViewBag.PageIndex = pageIndex;
                ViewBag.PageSize = pageSize;
                ViewBag.Total = total;
                var zoneId = zone.Id;
                if (zoneId > 0)
                {
                    var zone_target = zone;
                    if (zone_target != null)
                    {

                        ViewBag.ZoneTarget = zone_target;
                    }
                }
                return View(lstArticle);
            }
            return null;
        }
    }
}