﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class RedirectBlogList1ViewComponent : ViewComponent
    {
        private readonly IArticleDao _articleDao;

        public RedirectBlogList1ViewComponent(IArticleDao articleDao)
        {
            _articleDao = articleDao;
        }


        public async Task<IViewComponentResult> InvokeAsync(List<ZoneByTreeViewMinify> zones, int id)
        {
            if (zones != null && id > 0)
            {
                var zone_id = id;
                var list_zone_blog = zones;
                var zone_blog_parent = list_zone_blog.Where(r => (r.ParentId == 0 && r.Id == zone_id)
                                                            || (r.ParentId > 0 && r.Id == zone_id)).FirstOrDefault();
                if (zone_blog_parent != null)
                {
                    ViewBag.ZoneParent = zone_blog_parent;
                    var list_blog_childs = list_zone_blog.Where(r => r.ParentId == zone_blog_parent.Id).ToList();
                    if (list_blog_childs != null)
                        ViewBag.ZoneChilds = list_blog_childs;
                    else
                        ViewBag.ZoneChilds = new List<ZoneByTreeViewMinify>();
                }
                return View();
            }
            return null;
        }
    }
}