﻿using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Enterbuy.Services.Common.Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System;
using Enterbuy.Services.Blog.Services;

namespace Enterbuy.Services.Blog.Controllers
{
    public class BlogController : BaseController
    {
        private readonly IBlogServices _blogServices;
        private readonly IMenuZoneServices _menuZoneServices;

        public BlogController(IMenuZoneServices menuZoneServices, IBlogServices blogServices)
        {
            _menuZoneServices = menuZoneServices;
            _blogServices = blogServices;
        }
        public async Task<IActionResult> Index(string alias)
        {
            var zone_tar = await _menuZoneServices.GetZoneByAlias(alias, Utils.Utility.DefaultLang);
            ViewData["IsExpand"] = false;
            if (zone_tar != null && zone_tar.Id > 0)
            {
                var list_zone_blog = await _menuZoneServices.GetZoneByTreeViewMinifies((int)TypeZone.Article, Utils.Utility.DefaultLang, 0);
                var zone_blog_parent = list_zone_blog.Where(r => r.ParentId == 0 && r.Id == zone_tar.Id).FirstOrDefault();

                if (zone_blog_parent != null)
                {
                    ViewBag.ZoneParent = zone_blog_parent;
                    var list_blog_childs = list_zone_blog.Where(r => r.ParentId == zone_blog_parent.Id).ToList();
                    if (list_blog_childs != null)
                        ViewBag.ZoneChilds = list_blog_childs;
                    else
                        ViewBag.ZoneChilds = new List<ZoneByTreeViewMinify>();
                }
            }
            else
            {
                return NotFound();
            }

            return View();
        }

        [Route("news/{alias}.html")]
        [Route("news/{alias}/page/{pageIndex}.html")]
        public async Task<IActionResult> RedirectAction(string alias, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 10;
            ViewData["IsExpand"] = false;
            var zone_tar = await _menuZoneServices.GetZoneByAlias(alias, Utils.Utility.DefaultLang);
            if (zone_tar != null)
            {
                if (zone_tar.Url.Equals(alias))
                {
                    ViewBag.ZoneId = zone_tar.Id;
                    ViewBag.Type = zone_tar.Type;
                    ViewBag.Parent = zone_tar.ParentId;
                    ViewBag.PageIndex = pageIndex;
                    ViewBag.PageSize = pageSize;
                    ViewBag.IsHaveChild = zone_tar.isHaveChild;
                    ViewBag.MetaCanonical = zone_tar.MetaCanonical;
                    ViewBag.MetaNoIndex = zone_tar.MetaNoIndex;
                }
                else
                {
                    return Redirect(Utils.BaseBA.UrlCategoryNews(alias));
                }
            }
            else
            {
                return NotFound();
            }
            return View();

        }
        [Route("news/{category}/{alias}-{articleId}.html")]
        public IActionResult RedirectActionOld(string alias, int articleId)
        {
            return RedirectToAction("Details", new { url = alias.ToLower(), articleId = articleId });
        }
        [Route("tin-tuc/{url}-{articleId}.html")]
        public IActionResult Details(string url, int articleId)
        {
            ArticleDetail articleDetail = new ArticleDetail();
            ViewData["IsExpand"] = false;
            
            try
            {
                articleDetail = _blogServices.GetArticleDetail(articleId, Utils.Utility.DefaultLang);
                

                if (articleDetail.Url != url)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {

            }
            ViewBag.articleDetail = articleDetail;
            return View();
        }
        [HttpPost]
        public IActionResult ProductsInArticle(string product_ids, int location_id)
        {
            return ViewComponent("ProductListInArticle", new { product_ids = product_ids, location_id = location_id });
        }
    }
}