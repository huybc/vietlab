﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JanHome.Web.Models;
using JanHome.Web.Services.Zone.Repository;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using JanHome.Web.Services.Locations.Repository;
using JanHome.Web.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using Utils;

namespace JanHome.Web.Controllers
{
    public class HomeController : BaseController
    {
        private IHostingEnvironment _env;
        private readonly IZoneRepository _zoneRepository;
        private readonly ILocationsRepository _locationsRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICookieUtility _cookieUtility;
        public HomeController(IHostingEnvironment envrnmt, IZoneRepository zoneRepository, ILocationsRepository locationsRepository, IHttpContextAccessor httpContextAccessor, ICookieUtility cookieUtility)
        {
            _zoneRepository = zoneRepository;
            _locationsRepository = locationsRepository;
            _cookieUtility = cookieUtility;
            _httpContextAccessor = httpContextAccessor;
            _env = envrnmt;


        }
        [HttpPost]
        public IActionResult SetLanguage(string culture)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return RedirectToAction("IndexPublic");
        }
        public ActionResult RedirectToDefaultCulture()
        {
            var culture = CurrentLanguage;
            if (string.IsNullOrEmpty(culture))
                culture = "vi";

            return RedirectToAction("IndexPublic");
        }
        public IActionResult Index()
        {
           

            return View();
        }

        public IActionResult IndexPublic()
        {

            //Lay IP
            var customer_ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress; ;
            //GetIpValue(out customer_ip);
            //Lay location from IP
            Console.WriteLine(customer_ip);
            //Kiem tra co ton tai cookie location_id khong
            CookieOptions cookie = new CookieOptions() { 
                Path = "/",
                HttpOnly = false,
                Secure = false
            };
            if (Request.Cookies[CookieLocationId] == null) {
                var location_default = _locationsRepository.GetLocationFirst(CurrentLanguageCode);
                if (location_default != null) {
                    cookie.Expires = DateTime.Now.AddDays(7);
                    Response.Cookies.Append(CookieLocationId, location_default.Id.ToString(), cookie);
                    Response.Cookies.Append(CookieLocationName, location_default.Name, cookie);
                    ViewBag.LocationId = location_default.Id;
                    ViewBag.LocationName = location_default.Name;
                }
            }
            //var products = "";
            //var index = 0;
            //foreach (var p in cart.Products)
            //{
            //    index++;
            //     products += string.Format("<tr><td>{0}</td><td>{1}</td><td><a href=\"{2}\" target=\"_blank\">{3}</a></td><td>{4}</td><td>{5}</td></tr>", index, p.Code, "http://janhome.com/product/" + p.Id, p.Title, UIHelper.FormatMoney(p.Price) + " VNĐ", p.Quantity);
            //}
            //var body = Email.GetTemplate(_env.WebRootPath + "\\mail-templates\\Invoice.html");
            //body = body.Replace("{UserName}", "Hoang Hiep");
            //body = body.Replace("{CODE}", "M1221432");
            //body = body.Replace("{TOTAL}", "100.000");
            //body = body.Replace("{METHOD}", "Chuyen Khoan");
            //body = body.Replace("{PRODUCTS}", products);
            //Email.Send("idhiephv@gmail.com", "[Janhome.com] Thông tin đơn hàng " + "M1221432", body);

            return View();
        }

        [HttpPost]
        public IActionResult SwitchRegion(int region_id)
        {
            return ViewComponent("SwitchRegion", new { region_id = region_id });
        }
        [HttpPost]
        public IActionResult ViewMoreRegion(int zone_parent_id, int locationId, int skip, int size)
        {
            return ViewComponent("ViewMoreRegion", new { zone_parent_id = zone_parent_id, locationId = locationId, skip = skip, size = size });
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
