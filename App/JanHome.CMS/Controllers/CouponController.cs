using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Dapper.Data.Models;
using MI.Dapper.Data.Repositories.Interfaces;
using MI.Dapper.Data.ViewModels;
using MI.Entity.Common;
using Microsoft.AspNetCore.Mvc;
using Utils;

namespace JanHome.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CouponController : ControllerBase
    {
        private readonly ICouponRepository _couponRepository;

        public CouponController(ICouponRepository couponRepository)
        {
            _couponRepository = couponRepository;
        }


        [HttpGet("GetAll")]
        public IActionResult GetAll()
        {
            var responseData = new ResponseData();
            try
            {
                var ads = MI.Cache.RamCache.DicCoupon;
                return Ok(ads.Values.ToList());
            }
            catch (Exception ex)
            {
            }
            return Ok(new List<MI.Entity.Models.Coupon>());
        }

        [HttpGet("UnPublish")]
        public async Task<ResponseData> Unpublish(int id)
        {
            var responseData = new ResponseData();
            try
            {
                
                var ads = await _couponRepository.Unpublish(id);
                if (ads)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {
            }

            return responseData;
        }

        [HttpPost("Add")]
        public async Task<ResponseData> Add(CouponViewModel coupon)
        {
            var responseData = new ResponseData();
            try
            {
                MI.Cache.RamCache.LastestLoadCoupon = DateTime.MinValue;
                var ads = await _couponRepository.Create(coupon);
                if (ads)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }

        [HttpPut("Update")]
        public async Task<ResponseData> Update(CouponViewModel coupon)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                MI.Cache.RamCache.LastestLoadCoupon = DateTime.MinValue;
                var ads = await _couponRepository.Update(coupon);
                if (ads)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";
                }
            }
            catch (Exception ex)
            {
            }

            return responseData;
        }

        [HttpGet("GetAllPaging")]
        public async Task<IActionResult> Get([FromQuery] FilterQuery filter)
        {
            try
            {
                var data = await _couponRepository.GetAllPaging(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
            }
            return Ok();
        }

        [HttpGet("GetById")]
        public async Task<IActionResult> GetById(int id)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var ads = await _couponRepository.GetById(id);
                return Ok(ads);
            }
            catch (Exception ex)
            {
            }
            return Ok();
        }
    }
}
