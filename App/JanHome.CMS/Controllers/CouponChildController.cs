using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JanHome.CMS.ViewModel;
using MI.Bo.Bussiness;
using MI.Entity.Models;
using Microsoft.AspNetCore.Mvc;
using Utils;

namespace JanHome.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CouponChildController : ControllerBase
    {
        public CouponChildBCL couponChildBCL;
        public ProductInZoneBCL productInZoneBCL;
        public ProductBCL productBCL;
        public CouponChildController()
        {
            couponChildBCL = new CouponChildBCL();
            productInZoneBCL = new ProductInZoneBCL();
            productBCL = new ProductBCL();
        }
        [HttpPost("GetAllPaging")]
        public IActionResult Get([FromBody] FilterCouponChild filter)
        {
            dynamic result = new System.Dynamic.ExpandoObject();
            try
            {
                int total = 0;
                var data = couponChildBCL.FindAll(filter, out total);
                result.data = data.Select(x => new
                {
                    x.CreateBy,
                    x.CreateDate,
                    x.ExprireDate,
                    x.Ma,
                    x.Name,
                    x.NumberUseCode,
                    x.Parrent,
                    x.Status,
                    Nhom = MI.Cache.RamCache.DicCoupon.ContainsKey(x.Parrent) ? MI.Cache.RamCache.DicCoupon[x.Parrent].Code : "",
                });
                result.totals = total;
                return Ok(result);
            }
            catch (Exception ex)
            {
            }
            return Ok();
        }
        [HttpGet("GetById")]
        public IActionResult GetById(string ma)
        {
            try
            {
                var data = couponChildBCL.FindById(x => x.Ma == ma);
                return Ok(data);
            }
            catch (Exception ex)
            {
            }
            return Ok(new MI.Entity.Models.CouponChild());
        }
        [HttpGet("GetByParrentId")]
        public IActionResult GetByParrentId(int parrentId)
        {
            try
            {
                var data = couponChildBCL.GetByParrentId(parrentId);
                return Ok(data.Select(x => new { id = x.Key, label = x.Key + "-" + x.Value }).ToList());
            }
            catch (Exception ex)
            {
            }
            return Ok(new MI.Entity.Models.CouponChild());
        }
        [HttpPost("ChangeStatus")]
        public IActionResult ChangeStatus([FromBody] CouponChild coupon)
        {
            KeyValuePair<bool, string> Result = new KeyValuePair<bool, string>(false, "Thất bại");
            try
            {
                if (coupon.Status == null)
                    coupon.Status = 1;

                if (!String.IsNullOrEmpty(coupon.Ma))
                {
                    if (coupon.Status != 3)
                    {
                        Result = couponChildBCL.UpdateStatus(coupon);
                    }
                    else
                    {
                        bool isSuccess = couponChildBCL.Remove(coupon);
                        if (isSuccess)
                        {
                            Result = new KeyValuePair<bool, string>(true, "Thành công");
                        }
                        else
                        {
                            Result = new KeyValuePair<bool, string>(false, "Thất bại");
                        }
                    }

                }
                else
                    Result = new KeyValuePair<bool, string>(false, "Không được để trống mã");



                return Ok(Result);
            }
            catch (Exception ex)
            {
            }
            return Ok(Result);
        }

        [HttpPost("AddByListProduct")]
        public IActionResult AddByListProduct([FromBody]VoucherAddMulti obj)
        {
            KeyValuePair<bool, string> Result = new KeyValuePair<bool, string>(false, "Thất bại");
            try
            {
                var dicProduct = productBCL.GetVoucherById(obj.ListKey);
                foreach (var item in dicProduct.ToList())
                {
                    dicProduct[item.Key] = String.Join(",", obj.ListVoucher.Union(Utils.Utility.SplitStringToList(dicProduct[item.Key])));
                }

                Result = productBCL.UpdateVoucher(dicProduct);

                return Ok(Result);
            }
            catch (Exception ex)
            {
            }
            return Ok(Result);
        }


        [HttpPost("AddByListZone")]
        public IActionResult AddByListZone([FromBody]VoucherAddMulti obj)
        {
            KeyValuePair<bool, string> Result = new KeyValuePair<bool, string>(false, "Thất bại");
            try
            {
                var lstKey = productInZoneBCL.FindAll(x => obj.ListKey.Contains(x.ZoneId));
                var dicProduct = productBCL.GetVoucherById(lstKey.Select(x => x.ProductId).Distinct().ToList());
                foreach (var item in dicProduct.ToList())
                {
                    dicProduct[item.Key] = String.Join(",", obj.ListVoucher.Union(Utils.Utility.SplitStringToList(dicProduct[item.Key])));
                }
                Result = productBCL.UpdateVoucher(dicProduct);
                return Ok(Result);
            }
            catch (Exception ex)
            {
            }
            return Ok(Result);
        }
        [HttpPost("RemoveByListZone")]
        public IActionResult RemoveByListZone([FromBody]VoucherAddMulti obj)
        {
            KeyValuePair<bool, string> Result = new KeyValuePair<bool, string>(false, "Thất bại");
            try
            {
                var lstKey = productInZoneBCL.FindAll(x => obj.ListKey.Contains(x.ZoneId));
                var dicProduct = productBCL.GetVoucherById(lstKey.Select(x => x.ProductId).Distinct().ToList());
                foreach (var item in dicProduct.ToList())
                {
                    dicProduct[item.Key] = String.Join(",", Utils.Utility.SplitStringToList(dicProduct[item.Key]).Where(x => !obj.ListVoucher.Contains(x)));
                }
                Result = productBCL.UpdateVoucher(dicProduct);
                return Ok(Result);
            }
            catch (Exception ex)
            {
            }
            return Ok(Result);
        }
        [HttpPost("RemoveByListProduct")]
        public IActionResult RemoveByListProduct([FromBody]VoucherAddMulti obj)
        {
            KeyValuePair<bool, string> Result = new KeyValuePair<bool, string>(false, "Thất bại");
            try
            {
                var lstKey = productInZoneBCL.FindAll(x => obj.ListKey.Contains(x.ZoneId));
                var dicProduct = productBCL.GetVoucherById(lstKey.Select(x => x.ProductId).Distinct().ToList());
                foreach (var item in dicProduct.ToList())
                {
                    dicProduct[item.Key] = String.Join(",", Utils.Utility.SplitStringToList(dicProduct[item.Key]).Where(x => !obj.ListVoucher.Contains(x)));
                }
                Result = productBCL.UpdateVoucher(dicProduct);
                return Ok(Result);
            }
            catch (Exception ex)
            {
            }
            return Ok(Result);
        }
        [HttpPost("Merge")]
        public IActionResult Merge([FromBody] CouponChild coupon)
        {
            KeyValuePair<bool, string> Result = new KeyValuePair<bool, string>(false, "Thất bại");
            try
            {
                if (coupon.Status == null)
                    coupon.Status = 1;
                if (coupon.Parrent > 0)
                {
                    if (!String.IsNullOrEmpty(coupon.Ma))
                    {
                        var data = couponChildBCL.Merge(coupon);
                        if (data)
                            Result = new KeyValuePair<bool, string>(data, "Thành công");
                        else
                            Result = new KeyValuePair<bool, string>(data, "Thất bại");
                    }
                    else
                        Result = new KeyValuePair<bool, string>(false, "Không được để trống mã");

                }
                else
                    Result = new KeyValuePair<bool, string>(false, "Không được để trống nhóm");
                return Ok(Result);
            }
            catch (Exception ex)
            {
            }
            return Ok(Result);
        }
        [HttpPost("MergeList")]
        public IActionResult MergeList([FromBody]List<CouponChild> CouponChild)
        {
            KeyValuePair<bool, string> Result = new KeyValuePair<bool, string>(false, "Thất bại");
            try
            {
                var data = couponChildBCL.Merge(CouponChild);
                if (data)
                    Result = new KeyValuePair<bool, string>(data, "Thành công");
                else
                    Result = new KeyValuePair<bool, string>(data, "Thất bại");
                return Ok(Result);
            }
            catch (Exception ex)
            {
            }
            return Ok(Result);
        }
        [HttpPost("Delete")]
        public IActionResult Delete([FromBody] CouponChild coupon)
        {
            KeyValuePair<bool, string> Result = new KeyValuePair<bool, string>(false, "Thất bại");
            try
            {
                var data = couponChildBCL.Remove(coupon);
                if (data)
                    Result = new KeyValuePair<bool, string>(data, "Thành công");
                else
                    Result = new KeyValuePair<bool, string>(data, "Thất bại");
                return Ok(Result);
            }
            catch (Exception ex)
            {
            }
            return Ok(Result);
        }
    }
}
