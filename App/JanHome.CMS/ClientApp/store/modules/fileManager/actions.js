import HttpService from '../../../plugins/http'

const fmFileUpload = ({ commit }, data) => {
    return HttpService.post('/api/FileUploadV2/UploadImage', data, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
        .then(response => {
            return response.data;
        })
        .catch(e => {
            alert('ex found:' + e);
        })
}
const fmFileGetAll = ({ commit }, data) => {
    return HttpService.get(`/api/FileUploadV2/getall?take=${data.pageSize}&pageIndex=${data.pageIndex}`, {})
        .then(response => {
            let listFiles = {
                files: response.data.data,
                totals: response.data.totalRow
            }
            commit("GET_ALL_FILE", listFiles);
        })
        .catch(e => {
            alert('ex found:' + e);
        })
}

export default {
    fmFileUpload,
    fmFileGetAll
}
