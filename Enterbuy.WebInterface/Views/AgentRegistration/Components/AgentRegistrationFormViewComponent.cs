﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.WebInterface.Views.AgentRegistration.Components
{
    public class AgentRegistrationFormViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}