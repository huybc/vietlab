﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.WebInterface.Controllers
{
    public class ProductController : BaseController
    {
        [HttpPost]
        public IActionResult ViewMore(int zone_parent_id, int locationId, int skip, int size=11)
        {
            return ViewComponent("ViewMore", new { zone_parent_id = zone_parent_id, locationId = locationId, skip = skip, size = size });
        }
    }
}