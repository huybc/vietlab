﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.Text;

namespace Enterbuy.WebInterface.UtilityWeb
{

    public class UtilsHeperWeb
    {

        public UtilsHeperWeb()
        {

        }
        public static void GetCategoryProduct(List<ProductMinify> lstObj, List<ZoneDetail> lstZone)
        {
            var dic = lstZone.ToDictionary(x => x.Id, x => x);

            foreach (var item in lstObj)
            {
                if (dic.ContainsKey(item.ZoneId))
                {
                    item.ZoneUrl = dic[item.ZoneId].Url;
                }
            }
        }

        public static string BootstrapPager(int currentPageIndex, string link, int totalItems, int pageSize = 10, int numberOfLinks = 5, bool parameter = false)
        {
            StringBuilder sb = new StringBuilder();
            if (totalItems <= 0)
            {
                return string.Empty;
            }
            var totalPages = (int)Math.Ceiling(totalItems / (double)pageSize);
            var lastPageNumber = (int)Math.Ceiling((double)currentPageIndex / numberOfLinks) * numberOfLinks;
            var firstPageNumber = lastPageNumber - (numberOfLinks - 1);
            var hasPreviousPage = currentPageIndex > 1;
            var hasNextPage = currentPageIndex < totalPages;
            if (lastPageNumber > totalPages)
            {
                lastPageNumber = totalPages;
            }
            sb.Append("<ul class=\"number-page\">");
            sb.Append(AddLink(1, link, currentPageIndex == 1, "disabled", "<i class=\"fa fa-angle-double-left\"></i>", "First Page", true));
            sb.Append(AddLink(currentPageIndex - 1, link, !hasPreviousPage, "disabled", "<i class=\"fa fa-angle-left\"></i>", "Previous Page", true));
            for (int i = firstPageNumber; i <= lastPageNumber; i++)
            {
                sb.Append(AddLink(i, link, i == currentPageIndex, "active", i.ToString(), i.ToString(), parameter));
            }
            sb.Append(AddLink(currentPageIndex + 1, link, !hasNextPage, "disabled", "<i class=\"fa fa-angle-right\"></i>", "Next Page", true));
            sb.Append(AddLink(totalPages, link, currentPageIndex == totalPages, "disabled", "<i class=\"fa fa-angle-double-right\"></i>", "Last Page", true));
            sb.Append("</ul>");
            return sb.ToString();
        }
        private static string AddLink(int index, string link, bool condition, string classToAdd, string linkText, string tooltip, bool parameter)
        {
            StringBuilder sb = new StringBuilder();
            if (condition)
            {
                sb.Append("<li class='" + classToAdd + "'" + classToAdd + ">");
            }
            else
            {
                sb.Append("<li>");
            }

            var str = !parameter ? "" : "icon-pre";
            if (index <= 1)
            {
                sb.AppendFormat("<a class='nb-txt nb-hover " + str + "' href='{0}'>{1}</a>", !condition ? link + ".html" : "javascript:", linkText);
            }
            else
            {
                sb.AppendFormat("<a class='nb-txt nb-hover " + str + "' href='{0}'>{1}</a>", !condition ? link + "/page/" + index + ".html" : "javascript:", linkText);
            }

            sb.Append("</li>");
            return sb.ToString();
        }
        public static string SeoPaging(int index, string link, int total)
        {
            StringBuilder sb = new StringBuilder();
            if (index > 1)
            {
                sb.Append($"<link rel='prev' href='{link}/page/{index - 1}.html' >");
            }
            if (index < total)
            {
                sb.Append($"<link rel='next' href='{link}/page/{index + 1}.html' >");
            }
            return sb.ToString();
        }
    }


}

