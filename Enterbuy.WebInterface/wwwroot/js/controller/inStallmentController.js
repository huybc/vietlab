﻿R.Installment = {
    Init: function() {
        R.Installment.RegisterEvent();
    },

    RegisterEvent: function() {
        $(".btn-voucher-installment").on("click", function() {
            var parrent = $("#coupon-install-apply");
            var id = $("#ProductDetail_Id").val();
            var voucher = $("#txtCodeInstallment").val();
            R.Order.LoadVoucher(parrent, id, voucher);
        });
        $(".LoadInfoCard_Click").on("click", function() {
            var id = $(this).attr('data-id');
            var title = $(this).attr('data-title');
            $('#tableBankInfo_value').attr('data-bank', title);
            var giasanpham = $(this).attr('data-price');
            var phi = $(this).attr('data-charge');
            var strJson = document.getElementById('InfoCard_' + id).value;
            var objJson = JSON.parse(strJson);
            $('#listBank li img').removeClass("active");
            $(this).addClass("active");
            var l_InfoCard = objJson.InfoCard;
            var thangtragop = "";
            var listPhiQuetThe = [];
            var listPhiTraGop = [];
            var listGiaMuaTraGop = [];
            var listGopMoiThang = [];
            for (var idx in l_InfoCard) {
                thangtragop += "<td>" + l_InfoCard[idx].MonthNumber + " tháng</td>";
                //console.log(phi + ' ' + parseInt(l_InfoCard[idx].MonthNumber) + ' ' + (phi * parseInt(l_InfoCard[idx].MonthNumber)));
                //console.log(giasanpham + ' ' + parseInt(l_InfoCard[idx].InterestRate) / 100 + ' ' + (giasanpham * parseInt(l_InfoCard[idx].InterestRate) / 100));
                listPhiQuetThe.push(((phi / 100) * giasanpham) * parseInt(l_InfoCard[idx].MonthNumber));
                listPhiTraGop.push((giasanpham * parseInt(l_InfoCard[idx].InterestRate) / 100));
            }
            for (var i = 0; i < l_InfoCard.length; i++) {
                var gia = parseInt(giasanpham) + parseInt(listPhiQuetThe[i]) + parseInt(listPhiTraGop[i]);
                listGiaMuaTraGop.push(gia);
                listGopMoiThang.push(gia / parseInt(l_InfoCard[i].MonthNumber));
            }
            var htmlContentTable = '<thead>';
            htmlContentTable += '<tr>';
            htmlContentTable += '<th colspan=\"5\" class=\"cust-thead\">';
            htmlContentTable += 'Trả góp qua thẻ VISA, ' + title;
            htmlContentTable += '</th>';
            htmlContentTable += '</tr>';
            htmlContentTable += '</thead>';
            htmlContentTable += '<tbody>';
            htmlContentTable += '<tr>';
            htmlContentTable += '<td>Số tháng trả góp</td>';
            htmlContentTable += thangtragop
            htmlContentTable += '</tr>';
            htmlContentTable += '<tr>';
            htmlContentTable += '<td>Giá mua trả góp</td>';
            for (var i in listGiaMuaTraGop) {
                htmlContentTable += "<td>" + giasanpham + " ₫</td>";
            }
            htmlContentTable += '</tr>';
            htmlContentTable += '<tr>';
            htmlContentTable += '<td>Phí trả góp</td>';
            for (var i in listPhiTraGop) {
                htmlContentTable += "<td>" + R.FormatNumber(Math.round(listPhiTraGop[i])) + " ₫</td>";
            }
            htmlContentTable += '</tr>';
            htmlContentTable += '<tr>';
            htmlContentTable += '<td>Góp mỗi tháng</td>';
            for (var i in listGopMoiThang) {
                htmlContentTable += "<td>" + R.FormatNumber(Math.round(listGopMoiThang[i])) + " ₫</td>";
            }
            htmlContentTable += '</tr>';
            htmlContentTable += '<tr>';
            htmlContentTable += '<td>Tổng tiền phải trả</td>';
            for (var i in listGiaMuaTraGop) {
                htmlContentTable += "<td>" + R.FormatNumber(Math.round(listGiaMuaTraGop[i])) + " ₫</td>";
            }
            htmlContentTable += '</tr>';
            htmlContentTable += '<tr>';
            htmlContentTable += '<td>Chênh lệch với mua trả thẳng</td>';
            for (var i in listGiaMuaTraGop) {
                htmlContentTable += "<td>" + R.FormatNumber(Math.round(listGiaMuaTraGop[i] - giasanpham)) + " ₫</td>";
            }
            htmlContentTable += '</tr>';
            htmlContentTable += '<tr>';
            htmlContentTable += '<td></td>';
            for (var idx in l_InfoCard) {
                htmlContentTable += '<td>';
                htmlContentTable += '<button onclick=\"btnbuyinstallment_click(event, \'' + id + '\', \'' + l_InfoCard[idx].MonthNumber + '\', \'' + l_InfoCard[idx].InterestRate + '\', \'' + Math.round(listGopMoiThang[idx]) + '\')\" class=\"btn-buy btn-buy-installment-click\">'
                htmlContentTable += 'Chọn mua'
                htmlContentTable += '</button>'
                htmlContentTable += '</td>';
            }
            htmlContentTable += '</tr>';
            htmlContentTable += '</tbody>';

            document.getElementById('tableBankInfo').innerHTML = htmlContentTable;
        });
        $(".LoadInfoCard_Pay_Click").on("click", function() {
            var id = $(this).attr('data-id');
            var title = $(this).attr('data-title');
            $('#tableBankInfo_value').attr('data-card', title);
            $('#listBank_pay li img').removeClass("active");
            $(this).addClass("active");
        });

        $('#order-form-installment').off('submit').on('submit', function() {
            //Lay ra thong tin cua khach hang
            //var yeucaukhac = document.getElementById('other_required').value
            var tinh = $(this).find('.tinh-thanh option:selected').html();
            var quan = $(this).find('.quan-huyen option:selected').html();
            var phuong = $(this).find('.phuong-xa option:selected').html();
            var nha = $(this).find('.so-nha').val();
            var address = nha + " - " + phuong + " - " + quan + " - " + tinh;
            var gender = $("input[name='gender-radio']:checked").val();
            var customer_infomation = {
                Gender: gender,
                Name: $(this).find('.name').val(),
                PhoneNumber: $(this).find('.phone-number').val(),
                Note: $(this).find('.note').val(),
                Address: address,
                Id: 0
            }
            var checkDaChon = true;
            for (var i = 0; i < $('.btn-buy-installment-click').length; i++) {
                if ($('.btn-buy-installment-click')[i].textContent.trim() == 'Đã chọn') {
                    checkDaChon = false;
                }
            }
            if (checkDaChon == true) {
                alert('Chọn hình thức trả góp !');
                event.preventDefault();
                return;
            }

            event.preventDefault();
            //
            $('#modal-tra-gop').modal('show');
            var modals = $('#modal-tra-gop');
            let code = "ENB-" + R.Order.GetRandom(10);
            $('.order-code').text(code);

            modals.find('.customer-name').text(customer_infomation.Name);
            modals.find('.customer-phone').text(customer_infomation.PhoneNumber);
            modals.find('.customer-address').text(customer_infomation.Address);
            modals.find('.customer-note').text(customer_infomation.Note);
            var now = moment(new Date()).format('DD - MM - YYYY');
            modals.find('.bank-name').text($('#tableBankInfo_value').attr('data-bank'));
            modals.find('.bank-card').text($('#tableBankInfo_value').attr('data-card'));
            modals.find('.bank-month').text($('#tableBankInfo_value').attr('data-thang'));
            modals.find('.bank-everymonth').text($('#tableBankInfo_value').attr('data-gopmoithang'));
            //
            var promotion = [];
            $(".promotion_product").each(function(index) {
                var i = {
                    PromotionId: $(this).data('id'),
                    LogType: $(this).data('type'),
                    LogValue: $(this).data('value'),
                    LogName: $(this).data('name')
                };
                promotion.push(i);
            });
            //
            var product_infomation = [];
            var ProductId = 0,
                Name = '',
                LogPrice = 0;
            if ($('.product-id').length > 0)
                ProductId = parseInt($('.product-id').text());
            if ($('.product-name').length > 0)
                Name = $('.product-name').text()
            if ($('.product-total-price').length > 0)
                LogPrice = parseInt($('.product-total-price').data('saleprice'))
            var order_type = document.getElementById('is-flash-sale').value;            
            var order_source_id = 0;
            //if (el.data('is-flash-sale') > 0) {
            //    order_type = 3;
            //    order_source_id = el.data('is-flash-sale');
            //}
            var product_infomation_item = {
                Voucher: "",
                VoucherType: 0,
                VoucherPrice: 0,
                ProductId: ProductId,
                Name: Name,
                LogPrice: LogPrice,
                Quantity: 1,
                OrderSourceType: order_type,
                OrderSourceId: order_source_id,
                Promotions: promotion
            }
            var metaorder = {
                id: $('#tableBankInfo_value').attr('data-id'),
                MonthNumber: $('#tableBankInfo_value').attr('data-thang'),
                InterestRate: $('#tableBankInfo_value').attr('data-lai')
            }
            product_infomation.push(product_infomation_item);
            $(".btn_installment_save").off('click').on("click", function() {
                var orders = {
                    OrderCode: code,
                    Customer: customer_infomation,
                    Products: product_infomation,
                    Extras_MetaOrder: JSON.stringify(metaorder),
                    Order_Source_Type: order_type
                }
                var url = R.Order.culture + '/Product/CreateOrderInstallment';
                $.post(url, orders, function(response) {
                    $('#modal-tra-gop').modal('hide');
                    $('#modal-xn').modal('show');
                });
            });
            //
        });
        $('.btn-buy').off('click').on('click', function() {
            //Xoa toan bo cac css cu
            $('.cust-choose').each(function(element) {

                $(this).attr('style', '');
            })
            $(this).css('background-color', '#ffffff');
            $(this).css('color', '#EE7D22');
            $(this).css('border', '1px solid #EE7D22');

        })
    },
    LoadVoucher(parrent, id, voucher) {

        let url = "/Product/CheckVoucher?productId=" + id + "&voucher=" + voucher;
        $.get(url, function(data) {
            if (data.key == true) {
                parrent.find(".mess-coupon").show();
                parrent.find(".mess-coupon").addClass("alert-success");
                parrent.find(".mess-coupon").removeClass("alert-danger");
                parrent.find(".mess-coupon").text(data.value.name);
                parrent.find(".coupon > span.data-voucher").attr("data-value", data.value.valueDiscount);
                parrent.find(".coupon > span.data-voucher").attr("data-type", data.value.discountOption);
                parrent.find(".coupon > span.data-voucher").attr("voucher-active", data.value.code);

                if (data.value.discountOption == 2) {
                    parrent.find(".value-voucher").text("Giảm giá: " + R.FormatNumber(data.value.valueDiscount) + "đ");
                } else if (data.value.discountOption == 1) {
                    parrent.find(".value-voucher").text("Giảm giá: " + data.value.valueDiscount + "%");
                }

            } else {
                parrent.find(".mess-coupon").show();
                parrent.find(".mess-coupon").addClass("alert-danger");
                parrent.find(".mess-coupon").removeClass("alert-success");

                parrent.find(".mess-coupon").text(data.value.name);
                parrent.find(".coupon > span.data-voucher").attr("data-value", 0);
                parrent.find(".coupon > span.data-voucher").attr("data-type", 0);
                parrent.find(".coupon > span.data-voucher").attr("voucher-active", "");
                parrent.find(".value-voucher").text("");
            }
            R.Order.CalculatePriceProductItem(parrent);
        });
    }
};

$(function() {
    R.Installment.Init();
});