﻿R.BlogList2 = {
    Init: function () {
        R.BlogList2.culture = R.Culture();
        R.BlogList2.RegisterEvent();
    },
    RegisterEvent: function () {
        $('.select-page').off('change').on('change', function () {
            var url = $(this).val();
            window.location.href = url;
        })

    },
    ViewMore : function (zone_id, to_page, page_size, type, alias) {
        var url = "/" + alias + ".b" + zone_id + ".html";
        var queryStirng = "?pageIndex=" + to_page;
        window.location.href = url + queryStirng;
    }

}

$(function () {
    R.BlogList2.Init();
})