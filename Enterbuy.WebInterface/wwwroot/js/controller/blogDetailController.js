﻿R.BlogDetail = {
    Init: function () {
        R.BlogDetail.location_id = R.CurrentLocationId();
        R.BlogDetail.culture = R.Culture();
        R.BlogDetail.LoadListProduct();
        R.BlogDetail.AddViewCountArticle();
        //R.BlogDetail.FillterNameComment();
    },
    RegisterEvent: function () {

    },
    LoadListProduct: function () {
        $('product').each(function (element) {

            var el = $(this);
            var product_list = $(this).data('id-list');
            var params = {
                product_ids: product_list,
                location_id: R.BlogDetail.location_id
            }
            var url = "/Blog/ProductsInArticle";
            $.post(url, params, function (response) {
                el.replaceWith(response);
            })
        })
    },
    AddViewCountArticle: function () {

        var id_san_pham = $('.detail-container').data('id');
        var url = "/Common/CreateViewCount";
        var params = {
            objectId: id_san_pham,
            type: 'article'
        }
        $.post(url, params, function (response) {

        })

    },
    FillterNameComment: function () {
        var listfullName = $(".comment_infomation span#fullName");
        if (listfullName.length > 0) {
            for (var index = 0; index < listfullName.length; index++) {
                var fullName = listfullName[index];
                var profileId = fullName.dataset.fid;
                var textResult = fullName.textContent.split(' ').map(name => name[0]).join('').toUpperCase();
                document.getElementById("imageProfile_" + profileId).innerHTML = textResult;
            }

        } else {
            return false;
        }


    }
}



$(function () {
    R.BlogDetail.Init()
})