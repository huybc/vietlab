﻿using System.Collections.Generic;

namespace Enterbuy.Http.Exceptions
{
    public class BadRequestException : BaseCustomException
    {
        public BadRequestException(List<string> errors) : base(errors, System.Net.HttpStatusCode.BadRequest)
        {
        }

        public BadRequestException(string error) : base(new List<string>() { error }, System.Net.HttpStatusCode.BadRequest)
        {
        }
    }
}