﻿using System.Collections.Generic;

namespace Enterbuy.Http.Exceptions
{
    public class ForbiddenException : BaseCustomException
    {
        public ForbiddenException(string message) : base(new List<string>() { message }, System.Net.HttpStatusCode.Forbidden)
        {
        }
    }
}