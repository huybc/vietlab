﻿using Enterbuy.Http.Exceptions;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Enterbuy.Http.Controllers
{
    public class BaseController : Controller
    {
        public const string CookieLocationId = "_LocationId";
        public const string CookieLocationName = "_LocationName";

        private string _currentLanguage;
        private string _currentLanguageCode;
        public string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        public string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    IRequestCultureFeature feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                }

                return _currentLanguageCode;
            }


        }

        protected void ValidateModel()
        {
            if (!ModelState.IsValid)
            {
                var errors = new List<string>();
                foreach (var modelState in ModelState.Values)
                    foreach (var error in modelState.Errors)
                        errors.Add(error.ErrorMessage);
                throw new UnprocessableEntityException(errors);
            }
        }

        public override bool TryValidateModel(object model)
        {
            return base.TryValidateModel(model);
        }

        public override bool TryValidateModel(object model, string prefix)
        {
            return base.TryValidateModel(model, prefix);
        }

        protected string GetTextBody()
        {
            string text = (string)HttpContext.Items["BodyText"];
            return text;
        }

        protected JObject GetJBody()
        {
            var textBody = GetTextBody();
            if (string.IsNullOrWhiteSpace(textBody))
                return null;

            var jsonTree = JObject.Parse(textBody);
            return jsonTree;
        }
    }
}