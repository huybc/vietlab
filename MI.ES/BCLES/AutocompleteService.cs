﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.ES;
using Nest;

namespace MI.ES.BCLES
{
    public class AutocompleteService : ConfigES
    {
        static readonly ElasticClient client = client_HeThong;
        public static async Task<bool> CreateIndexAsync()
        {
            var createIndexDescriptor = new CreateIndexDescriptor(strNameIndex_HeThong)
                .Mappings(ms => ms
                          .Map<ProductES>(m => m
                                .AutoMap()
                                .Properties(ps => ps
                                    .Completion(c => c
                                        .Name(p => p.Suggest))))

                         );

            if (client.IndexExists(strNameIndex_HeThong.ToLowerInvariant()).Exists)
            {
                client.DeleteIndex(strNameIndex_HeThong.ToLowerInvariant());
            }

            ICreateIndexResponse createIndexResponse = await client.CreateIndexAsync(createIndexDescriptor);

            return createIndexResponse.IsValid;
        }

        public static async Task IndexAsync(List<ProductES> products)
        {
            await client.IndexManyAsync(products, strNameIndex_HeThong);
        }

        public static async Task<ProductSuggestResponse> SuggestAsync(string keyword, string lang)
        {
            if (string.IsNullOrEmpty(lang))
            {
                lang = Utils.Utility.DefaultLang;
            }

            ISearchResponse<ProductES> searchResponse = await client.SearchAsync<ProductES>(s => s
                                     .Index(strNameIndex_HeThong)
                                     .Query(x => x.Term(d => d.Info.Select(a => a.Lang), lang)
                                      && x.QueryString(d => d.DefaultField(u => u.Info.Select(a => a.Name)).Query($"\"{keyword}\"")))
                                     .Suggest(su => su
                                          .Completion("suggestions", c => c
                                               .Field(f => f.Suggest)
                                               .Prefix(keyword)
                                               .Fuzzy(f => f
                                                   .Fuzziness(Fuzziness.Auto)
                                               )
                                               .Size(10))
                                             ));
            var data1 = searchResponse.Suggest["suggestions"];

            var suggests = from suggest in searchResponse.Suggest["suggestions"]
                           from option in suggest.Options
                           select new ProductSuggest
                           {
                               Id = option.Source.Id,
                               Avatar = Utils.UIHelper.StoreFilePath(option.Source.Avatar),
                               Name = option.Source.Info.FirstOrDefault(x => x.Lang == lang).Name,
                               Url = "/" + option.Source.Info.FirstOrDefault(x => x.Lang == lang).Url,
                               Price = option.Source.Price,
                               DiscountPrice = option.Source.DiscountPrice,
                               Unit = option.Source.Unit,
                               Score = option.Score
                           };
            return new ProductSuggestResponse
            {
                Suggests = suggests
            };
        }

        public static async Task<ProductSuggestResponse> SuggestEnterBuyAsync(string keyword, string lang)
        {
            if (string.IsNullOrEmpty(lang))
            {
                lang = Utils.Utility.DefaultLang;
            }

            ISearchResponse<ProductES> searchResponse = await client.SearchAsync<ProductES>(s => s
                                     .Index(strNameIndex_HeThong)
                                     .Query(x => x.Term(d => d.Info.Select(a => a.Lang), lang)
                                      && x.QueryString(d => d.DefaultField(u => u.Info.Select(a => a.Name)).Query($"\"{keyword}\"")))
                                     .Suggest(su => su
                                          .Completion("suggestions", c => c
                                               .Field(f => f.Suggest)
                                               .Prefix(keyword)
                                               .Fuzzy(f => f
                                                   .Fuzziness(Fuzziness.Auto)
                                               )
                                               .Size(10))
                                             ));
            var data1 = searchResponse.Suggest["suggestions"];

            var suggests = from suggest in searchResponse.Suggest["suggestions"]
                           from option in suggest.Options
                           select new ProductSuggest
                           {
                               Id = option.Source.Id,
                               Avatar = Utils.UIHelper.StoreFilePath(option.Source.Avatar),
                               Name = option.Source.Info.FirstOrDefault(x => x.Lang == lang).Name,
                               Url = Utils.BaseBA.UrlProduct("", option.Source.Info.FirstOrDefault(x => x.Lang == lang).Url),
                               Price = option.Source.Price,
                               DiscountPrice = option.Source.DiscountPrice,
                               Unit = "/Chiếc",
                               Score = option.Score
                           };
            return new ProductSuggestResponse
            {
                Suggests = suggests
            };
        }
    }
}