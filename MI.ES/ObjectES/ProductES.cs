﻿using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MI.ES
{

    public class InfoES
    {
        public string Lang { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
    public class ProductES
    {
        public int Id { get; set; }
        public string Avatar { get; set; }
        public string Code { get; set; }
        public string Price { get; set; }
        public string DiscountPrice { get; set; }
        public string Unit { get; set; }
        public List<InfoES> Info { get; set; }
        public CompletionField Suggest { get; set; }

        public ProductES()
        {
            this.Id = 0;
            this.Avatar = string.Empty;
            this.Code = string.Empty;
            this.Price = "0";
            this.DiscountPrice = "0";
            this.DiscountPrice = "0";
            this.Unit = "/mm2";
            this.Info = new List<InfoES>();
        }
    }

    public class ProductSuggestResponse
    {
        public IEnumerable<ProductSuggest> Suggests { get; set; }
    }

    public class ProductSuggest
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string DiscountPrice { get; set; }
        public string Unit { get; set; }
        public double Score { get; set; }
    }
}
