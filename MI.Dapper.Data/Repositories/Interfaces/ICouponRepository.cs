using System.Collections.Generic;
using System.Threading.Tasks;
using MI.Dapper.Data.Models;
using MI.Dapper.Data.ViewModels;
using Utils;

namespace MI.Dapper.Data.Repositories.Interfaces
{
    public interface ICouponRepository
    {
        Task<bool> Create(CouponViewModel coupon);
        Task<bool> Update(CouponViewModel coupon);
        Task<PagedResult<CouponViewModel>> GetAllPaging(FilterQuery filterQuery);
        Task<CouponViewModel> GetById(int id);
        Task<bool> Unpublish(int id);
        int CheckCode(string Name, string Code);
        bool UpdateCouPonChill(string ma);
    }
}