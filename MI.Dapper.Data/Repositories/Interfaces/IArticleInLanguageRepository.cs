using System.Threading.Tasks;
using MI.Dapper.Data.ViewModels;

namespace MI.Dapper.Data.Repositories.Interfaces
{
    public interface IArticleInLanguageRepository
    {
        Task<int> Create(ArticleInLanguageViewModel articleInLanguageViewModel);
    }
}