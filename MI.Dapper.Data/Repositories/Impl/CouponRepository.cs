using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MI.Dapper.Data.Models;
using MI.Dapper.Data.Repositories.Interfaces;
using MI.Dapper.Data.ViewModels;
using Microsoft.Extensions.Configuration;
using Utils;

namespace MI.Dapper.Data.Repositories.Impl
{
    public class CouponRepository : ICouponRepository
    {
        private readonly string _connectionString;

        public CouponRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<bool> Create(CouponViewModel coupon)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    var parameters = new DynamicParameters();
                    parameters.Add("@Name", coupon.Name, DbType.String);
                    parameters.Add("@Code", coupon.Code);
                    parameters.Add("@DiscountOption", coupon.DiscountOption);
                    parameters.Add("@NumberOfUsed", coupon.NumberOfUsed);
                    parameters.Add("@QuantityDiscount", coupon.QuantityDiscount);
                    parameters.Add("@ValueDiscount", coupon.ValueDiscount);
                    parameters.Add("@StartDate", coupon.StartDate);
                    parameters.Add("@EndDate", coupon.EndDate);
                    parameters.Add("@Category", String.Join(",", coupon.Category));
                    parameters.Add("@IsCategory", coupon.IsCategory);
                    parameters.Add("@ListProduct",String.Join(",",coupon.ListProduct));
                    parameters.Add("@id", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    var result = await connection.ExecuteAsync("Create_Coupon", parameters, null, null,
                        CommandType.StoredProcedure);
                    var newId = parameters.Get<int>("@id");
                    return true;
                }
                catch (Exception e)
                {
                }
            }

            return false;
        }

        public async Task<bool> Update(CouponViewModel coupon)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    var parameters = new DynamicParameters();
                    parameters.Add("@Name", coupon.Name, DbType.String);
                    parameters.Add("@Code", coupon.Code);
                    parameters.Add("@DiscountOption", coupon.DiscountOption);
                    parameters.Add("@NumberOfUsed", coupon.NumberOfUsed);
                    parameters.Add("@QuantityDiscount", coupon.QuantityDiscount);
                    parameters.Add("@ValueDiscount", coupon.ValueDiscount);
                    parameters.Add("@StartDate", coupon.StartDate);
                    parameters.Add("@EndDate", coupon.EndDate);
                    parameters.Add("@Category", String.Join(",", coupon.Category));
                    parameters.Add("@IsCategory", coupon.IsCategory);
                    parameters.Add("@ListProduct", String.Join(",", coupon.ListProduct));
                    parameters.Add("@id", coupon.Id);
                    var result = await connection.ExecuteAsync("Update_Coupon", parameters, null, null,
                        CommandType.StoredProcedure);
                    return true;
                }
                catch (Exception e)
                {
                }
            }

            return false;
        }

        public async Task<PagedResult<CouponViewModel>> GetAllPaging(FilterQuery filterQuery)
        {
            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    var parameters = new DynamicParameters();
                    parameters.Add("@keyword", filterQuery.keyword);
                    parameters.Add("@pageIndex", filterQuery.pageIndex);
                    parameters.Add("@pageSize", filterQuery.pageSize);
                    parameters.Add("@totalRow", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    var result = await conn.QueryAsync<CouponViewModel>("Get_Coupon_AllPaging",
                        parameters, null, null,
                        CommandType.StoredProcedure);
                    var totalRow = parameters.Get<int>("@totalRow");
                    var manufacturersPaging = new PagedResult<CouponViewModel>()
                    {
                        TotalRow = totalRow,
                        Items = result.ToList(),
                        PageIndex = filterQuery.pageIndex,
                        PageSize = filterQuery.pageSize
                    };
                    return manufacturersPaging;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<CouponViewModel> GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    var sqlCouponById = "select * from Coupon where Id=" + id;
                   
                    var resultCouponById =
                        await connection.QueryAsync<CouponViewModel>(sqlCouponById, null, null, null,
                            CommandType.Text);
                    var result = resultCouponById.FirstOrDefault();

                    return result;
                }
                catch (Exception e)
                {
                }
            }
            return new CouponViewModel();
        }



        public async Task<bool> Unpublish(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    var sqlUnpublishCouponById = " update Coupon SET Locked=1 where Id=" + id;
                   

                    var resultCouponById =
                        await connection.ExecuteAsync(sqlUnpublishCouponById, null, null, null,
                            CommandType.Text);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
        public int CheckCode(string Name,string Code)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    var sqlUnpublishCoupon = "select * from Coupon c inner join CouponChild cc on cc.Parrent = c.Id where cc.Name = '" + Name+ "' And cc.Ma = '" + Code+"' and cc.NumberUseCode <= c.QuantityDiscount and cc.ExprireDate >= GETDATE()And cc.Status = 1";
                    var resultCouponById =
                         connection.Query<CouponViewModel>(sqlUnpublishCoupon, null, null, true,null,
                            CommandType.Text);
                    var getdate = DateTime.Now;
                    var rs =  resultCouponById.Where(x => x.Locked == false && x.EndDate >= getdate && x.QuantityDiscount > 0);
                    if(rs.Count() > 0)
                    {
                        return rs.FirstOrDefault().Id;
                    }
                    return 0;
                }
                catch (Exception e)
                {
                    return 0;
                }
            }
        }
        public bool UpdateCouPonChill(string ma)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    var sqlUnpublishCouponById = "update CouponChild set NumberUseCode = NumberUseCode + 1 ,Status = 4 where Ma = '" + ma+"'";
                    var resultCouponById =
                         connection.Execute(sqlUnpublishCouponById, null, null,null,
                            CommandType.Text);
                    if (resultCouponById > 0)
                    {
                        return true;
                    }
                    return false;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
    }
}