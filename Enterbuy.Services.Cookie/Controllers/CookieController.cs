﻿using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Services.Contact.Services;
using System;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Enterbuy.Services.Cookie.Controllers
{
    public class CookieController : BaseController
    {
        private readonly ILocationDao _locationsRepository;
        private IHttpContextAccessor _httpContextAccessor;

        public CookieController(ILocationDao locationsRepository, IHttpContextAccessor httpContextAccessor)
        {
            _locationsRepository = locationsRepository;
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult ChangeCurrentLocation(int location_id, string location_name)
        {

            CookieOptions cookie = new CookieOptions()
            {
                Path = "/",
                HttpOnly = false,
                Secure = false
            };
            var cookie_current_location = _httpContextAccessor.HttpContext.Request.Cookies[CookieLocationId];
            if (cookie_current_location != null)
            {
                //Xoa cookie hien tai co san
                Response.Cookies.Delete(CookieLocationId);
                Response.Cookies.Delete(CookieLocationName);
                //Create cookie moi
                Response.Cookies.Append(CookieLocationId, location_id.ToString(), cookie);
                Response.Cookies.Append(CookieLocationName, location_name, cookie);
            }
            return Ok(new KeyValuePair<int,string>(location_id, location_name));
        }

        public IActionResult SetCookieDefault()
        {
            CookieOptions cookie = new CookieOptions()
            {
                Path = "/",
                HttpOnly = false,
                Secure = false
            };
            if (Request.Cookies[CookieLocationId] == null)
            {
                var location_default = _locationsRepository.GetLocationFirst(CurrentLanguageCode);
                if (location_default != null)
                {
                    Response.Cookies.Append(CookieLocationId, location_default.Id.ToString(), cookie);
                    Response.Cookies.Append(CookieLocationName, location_default.Name, cookie);
                    ViewBag.LocationId = location_default.Id;
                    ViewBag.LocationName = location_default.Name;
                }
            }
            return Ok("Set Cookie successful!");
        }

    }
}
