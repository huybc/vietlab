﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;

namespace Enterbuy.Services.ProductCategory.Components
{
    public class FilterProductInCategoryViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneDao;
        private readonly IManufactureDao _manufactureDao;

        public FilterProductInCategoryViewComponent(IZoneDao zoneDao, IManufactureDao manufactureDao)
        {
            _zoneDao = zoneDao;
            _manufactureDao = manufactureDao;
        }

        public async Task<IViewComponentResult> InvokeAsync(int zoneId,string manufacturerId)
        {
            ViewBag.ZoneId = zoneId;
            var result = await _manufactureDao.GetManufactures(Utils.Utility.DefaultLang, manufacturerId);
            return View(result);
        }
    }
}