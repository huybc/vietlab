﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.ProductCategory.Components
{
    public class ProductListDetailInCategoryViewComponent : ViewComponent
    {
        private readonly IProductDao _productDao;
        private readonly Utils.ICookieLocationUtility _cookieLocationUtility;
        public ProductListDetailInCategoryViewComponent(IProductDao productDao, Utils.ICookieLocationUtility cookieLocationUtility)
        {
            _productDao = productDao;
            _cookieLocationUtility = cookieLocationUtility;
        }

        public async Task<IViewComponentResult> InvokeAsync(ZoneByTreeViewMinify zoneParent, int locationId, int PageIndex = 1)
        {
            var location = _cookieLocationUtility.SetCookieDefault();

            var total = 0;
            var model = _productDao.GetProductMinifiesTreeViewByZoneParentId(zoneParent.Id, Utils.Utility.DefaultLang, location.LocationId, PageIndex, 28, out total);
            ViewBag.Total = total;
            ViewBag.ZoneParent = zoneParent;
            return View(model);
        }
    }
}
