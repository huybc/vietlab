﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Services.ProductCategory.Components
{
    public class ListProductInCategoryViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneDao;

        public ListProductInCategoryViewComponent(IZoneDao zoneDao)
        {
            _zoneDao = zoneDao;
        }

        public async Task<IViewComponentResult> InvokeAsync(ZoneDetail zone)
        {
            var zoneSelectedWithTreeview = await _zoneDao.GetZoneByTreeViewMinifies(1, Utils.Utility.DefaultLang, zone.Id);
            var zoneDetails = zone;
            ViewBag.ZoneTreeView = zoneSelectedWithTreeview;
            ViewBag.Id = zone.Id;
            ViewBag.ZoneDetail = zoneDetails;
            return View();
        }
    }
}