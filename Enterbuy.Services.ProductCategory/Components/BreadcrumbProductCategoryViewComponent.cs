﻿using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.ProductCategory.Components
{
    public class BreadcrumbProductCategoryViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneDao;

        public BreadcrumbProductCategoryViewComponent(IZoneDao zoneDao)
        {
            _zoneDao = zoneDao;
        }

        public async Task<IViewComponentResult> InvokeAsync(int zoneId)
        {
            var breadcrumbs = await _zoneDao.GetBreadcrumbByZoneId(zoneId, "vi-VN");
            return View(breadcrumbs);
        }
    }
}