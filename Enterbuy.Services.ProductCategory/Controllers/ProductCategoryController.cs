﻿using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.ProductCategory.Controllers
{
    public class ProductCategoryController : BaseController
    {
        private readonly IZoneDao _zoneDao;

        public ProductCategoryController(IZoneDao zoneDao)
        {
            _zoneDao = zoneDao;
        }
        [Route("{alias}.html")]
        public async Task<IActionResult> Index(string alias, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 10;
            var alsplit = alias.Split('.');
            ViewData["IsExpand"] = false;
            var zoneTarget = await _zoneDao.GetZoneByAlias(alsplit[0].ToString(), "vi-VN");
            if (zoneTarget != null)
            {
                if (zoneTarget.Type != 1)
                {
                    return Redirect(Utils.BaseBA.UrlCategoryNews(zoneTarget.Url));
                }
                else
                {
                    if (alsplit[0].ToString().Equals(zoneTarget.Url))
                    {
                        ViewBag.ZoneId = zoneTarget.Id;
                        ViewBag.Type = zoneTarget.Type;
                        ViewBag.Parent = zoneTarget.ParentId;
                        ViewBag.MetaCanonical = zoneTarget.MetaCanonical;
                        ViewBag.MetaNoIndex = zoneTarget.MetaNoIndex;
                        ViewBag.PageIndex = pageIndex;
                        ViewBag.PageSize = pageSize;
                    }
                    else
                    {
                        return Redirect(Utils.BaseBA.UrlCategory(zoneTarget.Url));
                    }
                }

            }
            else
            {
                return NotFound();
            }
            return View();
        }
    }
}