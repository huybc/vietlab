﻿using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Introduce.Controllers
{
    public class AboutController : BaseController
    {
        public IActionResult Index()
        {
            ViewData["IsExpand"] = false;
            return View();
        }
    }
}