﻿using Enterbuy.Statics.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Core.Enumerations
{
    public enum TypeContact
    {
        [EnumDescription("Tất cả")]
        All = 0,
        [EnumDescription("Liên hệ")]
        Guarantee = 1,
        [EnumDescription("Đăng ký đại lý")]
        RegisterAgent = 3,
        [EnumDescription("Đăng ký tư vấn")]
        RegisterAdvisory = 4,
    }
}
