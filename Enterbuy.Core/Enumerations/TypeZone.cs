﻿using Enterbuy.Statics.Helpers;

namespace Enterbuy.Core.Enumerations
{
    public enum TypeZone : byte
    {
        [EnumDescription("Tất cả")]
        All = 0,
        [EnumDescription("Sản phẩm")]
        Product = 1,
        [EnumDescription("Bài viết")]
        Article = 2,
        [EnumDescription("Tuyển dụng")]
        Recruitment = 3,
        [EnumDescription("Khuyến mại")]
        Promotion = 4,
        [EnumDescription("Báo giá")]
        Quotation = 5,
        [EnumDescription("Tải category")]
        Categories = 6,
        [EnumDescription("Vùng hiển thị")]
        Region = 7,
        [EnumDescription("Type bài viết")] // Tat cac cac type ngoai tru san pham va vung hien thi
        AllButProduct = 10,
    }
}