﻿using Enterbuy.Statics.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Core.Enumerations
{
    public enum TypeArticle : byte
    {
        [EnumDescription("Tất cả")]
        All = 0,
        [EnumDescription("Sản phẩm")]
        Product = 1,
        [EnumDescription("Bài viết Nội dung")]
        Blog = 3,
        [EnumDescription("Bài viết Video")]
        Video = 4,
        [EnumDescription("Bài viết hình ảnh")]
        Image = 5,
        [EnumDescription("Bài viết File Download")]
        Download = 6,
    }
}
