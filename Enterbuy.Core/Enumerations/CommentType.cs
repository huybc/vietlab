﻿using Enterbuy.Statics.Helpers;

namespace Enterbuy.Core.Enumerations
{
    public enum CommentType : byte
    {
        [EnumDescription("Tất cả")]
        All = 0,

        [EnumDescription("Sản phẩm")]
        Product = 1,

        [EnumDescription("Bài viết")]
        Article = 2,

        [EnumDescription("Danh mục sản phẩm")]
        ArticleZone = 3,

        [EnumDescription("Danh mục sản phẩm")]
        ProductZone = 4,
    }
}