﻿namespace Enterbuy.Core.Statics
{
    public static class BannerLocationStatic
    {
        public const string BannerTrangchuDocTrai = "Banner_Trangchu_Doc_Trai";
        public const string BannerTrangchuDocPhai = "Banner_Trangchu_Doc_Phai";
        public const string SlideTrangchuTren = "Slide_Trangchu_Tren";
        public const string StemTrangChu = "Stem_Trangchu";
        public const string BannerTrangchuBodyTrenTrai1 = "Banner_Trangchu_body_tren_Trai_1";
        public const string StemChantrang = "Stem_Chantrang";
        public const string Column1 = "ChanTrang_Cot1";
        public const string Column2 = "ChanTrang_Cot2";
        public const string Column3 = "ChanTrang_Cot3";
        public const string ColumnDepartment = "ChanTrang_TruSo";

        public const string Banner_Trangchu_Trencung = "Banner_Trangchu_Trencung";
        public const string Stem_Menu = "Stem_Menu";
        public const string Banner_Trangchu_Ngang2 = "Banner_Trangchu_Ngang2";
        public const string Banner_Hethongdaily = "Banner_Hethongdaily";
        public const string Banner_Khuyenmai_Trangchu = "Banner_Khuyenmai_Trangchu";
        public const string Slide_Body = "Slide_Body";
    }
}