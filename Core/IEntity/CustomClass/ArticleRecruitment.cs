﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Entity.CustomClass
{
    public class ArticleRecruitment
    {
        public string Position { get; set; }
        public string Address { get; set; }
        public string Count { get; set; }
        public string Salary { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
