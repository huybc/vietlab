﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.Entity.Models
{
  public partial class CustomLucky
    {
        public int Id { get; set; }
        public string LuckySpinValue { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LuckySpinName { get; set; }
       
    }
}
