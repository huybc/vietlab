﻿using EFCore.BulkExtensions;
using MI.Dal.IDbContext;
using MI.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MI.Bo.Bussiness
{
    public class ProductInProductBCL : Base<ProductInProduct>
    {
        public ProductInProductBCL()
        {

        }

        public bool Merge(int idProduct, string type, List<ProductInProduct> lstObj)
        {
            try
            {
                using (IDbContext _context = new IDbContext())
                {
                    _context.ProductInProduct.RemoveRange(_context.ProductInProduct.Where(x => x.ProductId == idProduct && x.Type == type));
                    _context.ProductInProduct.AddRange(lstObj);
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                //Logger.WriteLog(Logger.LogType.Error, ex.Message);

                return false;
            }



        }
    }
}
