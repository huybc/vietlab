﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MI.Cache
{
    public class RamCache
    {
        private const int timeCache = 2;

        #region Mail Camp

        public static Dictionary<int, MI.Entity.Models.Coupon> _DicCoupon { get; set; }
        public static DateTime LastestLoadCoupon = new DateTime(2000, 01, 01);

        public static Dictionary<int, MI.Entity.Models.Coupon> DicCoupon
        {
            get
            {
                if (_DicCoupon == null || _DicCoupon.Count <= 0 || LastestLoadCoupon.AddDays(timeCache) < DateTime.Now)
                {
                    _DicCoupon = new MI.Bo.Bussiness.CouponBCL().FindAll().ToDictionary(p => p.Id, p => p);
                    if (_DicCoupon == null)
                    {
                        _DicCoupon = new Dictionary<int, Entity.Models.Coupon>();
                    }
                    LastestLoadCoupon = DateTime.Now;
                }

                return _DicCoupon;
            }
        }

        #endregion Mail Camp

        #region Mail Camp

        public static Dictionary<int, MI.Entity.Models.Zone> _DicZone { get; set; }
        public static DateTime LastestLoadZone = new DateTime(2000, 01, 01);

        public static Dictionary<int, MI.Entity.Models.Zone> DicZone
        {
            get
            {
                if (_DicZone == null || _DicZone.Count <= 0 || LastestLoadZone.AddDays(timeCache) < DateTime.Now)
                {
                    _DicZone = new MI.Bo.Bussiness.ZoneBCL().FindAll(x => x.Status != 3).ToDictionary(p => p.Id, p => p);
                    if (_DicZone == null)
                    {
                        _DicZone = new Dictionary<int, Entity.Models.Zone>();
                    }
                    LastestLoadZone = DateTime.Now;
                }

                return _DicZone;
            }
        }

        #endregion Mail Camp
    }
}
