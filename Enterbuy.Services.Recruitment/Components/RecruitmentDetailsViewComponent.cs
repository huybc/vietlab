﻿using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Services.Blog.Services;
using Enterbuy.Services.Common.Services.Interfaces;
using System;

namespace Enterbuy.Services.Recruitment.Components
{
    public class RecruitmentDetailsViewComponent : ViewComponent
    {
        private readonly IBlogServices _blogServices;
        private readonly IMenuZoneServices _menuZoneServices;
        public RecruitmentDetailsViewComponent(IBlogServices blogServices, IMenuZoneServices menuZoneServices)
        {
            _blogServices = blogServices;
            _menuZoneServices = menuZoneServices;
        }
        public async Task<IViewComponentResult> InvokeAsync(int articleId)
        {
            ArticleDetail article_detail = new ArticleDetail();
            try
            {
                article_detail = _blogServices.GetArticleDetail(articleId, "vi-VN");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            ViewBag.Detail = article_detail;
            return View();
        }
    }
}