using MI.Bo.Bussiness;
using MI.Dal.IDbContext;
using MI.Dapper.Data.Models;
using MI.Dapper.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Hosting;
using OfficeOpenXml;
//using OfficeOpenXml;
//using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VietLab.CMS.ExcelHelper
{
    public interface IExportUtility
    {
        string ExportProductPriceLocationExcel();
        string ImportProductPriceLocationExcel(string path);
    }
    public class ExportUtility : IExportUtility
    {
        LocationBCL locationsBCL;
        ProductBCL productsBCL;
        ProductPriceInLocationBCL productPriceInLocationBCL;
        IProductRepository _productRepository;
        IHostingEnvironment _hostingEnvironment;
        public ExportUtility(IProductRepository productRepository, IHostingEnvironment hostingEnvironment)
        {
            locationsBCL = new LocationBCL();
            productsBCL = new ProductBCL();
            productPriceInLocationBCL = new ProductPriceInLocationBCL();
            _productRepository = productRepository;
            _hostingEnvironment = hostingEnvironment;

        }
        public string ExportProductPriceLocationExcel()
        {

            string rootFolder = _hostingEnvironment.WebRootPath;
            string fileName = @"ExportProductPriceInLocation.xlsx";
            //string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, fileName);
            System.IO.FileInfo file = new System.IO.FileInfo(Path.Combine(rootFolder, fileName));
            if (file.Exists)
            {
                file.Delete();
                file = new System.IO.FileInfo(Path.Combine(rootFolder, fileName));
            }

            var locations = locationsBCL.GetAll();
            Dictionary<int, string> displayLocation = new Dictionary<int, string>();
            foreach (var item in locations.OrderBy(r => r.Id))
            {
                displayLocation.Add(item.Id, item.Name);
            }
            var t1 = 0;
            var list = productsBCL.Get(1, 10, "Id", "asc", out t1);

            using (var excel = new ExcelPackage(file))
            {
                if (list != null)
                {

                    var ls_id = list.Select(p => new { Id = p.Id });
                    var string_int = string.Join(",", ls_id.Select(x => x.Id));
                    //Dung file Excel
                    try
                    {
                        var workSheet = excel.Workbook.Worksheets.Add("Sheet 1");
                        var start = 3;
                        foreach (var item in displayLocation)
                        {
                            var index_excel = 2;
                            var r = _productRepository.ExportExcel(string_int, item.Key);
                            if (start == 3)
                            {
                                workSheet.Cells[1, 1].Value = "Id";
                                workSheet.Cells[1, 2].Value = "Name";
                                //workSheet.Column(1).Style.WrapText = true;
                                //workSheet.Column(2).Style.WrapText = true;


                            }
                            workSheet.Cells[1, start].Value = "[" + item.Key.ToString() + "] " + item.Value;
                            foreach (var p in r)
                            {
                                workSheet.Cells[index_excel, 1].Value = p.Id;
                                workSheet.Cells[index_excel, 2].Value = p.Name;
                                workSheet.Cells[index_excel, start].Value = string.Format("{0},{1},{2}", p.Price, p.SalePrice, p.DiscountPercent);
                                //workSheet.Column(start).Style.WrapText = true;
                                index_excel++;
                            }
                            start++;
                        }
                        workSheet.Cells.AutoFitColumns();
                        excel.Save();

                        return fileName;
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex);
                        return null;
                    }
                }
            }
            return null;
        }
        public string ImportProductPriceLocationExcel(string path)
        {
            //string sWebRootFolder = _hostingEnvironment.WebRootPath;
            //string sFileName = @"ExportProductPriceInLocation.xlsx";
            FileInfo file = new FileInfo(path);
            try
            {
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    int rowCount = worksheet.Dimension.Rows;
                    int colCount = worksheet.Dimension.Columns;
                    DataTable mergeTbl = new DataTable();
                    mergeTbl.Columns.Add("ProductId", typeof(int));
                    mergeTbl.Columns.Add("LocationId", typeof(int));
                    mergeTbl.Columns.Add("Price", typeof(decimal));
                    mergeTbl.Columns.Add("SalePrice", typeof(decimal));
                    mergeTbl.Columns.Add("DiscountPercent", typeof(decimal));
                    for (int col = 3; col <= colCount; col++)
                    {
                        var tt_location = worksheet.Cells[1, col].Value;
                        string pattern = @"\[(\d+)\]";
                        string input = tt_location.ToString();
                        RegexOptions options = RegexOptions.Multiline;
                        var loc_id = 0;
                        var m = Regex.Matches(input, pattern, options).FirstOrDefault();
                        if (m != null)
                        {
                            loc_id = int.Parse(m.Groups[1].Value.ToString());
                        }
                        for (int row = 2; row <= rowCount; row++)
                        {
                            var product_id = worksheet.Cells[row, 1].Value.ToString();
                            var dl_price = worksheet.Cells[row, col].Value.ToString();
                            if (dl_price != "0,0,0" && dl_price.Contains(",") && !string.IsNullOrEmpty(dl_price))
                            {
                                var res = dl_price.Split(",");
                                decimal res_price = 0;
                                decimal res_salePrice = 0;
                                decimal res_discountPercent = 0;
                                if (res.Length >= 1)
                                {
                                    res_price = decimal.Parse(res[0]);
                                }
                                if (res.Length >= 2)
                                {
                                    res_salePrice = decimal.Parse(res[1]);
                                }
                                if (res.Length >= 3)
                                {
                                    res_discountPercent = decimal.Parse(res[2]);
                                }
                                DataRow dr = mergeTbl.NewRow();
                                dr[0] = product_id;
                                dr[1] = loc_id;
                                dr[2] = res_price;
                                dr[3] = res_salePrice;
                                dr[4] = res_discountPercent;
                                mergeTbl.Rows.Add(dr);
                            }
                        }
                    }
                    Console.WriteLine(mergeTbl);
                    var kq = _productRepository.ImportExcelProductPriceInLocation(mergeTbl);

                    return kq;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                return "error";
            }
        }
    }
}
