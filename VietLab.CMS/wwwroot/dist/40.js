webpackJsonp([40],{

/***/ 1072:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(53)();
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", "", {"version":3,"sources":[],"names":[],"mappings":"","file":"edit.vue","sourceRoot":""}]);

// exports


/***/ }),

/***/ 1113:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkBreakpoint = checkBreakpoint;
var sidebarCssClasses = exports.sidebarCssClasses = ['sidebar-show', 'sidebar-sm-show', 'sidebar-md-show', 'sidebar-lg-show', 'sidebar-xl-show'];

var asideMenuCssClasses = exports.asideMenuCssClasses = ['aside-menu-show', 'aside-menu-sm-show', 'aside-menu-md-show', 'aside-menu-lg-show', 'aside-menu-xl-show'];

var validBreakpoints = exports.validBreakpoints = ['sm', 'md', 'lg', 'xl'];

function checkBreakpoint(breakpoint, list) {
  return list.indexOf(breakpoint) > -1;
}

/***/ }),

/***/ 1153:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = __webpack_require__(389);

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = __webpack_require__(8);

var _extends3 = _interopRequireDefault(_extends2);

__webpack_require__(780);

var _constant = __webpack_require__(781);

var _constant2 = _interopRequireDefault(_constant);

var _vuex = __webpack_require__(179);

var _vueLoadingOverlay = __webpack_require__(373);

var _vueLoadingOverlay2 = _interopRequireDefault(_vueLoadingOverlay);

var _list = __webpack_require__(975);

var _list2 = _interopRequireDefault(_list);

var _eventBus = __webpack_require__(964);

var _eventBus2 = _interopRequireDefault(_eventBus);

var _classes = __webpack_require__(1113);

var _debounce = __webpack_require__(52);

var _debounce2 = _interopRequireDefault(_debounce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: "bankinstalledit",
    data: function data() {
        return {
            isLoading: false,
            fullPage: false,
            color: "#007bff",
            currentSort: "Id",
            currentSortDir: "asc",
            monthNumber: "",
            interestRate: "",
            loading: true,
            locationId: 0,
            objRequest: {
                avatar: ""
            },
            langSelected: 1,
            ListType: [],
            KeyImg: -1,
            mikey1: 'mikey1',
            ListMonthNumber: [{ key: 3, value: 3 }, { key: 6, value: 6 }, { key: 9, value: 9 }, { key: 12, value: 12 }],
            infoCardDataDetail: {},
            InfoCardData: {
                InfoCard: []
            }
        };
    },
    created: function created() {
        var vm = this;
        _eventBus2.default.$on('FileSelected', function (value) {});
    },

    components: {
        Loading: _vueLoadingOverlay2.default,
        FileManager: _list2.default
    },
    mounted: function mounted() {
        var _this = this;

        if (this.$route.params.id > 0) {
            this.isLoading = true;
            var initial = this.$route.query.initial;
            initial = typeof initial != "undefined" ? initial.toLowerCase() : "";
            this.getBankInstallment(this.$route.params.id).then(function (respose) {
                _this.objRequest = respose;
                _this.InfoCardData = JSON.parse(respose.infoCard);
                console.log(_this.InfoCardData);
            });
            this.isLoading = false;
        };

        this.getCardType().then(function (respose) {
            _this.ListType = respose;
        });
    },


    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)(["bankinstallment"])),

    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(["getBankInstallment", "createBankInstallment", "getCardType"]), {
        DoAddEdit: function DoAddEdit() {
            var _this2 = this;

            this.isLoading = true;
            var dataInput = this.infoCardDataDetail;
            if (this.InfoCardData.InfoCard.length > 0) {
                for (var index = 0; index < this.InfoCardData.InfoCard.length; index++) {
                    if (this.InfoCardData.InfoCard[index].MonthNumber == dataInput.monthNumber) {
                        this.InfoCardData.InfoCard[index].InterestRate = dataInput.interestRate;
                    }
                }
            } else {
                this.InfoCardData.InfoCard.push(this.infoCardDataDetail);
            }
            this.objRequest.infoCard = (0, _stringify2.default)(this.InfoCardData);
            this.createBankInstallment(this.objRequest).then(function (response) {
                if (response.key == true) {
                    _this2.$toast.success(response.value, {});
                    _this2.isLoading = false;
                } else {
                    _this2.$toast.error(response.value, {});
                    _this2.isLoading = false;
                }
            }).catch(function (e) {
                _this2.$toast.error(_constant2.default.error + ". Error:" + e, {});
                _this2.isLoading = false;
            });
        },
        onChangeSelectd: function onChangeSelectd() {
            if (this.objRequestDetails != null && this.objRequestDetails.length > 0) {
                var lang = this.langSelected || "vi-VN";
                var lstObjLang = this.objRequestDetails.filter(function (item) {
                    return item.languageCode.trim() === lang.trim();
                });
                if (lstObjLang != null && lstObjLang != undefined && lstObjLang.length > 0) {
                    this.objRequestDetail = lstObjLang[0];
                } else {
                    this.objRequestDetail = {};
                    this.objRequestDetail.languageCode = lang;
                }
            } else {
                var _lang = this.langSelected;
                this.objRequestDetail = {};
                this.objRequestDetail.languageCode = _lang;
            }
        },
        openImg: function openImg(img) {

            this.KeyImg = img;
            _eventBus2.default.$emit(this.mikey1, '');
        },
        DoAttackFile: function DoAttackFile(value) {
            var vm = this;
            if (this.KeyImg == 99999) {
                vm.objRequest.avatar = value[0].path;
            } else {
                if (this.KeyImg >= 0) {
                    vm.objRequest[this.KeyImg].avatar = value[0].path;
                }
            }
        },
        pathImgs: function pathImgs(path) {
            var url = 'https://cmsenterbuy.migroup.asia/' + "uploads/thumb" + path;
            return url;
        },
        MonthNumberChange: function MonthNumberChange() {
            var dataInfoCard = this.InfoCardData;
            var data = this.infoCardDataDetail;
            var dataCheck = dataInfoCard.InfoCard;
            if (dataCheck.length > 0) {
                for (var index = 0; index < dataCheck.length; index++) {
                    if (dataCheck[index].MonthNumber == data.monthNumber) {
                        this.infoCardDataDetail.interestRate = dataCheck[index].InterestRate;
                    } else {
                        this.infoCardDataDetail.interestRate = 0;
                    }
                }
            }
        }
    }),
    watch: {
        'infoCardDataDetail.monthNumber': function infoCardDataDetailMonthNumber(newVal) {
            this.MonthNumberChange();
        }
    }
};

/***/ }),

/***/ 1514:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "productadd"
  }, [_c('loading', {
    attrs: {
      "active": _vm.isLoading,
      "height": 35,
      "width": 35,
      "color": _vm.color,
      "is-full-page": _vm.fullPage
    },
    on: {
      "update:active": function($event) {
        _vm.isLoading = $event
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "row productedit"
  }, [_c('div', {
    staticClass: "col-md-12 col-sd-12"
  }, [_c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "card-header"
  }, [_vm._v("\n                    Thông tin chính\n                ")]), _vm._v(" "), _c('div', {
    staticClass: "card-body"
  }, [_c('b-form', {
    staticClass: "form-horizontal"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-2 col-sm-3"
  }, [_c('b-form-group', {
    attrs: {
      "label": "Hình ảnh"
    }
  }, [_c('div', {
    staticStyle: {
      "cursor": "pointer"
    },
    on: {
      "click": function($event) {
        return _vm.openImg(99999)
      }
    }
  }, [_c('div', {
    staticClass: "gallery-upload-file ui-sortable",
    staticStyle: {
      "width": "100%",
      "display": "flex"
    }
  }, [_c('div', {
    staticClass: " r-queue-item ui-sortable-handle",
    staticStyle: {
      "width": "100%",
      "height": "auto",
      "margin": "0"
    }
  }, [(_vm.objRequest.avatar != null && _vm.objRequest.avatar != undefined && _vm.objRequest.avatar.length > 0) ? _c('div', {
    staticStyle: {
      "width": "100%"
    }
  }, [_c('img', {
    staticStyle: {
      "height": "100px",
      "width": "100%"
    },
    attrs: {
      "alt": "Ảnh lỗi",
      "src": _vm.pathImgs(_vm.objRequest.avatar)
    }
  })]) : _c('div', [_c('i', {
    staticClass: "fa fa-picture-o"
  }), _vm._v(" "), _c('p', [_vm._v("[Chọn ảnh]")])])])])])])], 1), _vm._v(" "), _c('div', {
    staticClass: "col-md-10 col-sm-10"
  }, [_c('b-form-group', {
    attrs: {
      "label": "Tên ngân hàng"
    }
  }, [_c('b-form-input', {
    attrs: {
      "placeholder": "Tên ngân hàng",
      "required": ""
    },
    model: {
      value: (_vm.objRequest.name),
      callback: function($$v) {
        _vm.$set(_vm.objRequest, "name", $$v)
      },
      expression: "objRequest.name"
    }
  })], 1), _vm._v(" "), _c('b-form-group', {
    attrs: {
      "label": "Đường dẫn"
    }
  }, [_c('b-form-input', {
    attrs: {
      "placeholder": "Đường dẫn",
      "required": ""
    },
    model: {
      value: (_vm.objRequest.url),
      callback: function($$v) {
        _vm.$set(_vm.objRequest, "url", $$v)
      },
      expression: "objRequest.url"
    }
  })], 1)], 1)]), _vm._v(" "), _c('b-row', [_c('div', {
    staticClass: "col-md-3 col-sm-3"
  }, [_c('b-form-group', {
    attrs: {
      "label": "Loại thẻ"
    }
  }, [_c('v-select', {
    attrs: {
      "options": _vm.ListType,
      "reduce": function (x) { return x.key; },
      "label": "value",
      "placeholder": "Chọn loại thẻ"
    },
    model: {
      value: (_vm.objRequest.type),
      callback: function($$v) {
        _vm.$set(_vm.objRequest, "type", $$v)
      },
      expression: "objRequest.type"
    }
  })], 1)], 1), _vm._v(" "), _c('div', {
    staticClass: "col-md-3 col-sm-3"
  }, [_c('b-form-group', {
    attrs: {
      "label": "Phí quẹt thẻ"
    }
  }, [_c('b-form-input', {
    attrs: {
      "placeholder": "Phí quẹt thẻ",
      "required": ""
    },
    model: {
      value: (_vm.objRequest.charge),
      callback: function($$v) {
        _vm.$set(_vm.objRequest, "charge", $$v)
      },
      expression: "objRequest.charge"
    }
  })], 1)], 1), _vm._v(" "), _c('div', {
    staticClass: "col-md-3 col-sm-3"
  }, [_c('b-form-group', {
    attrs: {
      "label": "Số tháng trả góp"
    }
  }, [_c('v-select', {
    attrs: {
      "options": _vm.ListMonthNumber,
      "reduce": function (x) { return x.key; },
      "label": "value",
      "placeholder": "Chọn số tháng",
      "on-change": _vm.MonthNumberChange
    },
    model: {
      value: (_vm.infoCardDataDetail.monthNumber),
      callback: function($$v) {
        _vm.$set(_vm.infoCardDataDetail, "monthNumber", $$v)
      },
      expression: "infoCardDataDetail.monthNumber"
    }
  })], 1)], 1), _vm._v(" "), _c('div', {
    staticClass: "col-md-3 col-sm-3"
  }, [_c('b-form-group', {
    attrs: {
      "label": "Phần trăm lãi suất"
    }
  }, [_c('b-form-input', {
    attrs: {
      "placeholder": "Phần trăm lãi suất",
      "required": ""
    },
    model: {
      value: (_vm.infoCardDataDetail.interestRate),
      callback: function($$v) {
        _vm.$set(_vm.infoCardDataDetail, "interestRate", $$v)
      },
      expression: "infoCardDataDetail.interestRate"
    }
  })], 1)], 1)])], 1), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-5"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-2"
  }, [_c('button', {
    staticClass: "btn btn-info btn-submit-form col-md-12 btncus",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        return _vm.DoAddEdit()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-save"
  }), _vm._v(" Cập nhật\n                            ")])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-2"
  }, [_c('button', {
    staticClass: "btn btn-success col-md-12 btncus",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        return _vm.DoRefesh()
      }
    }
  }, [_c('i', {
    staticClass: "fa fa-refresh"
  }), _vm._v(" Làm mới\n                            ")])])])], 1)])])]), _vm._v(" "), _c('FileManager', {
    attrs: {
      "miKey": _vm.mikey1
    },
    on: {
      "handleAttackFile": _vm.DoAttackFile
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (true) {
  module.hot.accept()
  if (module.hot.data) {
     __webpack_require__(177).rerender("data-v-df011152", module.exports)
  }
}

/***/ }),

/***/ 1551:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1072);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(785)("14e8e9d2", content, false);
// Hot Module Replacement
if(true) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept(1072, function() {
     var newContent = __webpack_require__(1072);
     if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 726:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1551)

var Component = __webpack_require__(372)(
  /* script */
  __webpack_require__(1153),
  /* template */
  __webpack_require__(1514),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "C:\\Users\\Administrator\\Desktop\\vietlab\\VietLab.CMS\\ClientApp\\pages\\bankinstallment\\edit.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] edit.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (true) {(function () {
  var hotAPI = __webpack_require__(177)
  hotAPI.install(__webpack_require__(27), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-df011152", Component.options)
  } else {
    hotAPI.reload("data-v-df011152", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 779:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(53)();
// imports


// module
exports.push([module.i, ".vld-overlay {\n  bottom: 0;\n  left: 0;\n  position: absolute;\n  right: 0;\n  top: 0;\n  align-items: center;\n  display: none;\n  justify-content: center;\n  overflow: hidden;\n  z-index: 9999;\n}\n\n.vld-overlay.is-active {\n  display: flex;\n}\n\n.vld-overlay.is-full-page {\n  z-index: 9999;\n  position: fixed;\n}\n\n.vld-overlay .vld-background {\n  bottom: 0;\n  left: 0;\n  position: absolute;\n  right: 0;\n  top: 0;\n  background: #fff;\n  opacity: 0.5;\n}\n\n.vld-overlay .vld-icon, .vld-parent {\n  position: relative;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 780:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(779);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(178)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(true) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept(779, function() {
			var newContent = __webpack_require__(779);
			if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 781:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
   value: true
});
var msgNotify = {};
exports.default = msgNotify;

/***/ }),

/***/ 785:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(793)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 786:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(921), __esModule: true };

/***/ }),

/***/ 793:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 921:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(922);
var $Object = __webpack_require__(19).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ 922:
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(32);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(44), 'Object', { defineProperty: __webpack_require__(54).f });


/***/ }),

/***/ 926:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(53)();
// imports


// module
exports.push([module.i, "\n#file-manager[data-v-d3b1cad6] { padding: 0;\n}\n#file-manager___BV_modal_body_[data-v-d3b1cad6] { padding: 0;\n}\n#fm-container-dialog[data-v-d3b1cad6] { padding: 0; overflow: hidden !important;\n}\n#fm-container[data-v-d3b1cad6] { font-size: 12px; font-family: sans-serif; padding: 0\n}\n#fm-toolbar ul[data-v-d3b1cad6] { float: left; margin: 3.5px 0 3px 10px; padding: 1px 2px; border-radius: 0\n}\n#fm-toolbar ul li[data-v-d3b1cad6] { border: 1px solid #fff; cursor: pointer; float: left; line-height: 26px; list-style-type: none; padding: 0 8px; text-align: center;\n}\n#fm-toolbar ul li[data-v-d3b1cad6]:hover { background: #eaeaea;\n}\n#fm-toolbar ul li.active[data-v-d3b1cad6] { background: #eaeaea;\n}\n#fm-container[data-v-d3b1cad6] {\n}\n#fm-toolbar[data-v-d3b1cad6] {\n}\n#fm-footer[data-v-d3b1cad6] { border-top: 1px solid #eaeaea; padding-top: 10px\n}\n#fm-toolbar .tool-items[data-v-d3b1cad6] { clear: both; padding-right: 6px;\n}\n#fm-toolbar .tools[data-v-d3b1cad6] { float: left;\n}\n#fm-main[data-v-d3b1cad6] { margin-top: 8px; border-top: 1px solid #eaeaea;\n}\n#fm-sidebar .fm-header[data-v-d3b1cad6] { height: 35px; line-height: 35px; background-color: #39c; overflow: hidden; font-size: 17px; color: #ecf3f9; text-align: center;\n}\n#fm-sidebar[data-v-d3b1cad6] { border-right: 1px solid #eaeaea\n}\n#fm-content[data-v-d3b1cad6] { background-color: white; cursor: default; /*z-index: 0; box-shadow: 0 2px 4px rgba(0,0,0,0.04), 0 1px 4px rgba(0,0,0,0.12);*/ height: 100%;\n}\n#fm-data-wrapper[data-v-d3b1cad6] { overflow: hidden;\n}\n#fm-file-view[data-v-d3b1cad6] { border-left: 1px solid #eaeaea; float: right; height: 100%; width: 250px; padding: 9px; background: #fff;\n}\n#fm-file-view .file-thumb[data-v-d3b1cad6] { text-align: center; max-height: 250px; max-width: 230px; overflow: hidden;\n}\n#fm-file-view .header[data-v-d3b1cad6] { font-weight: bold; margin-bottom: 12px; padding: 2px 0; text-align: center; text-transform: uppercase;\n}\n#fm-file-view .details[data-v-d3b1cad6] { padding-top: 16px;\n}\n#fm-file-view .details div[data-v-d3b1cad6] { line-height: 21px;\n}\n#fm-file-view .details .uploaded[data-v-d3b1cad6],\n#fm-file-view .details .file-size[data-v-d3b1cad6],\n#fm-file-view .details .dimensions[data-v-d3b1cad6] { line-height: 21px;\n}\n#fm-file-view .file-thumb img[data-v-d3b1cad6] { border: 1px solid #eaeaea; padding: 4px; /*max-height: 220px; max-width: 228px;*/\n}\n.attachment-info .filename[data-v-d3b1cad6] { font-weight: 600; color: #444; word-wrap: break-word;\n}\n.btn-attack[data-v-d3b1cad6] { background: #fff none repeat scroll 0 0; padding: 3px 3px 3px 0; width: 230px; float: right; clear: both;\n}\n.btn-attack button#btn-attack[data-v-d3b1cad6] { color: #fff; background: #0085ba; border: 0; padding: 4px 6px;\n}\n.btn-attack button#dl-close[data-v-d3b1cad6] { background: #eaeaea; border: 0; padding: 4px 6px; float: right; clear: both;\n}\n#fm-content table._list[data-v-d3b1cad6] { width: 100%;\n}\n#fm-content table._list tr[data-v-d3b1cad6] {\n}\n#fm-content table._list tr th[data-v-d3b1cad6] { text-align: center; font-weight: bold;\n}\n#fm-content table._list tr td[data-v-d3b1cad6] { padding: 5px 4px;\n}\n#fm-content table._list tr._active[data-v-d3b1cad6] { background: #48adff;\n}\n#fm-toolbar ul li i[data-v-d3b1cad6] { background-repeat: no-repeat; display: inline-block; height: 16px; vertical-align: middle; width: 16px;\n}\n#fm-toolbar ul li i[data-v-d3b1cad6] {\n}\nli i.create-folder[data-v-d3b1cad6] { background-image: url(" + __webpack_require__(972) + ");\n}\nli i.upload[data-v-d3b1cad6] { background-image: url(" + __webpack_require__(974) + ");\n}\nli i.list[data-v-d3b1cad6] { background-image: url(" + __webpack_require__(969) + ");\n}\nli i.grid[data-v-d3b1cad6] { background-image: url(" + __webpack_require__(968) + ");\n}\nli i.iclose[data-v-d3b1cad6] { background-image: url(" + __webpack_require__(973) + ");\n}\nli i.crop[data-v-d3b1cad6] { background-image: url(" + __webpack_require__(971) + ");\n}\nli i.remove[data-v-d3b1cad6] { background-image: url(" + __webpack_require__(970) + ");\n}\nli i.close[data-v-d3b1cad6]:hover {\n}\n#fm-footer[data-v-d3b1cad6] { min-height: 32px;\n}\n#fm-data-wrapper[data-v-d3b1cad6] { height: 100%;\n}\n.clear[data-v-d3b1cad6] { clear: both;\n}\n#_list ._active[data-v-d3b1cad6] { background: #48adff;\n}\n#fm-content[data-v-d3b1cad6] {\n}\n#fm-content table._list table thead tr th[data-v-d3b1cad6] { text-align: center;\n}\n#fm-content table._list tr[data-v-d3b1cad6] { font-size: 11px;\n}\n#fm-content table._list tr[data-v-d3b1cad6]:hover { background: #c1edff; cursor: pointer;\n}\n#fm-content table._list tr td[data-v-d3b1cad6] {\n}\n#fm-content table._list tbody tr td.name[data-v-d3b1cad6] { display: inline-flex;\n}\n#fm-content table._list tbody tr td.name .list-icon[data-v-d3b1cad6] { display: inline-block; float: left; padding-right: 20px;\n}\n/* #fm-content table._list tbody tr td.name .png, #fm-content table tbody tr td.name .jpg, #fm-content table tbody tr td.name .jpeg, #fm-content table tbody tr td.name .gif,\n#fm-content table._list tbody tr td.name .bmp { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/picture.png) no-repeat; width: 16px; height: 16px; }\n#fm-content table._list tbody tr td.name .doc, #fm-content table tbody tr td.name .docx { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/doc.png) no-repeat; width: 16px; height: 16px; }\n#fm-content table._list tbody tr td.name .xlsx, #fm-content table tbody tr td.name .xls { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/xls.png) no-repeat; width: 16px; height: 16px; }\n#fm-content table._list tbody tr td.name .zip, #fm-content table tbody tr td.name .rar { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/zip.png) no-repeat; width: 16px; height: 16px; }\n#fm-content table._list tbody tr td.name .txt { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/txt.png) no-repeat; width: 16px; height: 16px; }\n#fm-content table._list tbody tr td.name .pdf { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/pdf.png) no-repeat; width: 16px; height: 16px; }\n#fm-content table._list tbody tr td.name { width: 274px; display: inline-block; overflow: hidden; }\n#fm-content table._list tbody tr td.name .fname { display: inline-flex; }\n#fm-content table._list tbody tr td.type, #fm-content table tbody tr td.size, #fm-content table tbody tr td.dimensions, #fm-content table tbody tr td.date { overflow: hidden; width: 98px; }\n#fm-content table._list tbody tr td.size { width: 70px; }\n#fm-content table._list tbody tr td.dimensions { width: 80px; }\n#fm-content table._list tbody tr td.date { width: 115px; } */\n#fm-grid[data-v-d3b1cad6], #fm-folder ul[data-v-d3b1cad6], #fm-grid li .info p[data-v-d3b1cad6] { padding: 0; margin: 0;\n}\n#fm-grid li[data-v-d3b1cad6] { position: relative; vertical-align: middle; display: table-cell; border: 1px solid #eaeaea; cursor: pointer; float: left; height: 100px; min-width: 114px; list-style-type: none; margin: 4px; padding: 2px; width: 15.78%; position: relative; overflow: hidden; text-align: center;\n}\n#fm-grid li .info[data-v-d3b1cad6] { font-size: 10px; background: rgba(109,105,105,0.45882); position: absolute; bottom: 0; width: 100%; height: 0\n}\n#fm-grid li:hover .info[data-v-d3b1cad6] { height: auto\n}\n.fm-list li img[data-v-d3b1cad6] { width: 100%\n}\n.fm-list li.not_img img[data-v-d3b1cad6] { width: 50%; padding-top: 10px\n}\n#fm-grid li img.thumb[data-v-d3b1cad6] { width: 60%; margin: 0 auto;\n}\n#fm-grid li i[data-v-d3b1cad6] { position: absolute; z-index: 999; right: 4px; background: #fff; border-radius: 11px; color: #0085ba; padding: 1px; top: 3px; display: none;\n}\n#fm-grid li._active i[data-v-d3b1cad6] { display: block;\n}\n#fm-grid li._active[data-v-d3b1cad6] { border: 1px solid #0085ba;\n}\n#fm-folder ul li[data-v-d3b1cad6] { list-style-type: none; padding: 8px 0; cursor: pointer\n}\n#fm-folder ul li i[data-v-d3b1cad6] { background-repeat: no-repeat; display: inline-block; height: 16px; vertical-align: middle; width: 16px;\n}\n.plupload_filelist_header[data-v-d3b1cad6] { height: 20px;\n}\n.plupload_filelist_footer[data-v-d3b1cad6] { padding: 6px 20px; height: 33px;\n}\n.plupload_scroll .plupload_filelist[data-v-d3b1cad6] { height: 172px !important;\n}\n.plupload_filelist_footer[data-v-d3b1cad6] { padding: 6px 20px;\n}\n.plupload_filelist[data-v-d3b1cad6]:empty,\n.plupload_filelist li.plupload_droptext[data-v-d3b1cad6] { height: 140px;\n}\n.plupload_filelist[data-v-d3b1cad6]:empty::before,\n.plupload_filelist li.plupload_droptext[data-v-d3b1cad6]::before { font-size: 52px; height: 75px; left: 49%; margin-left: -40px; padding-top: 43px;\n}\n.items-action[data-v-d3b1cad6] { padding-top: 20px; display: inline-block; width: 100%;\n}\n.items-action .file-action[data-v-d3b1cad6] { border: 0 none; color: #fff; padding: 4px 10px;\n}\n.items-action .file-action[data-v-d3b1cad6] { background: rosybrown; float: left; clear: both;\n}\n/*.items-action .file-action:last-child { background: #0085ba;float: right;}*/\n#fm-toolbar .Mi.ipagination a[data-v-d3b1cad6]:first-child { border: 0; border-radius: 0;\n}\n#fm-toolbar .Mi.ipagination input[data-v-d3b1cad6] { border: medium none; float: left; font-family: arial; font-size: 11px; height: 30px; margin: 0; outline: medium none; padding: 0; text-align: center; vertical-align: middle; width: 84px;\n}\n#fm-toolbar .Mi.ipagination a[data-v-d3b1cad6] { border: 0; border-radius: 0; background: #fff;\n}\n#fm-toolbar .ipagination.iweb[data-v-d3b1cad6] { margin-top: 3.45px;\n}\n#frowInTotals[data-v-d3b1cad6] { display: inline-block; float: left; line-height: 39px; padding-right: 3px; vertical-align: baseline;\n}\nlabel#progressall[data-v-d3b1cad6] { display: none; padding: 7px 7px;\n}\n.Cdiv[data-v-d3b1cad6] { padding-left: 12px; height: 17px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; display: inline-block; font-weight: normal;\n}\n#btn-fm-upload label[data-v-d3b1cad6] { margin-bottom: 0 !important;\n}\n/* Extra small devices (phones, 600px and down) */\n@media only screen and (max-width: 600px) {\n#fm-grid li[data-v-d3b1cad6] { width: 47.65%;\n}\n}\n\n/* Small devices (portrait tablets and large phones, 600px and up) */\n@media only screen and (min-width: 600px) {\n}\n\n/* Medium devices (landscape tablets, 768px and up) */\n@media only screen and (min-width: 768px) {\n}\n\n/* Large devices (laptops/desktops, 992px and up) */\n@media only screen and (min-width: 992px) {\n}\n\n/* Extra large devices (large laptops and desktops, 1200px and up) */\n@media only screen and (min-width: 1200px) {\n}\n", "", {"version":3,"sources":["C:/Users/Administrator/Desktop/vietlab/VietLab.CMS/ClientApp/components/fileManager/list.vue?12ece3a9"],"names":[],"mappings":";AA0UA,iCAAA,WAAA;CAAA;AACA,kDAAA,WAAA;CAAA;AAEA,wCAAA,WAAA,CAAA,4BAAA;CAAA;AACA,iCAAA,gBAAA,CAAA,wBAAA,CAAA,UAAA;CAAA;AACA,kCAAA,YAAA,CAAA,yBAAA,CAAA,iBAAA,CAAA,gBAAA;CAAA;AACA,qCAAA,uBAAA,CAAA,gBAAA,CAAA,YAAA,CAAA,kBAAA,CAAA,sBAAA,CAAA,eAAA,CAAA,mBAAA;CAAA;AACA,2CAAA,oBAAA;CAAA;AACA,4CAAA,oBAAA;CAAA;AACA;CAAA;AACA;CAAA;AACA,8BAAA,8BAAA,CAAA,iBAAA;CAAA;AACA,2CAAA,YAAA,CAAA,mBAAA;CAAA;AACA,sCAAA,YAAA;CAAA;AACA,4BAAA,gBAAA,CAAA,8BAAA;CAAA;AACA,0CAAA,aAAA,CAAA,kBAAA,CAAA,uBAAA,CAAA,iBAAA,CAAA,gBAAA,CAAA,eAAA,CAAA,mBAAA;CAAA;AACA,+BAAA,+BAAA;CAAA;AACA,+BAAA,wBAAA,CAAA,gBAAA,CAAA,mFAAA,CAAA,aAAA;CAAA;AACA,oCAAA,iBAAA;CAAA;AACA,iCAAA,+BAAA,CAAA,aAAA,CAAA,aAAA,CAAA,aAAA,CAAA,aAAA,CAAA,iBAAA;CAAA;AACA,6CAAA,mBAAA,CAAA,kBAAA,CAAA,iBAAA,CAAA,iBAAA;CAAA;AACA,yCAAA,kBAAA,CAAA,oBAAA,CAAA,eAAA,CAAA,mBAAA,CAAA,0BAAA;CAAA;AACA,0CAAA,kBAAA;CAAA;AACA,8CAAA,kBAAA;CAAA;AACA;;sDAEA,kBAAA;CAAA;AACA,iDAAA,0BAAA,CAAA,aAAA,CAAA,wCAAA;CAAA;AACA,8CAAA,iBAAA,CAAA,YAAA,CAAA,sBAAA;CAAA;AACA,+BAAA,wCAAA,CAAA,uBAAA,CAAA,aAAA,CAAA,aAAA,CAAA,YAAA;CAAA;AACA,iDAAA,YAAA,CAAA,oBAAA,CAAA,UAAA,CAAA,iBAAA;CAAA;AACA,+CAAA,oBAAA,CAAA,UAAA,CAAA,iBAAA,CAAA,aAAA,CAAA,YAAA;CAAA;AACA,2CAAA,YAAA;CAAA;AACA;CAAA;AACA,iDAAA,mBAAA,CAAA,kBAAA;CAAA;AACA,iDAAA,iBAAA;CAAA;AACA,sDAAA,oBAAA;CAAA;AACA,uCAAA,6BAAA,CAAA,sBAAA,CAAA,aAAA,CAAA,uBAAA,CAAA,YAAA;CAAA;AACA;CAAA;AACA,sCAAA,gDAAA;CAAA;AACA,+BAAA,gDAAA;CAAA;AACA,6BAAA,gDAAA;CAAA;AACA,6BAAA,gDAAA;CAAA;AACA,+BAAA,gDAAA;CAAA;AACA,6BAAA,gDAAA;CAAA;AACA,+BAAA,gDAAA;CAAA;AACA;CAAA;AACA,8BAAA,iBAAA;CAAA;AACA,oCAAA,aAAA;CAAA;AACA,0BAAA,YAAA;CAAA;AACA,mCAAA,oBAAA;CAAA;AACA;CAAA;AACA,6DAAA,mBAAA;CAAA;AACA,8CAAA,gBAAA;CAAA;AACA,oDAAA,oBAAA,CAAA,gBAAA;CAAA;AACA;CAAA;AACA,4DAAA,qBAAA;CAAA;AACA,uEAAA,sBAAA,CAAA,YAAA,CAAA,oBAAA;CAAA;AACA;;;;;;;;;;;;6DAYA;AAEA,kGAAA,WAAA,CAAA,UAAA;CAAA;AACA,+BAAA,mBAAA,CAAA,uBAAA,CAAA,oBAAA,CAAA,0BAAA,CAAA,gBAAA,CAAA,YAAA,CAAA,cAAA,CAAA,iBAAA,CAAA,sBAAA,CAAA,YAAA,CAAA,aAAA,CAAA,cAAA,CAAA,mBAAA,CAAA,iBAAA,CAAA,mBAAA;CAAA;AACA,qCAAA,gBAAA,CAAA,sCAAA,CAAA,mBAAA,CAAA,UAAA,CAAA,YAAA,CAAA,SAAA;CAAA;AACA,2CAAA,YAAA;CAAA;AACA,mCAAA,WAAA;CAAA;AACA,2CAAA,WAAA,CAAA,iBAAA;CAAA;AACA,yCAAA,WAAA,CAAA,eAAA;CAAA;AACA,iCAAA,mBAAA,CAAA,aAAA,CAAA,WAAA,CAAA,iBAAA,CAAA,oBAAA,CAAA,eAAA,CAAA,aAAA,CAAA,SAAA,CAAA,cAAA;CAAA;AACA,yCAAA,eAAA;CAAA;AACA,uCAAA,0BAAA;CAAA;AACA,oCAAA,sBAAA,CAAA,eAAA,CAAA,eAAA;CAAA;AACA,sCAAA,6BAAA,CAAA,sBAAA,CAAA,aAAA,CAAA,uBAAA,CAAA,YAAA;CAAA;AACA,6CAAA,aAAA;CAAA;AACA,6CAAA,kBAAA,CAAA,aAAA;CAAA;AACA,uDAAA,yBAAA;CAAA;AACA,6CAAA,kBAAA;CAAA;AACA;2DACA,cAAA;CAAA;AAEA;mEACA,gBAAA,CAAA,aAAA,CAAA,UAAA,CAAA,mBAAA,CAAA,kBAAA;CAAA;AACA,iCAAA,kBAAA,CAAA,sBAAA,CAAA,YAAA;CAAA;AACA,8CAAA,eAAA,CAAA,YAAA,CAAA,kBAAA;CAAA;AACA,8CAAA,sBAAA,CAAA,YAAA,CAAA,YAAA;CAAA;AACA,8EAAA;AACA,6DAAA,UAAA,CAAA,iBAAA;CAAA;AACA,qDAAA,oBAAA,CAAA,YAAA,CAAA,mBAAA,CAAA,gBAAA,CAAA,aAAA,CAAA,UAAA,CAAA,qBAAA,CAAA,WAAA,CAAA,mBAAA,CAAA,uBAAA,CAAA,YAAA;CAAA;AACA,iDAAA,UAAA,CAAA,iBAAA,CAAA,iBAAA;CAAA;AACA,iDAAA,mBAAA;CAAA;AACA,iCAAA,sBAAA,CAAA,YAAA,CAAA,kBAAA,CAAA,mBAAA,CAAA,yBAAA;CAAA;AACA,qCAAA,cAAA,CAAA,iBAAA;CAAA;AACA,yBAAA,mBAAA,CAAA,aAAA,CAAA,iBAAA,CAAA,wBAAA,CAAA,oBAAA,CAAA,YAAA,CAAA,sBAAA,CAAA,oBAAA;CAAA;AACA,wCAAA,4BAAA;CAAA;AACA,kDAAA;AAEA;AACA,+BAAA,cAAA;CAAA;CACA;;AAEA,qEAAA;AAEA;CACA;;AAEA,sDAAA;AAEA;CACA;;AAEA,oDAAA;AAEA;CACA;;AAEA,qEAAA;AAEA;CACA","file":"list.vue","sourcesContent":["<template>\r\n    <div class=\"file-manager\">\r\n        <loading :active.sync=\"isLoading\"\r\n                 :height=\"35\"\r\n                 :width=\"35\"\r\n                 :color=\"color\"\r\n                 :is-full-page=\"fullPage\"></loading>\r\n        <b-modal ref=\"file-manager-modal\" id=\"file-manager\" fbody=\"xxx\" hide-footer hide-header size=\"xl\">\r\n            <div id=\"fm-container\" class=\"container-fluid\">\r\n                <b-row id=\"fm-toolbar\">\r\n                    <b-col lg=\"2\" md=\"2\" sm=\"12\">\r\n                        <b>Thư viện</b>\r\n                    </b-col>\r\n                    <b-col>\r\n                        <b-row align-h=\"between\">\r\n                            <b-col cols=\"4\">\r\n                                <div class=\"tool-items\">\r\n                                    <ul class=\"tools\">\r\n                                        <li title=\"Create folder\">\r\n                                            <i class=\"create-folder\"></i>\r\n                                        </li>\r\n                                    </ul>\r\n                                    <ul class=\"tools\">\r\n                                        <li title=\"Remove\">\r\n                                            <i class=\"remove\"></i>\r\n                                        </li>\r\n                                    </ul>\r\n                                </div>\r\n                            </b-col>\r\n                            <b-col cols=\"4\" class=\"bd-0\">\r\n                                <b-pagination v-model=\"currentPage\" align=\"right\" size=\"sm\" :limit=\"4\"\r\n                                              :total-rows=\"files.totals\"\r\n                                              :per-page=\"pageSize\"></b-pagination>\r\n                            </b-col>\r\n                            <b-col cols=\"4\">\r\n                                <div class=\"tool-items float-right\">\r\n\r\n                                    <ul class=\"tools\">\r\n                                        <li title=\"Create folder\">\r\n                                            <i class=\"create-folder\"></i>\r\n                                        </li>\r\n                                        <li title=\"Upload\" id=\"btn-fm-upload\">\r\n                                            <label for=\"fileSingleupload\">\r\n                                                <i class=\"upload\"></i>\r\n                                            </label>\r\n                                            <input accept=\"image/*,.doc,.docx,.pdf,.xls,.xlsx,.zip,.rar\" @change=\"DoUploadFile\" id=\"fileSingleupload\"\r\n                                                   multiple\r\n                                                   type=\"file\"\r\n                                                   name=\"files[]\"\r\n                                                   style=\"display: none\" />\r\n                                        </li>\r\n                                    </ul>\r\n                                    <ul class=\"tools\">\r\n                                        <li title=\"List view\">\r\n                                            <i class=\"list\"></i>\r\n                                        </li>\r\n                                        <li title=\"Grid View\">\r\n                                            <i class=\"grid\"></i>\r\n                                        </li>\r\n                                    </ul>\r\n                                </div>\r\n                            </b-col>\r\n                        </b-row>\r\n\r\n                    </b-col>\r\n\r\n                </b-row>\r\n                <b-row id=\"fm-main\">\r\n                    <b-col id=\"fm-sidebar\" lg=\"2\" md=\"2\" sm=\"12\">\r\n                        <div id=\"fm-folder\">\r\n                            <ul>\r\n                                <li><i class=\"create-folder\"></i> Document</li>\r\n                                <li><i class=\"create-folder\"></i> Image</li>\r\n                                <li><i class=\"create-folder\"></i> Icon</li>\r\n                            </ul>\r\n                        </div>\r\n                    </b-col>\r\n                    <b-col id=\"fm-content\" lg=\"10\" dm=\"2\" sm=\"12\">\r\n                        <div id=\"fm-data-wrapper\">\r\n                            <div class=\"fm-list-wrapper\">\r\n                                <div class=\"fm-list\">\r\n                                    <ul id=\"fm-grid\">\r\n                                        <li v-for=\"file in files.files\" :key=\"file.id\" class=\"item\" :class=\"{ _active: isActive, not_img:checkImage(file.fileExt)}\" @click=\"SelectFile($event,{path:file.filePath,id:file.id})\">\r\n                                            <img :src=\"mapImageUrl(file.filePath,file.fileExt)\" alt=\"\">\r\n                                            <i class=\"fa fa-check\"></i>\r\n                                            <div class=\"info\">\r\n                                                <p class=\"name\">{{file.name}}</p>\r\n                                                <p class=\"dimensions\">{{file.dimensions}}</p>\r\n                                                <p class=\"size\">{{file.fileSize}}kb</p>\r\n                                            </div>\r\n                                        </li>\r\n                                    </ul>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </b-col>\r\n                </b-row>\r\n                <b-row align-h=\"end\" id=\"fm-footer\">\r\n                    <b-col cols=\"4\">\r\n                        <div class=\"btn-attack\">\r\n                            <button type=\"button\" id=\"btn-attack\" @click=\"attackFile\">Đính kèm</button>\r\n                            <!--<button type=\"button\" id=\"btn-attack\" @click=\"attackFileSelected\">Đính kèm2</button>-->\r\n                            <button type=\"button\" id=\"dl-close\" @click=\"hideFileManagerModal\" class=\"iclose\">Đóng</button>\r\n                        </div>\r\n                    </b-col>\r\n                </b-row>\r\n            </div>\r\n        </b-modal>\r\n    </div>\r\n</template>\r\n\r\n<script>\r\n    import \"vue-loading-overlay/dist/vue-loading.css\";\r\n    import msgNotify from \"./../../common/constant\";\r\n    import {mapActions } from \"vuex\";\r\n    import Loading from \"vue-loading-overlay\";\r\n    import EventBus from \"./../../common/eventBus\";\r\n\r\n\r\n    export default {\r\n        name: \"FileManager\",\r\n        props: {\r\n            miKey: {\r\n                type: String\r\n            }\r\n        },\r\n        components: {\r\n            Loading\r\n        },\r\n        data() {\r\n            return {\r\n                isLoading: false,\r\n                fullPage: false,\r\n                color: \"#007bff\",\r\n                isLoadLang: false,\r\n                //  perPage: 10,\r\n                currentPage: 1,\r\n                pageSize: 30,\r\n                isLoading: false,\r\n                MaxFileSize: 3000,//Kb\r\n                selectedFile: [],\r\n                extensions: [],\r\n                extImage: [],\r\n                isActive: false,\r\n                selectType: ''\r\n            };\r\n        },\r\n        created() {\r\n            let config = require('./../../../appsettings.json');\r\n            this.extImage = config.AppSettings.ImageAllowUpload;\r\n            this.extensions = config.AppSettings.ImageAllowUpload.concat(config.AppSettings.DocumentAllowUpload);\r\n\r\n            EventBus.$on(this.miKey, this.FileManagerOpen);\r\n        },\r\n        destroyed() {\r\n\r\n        },\r\n        computed: {\r\n            files() {\r\n                return this.$store.getters.files\r\n            }\r\n        },\r\n        methods: {\r\n            ...mapActions([\"fmFileUpload\", \"fmFileGetAll\"]),\r\n            mapImageUrl(img, ext) {\r\n                if (this.extImage.indexOf(ext) !== -1) {\r\n                    return '/uploads/thumb' + img;\r\n                }\r\n                return './../../ClientApp/assets/fileicons/' + ext.replace('.', '') + '.png';\r\n            },\r\n            checkImage(ext) {\r\n                if (this.extImage.indexOf(ext) == -1) {\r\n                    return true\r\n                }\r\n                return false\r\n            },\r\n            DoUploadFile(e) {\r\n                let files = e.srcElement.files;\r\n\r\n                if (files) {\r\n                    let filesTemp = Array.from(files);\r\n\r\n                    let msgFileAllow = '';\r\n                    let msgLimitedSize = '';\r\n                    for (var i = 0; i < filesTemp.length; i++) {\r\n\r\n                        let name = filesTemp[i].name;\r\n                        let type = name.split('.').pop();\r\n                        if (this.extensions.indexOf(type) == -1) {\r\n                            filesTemp.splice(i, 1); // xóa khỏi array\r\n                            if (msgFileAllow.length == 0) {\r\n                                msgFileAllow = name;\r\n                            } else {\r\n                                msgFileAllow += ', ' + name;\r\n                            }\r\n                        }\r\n                        if (msgFileAllow.length > 0) {\r\n                            this.$toast.error(msgFileAllow + ' không hợp lệ !', {});\r\n                        }\r\n                    }\r\n                    for (var i = 0; i < filesTemp.length; i++) {\r\n                       \r\n\r\n                        let size = filesTemp[i].size;\r\n                        let name = filesTemp[i].name;\r\n\r\n\r\n                        if (this.MaxFileSize < (size / 1024)) {\r\n                            filesTemp.splice(i, 1);\r\n                            if (msgLimitedSize.length == 0) {\r\n                                msgLimitedSize = name;\r\n                            } else {\r\n                                msgLimitedSize += ', ' + name;\r\n                            }\r\n\r\n                        }\r\n                        if (msgLimitedSize.length > 0) {\r\n                            this.$toast.error(msgFileAllow + ' dung lượng quá lớn !', {});\r\n                        }\r\n\r\n\r\n                    }\r\n                    if (filesTemp.length) {\r\n                        let fd = new FormData();\r\n\r\n                        filesTemp.forEach(function (item) {\r\n                            fd.append('files', item);\r\n                        });\r\n                        this.UploadFileAction(fd);\r\n                    }\r\n\r\n                }\r\n\r\n            },\r\n            UploadFileAction(files) {\r\n                this.fmFileUpload(files)\r\n                    .then(response => {\r\n                        if (response.success) {\r\n                            this.fmFileGetAll({\r\n                                // code tam thoi\r\n                                pageIndex: this.currentPage,\r\n                                pageSize: this.pageSize\r\n                            });\r\n                            this.$toast.success(response.Message, {});\r\n                            this.isLoading = false;\r\n                        }\r\n                        else {\r\n                            this.$toast.error(response.Message, {});\r\n                            this.isLoading = false;\r\n                        }\r\n                    })\r\n                    .catch(e => {\r\n                        this.$toast.error(msgNotify.error + \". Error:\" + e, {});\r\n                        this.isLoading = false;\r\n                    });\r\n\r\n\r\n            },\r\n            SelectFile(event, file) {\r\n                if (this.selectType == 'multi') {\r\n                    if (event.currentTarget.classList.contains('_active')) {\r\n                        event.currentTarget.classList.remove('_active');\r\n\r\n                       // let objIsExist = this.selectedFile.some(obj => obj.id = file.id);\r\n                       // if (objIsExist) {\r\n                            this.selectedFile = this.selectedFile.filter(obj => obj.id != file.id)\r\n                       // }\r\n                    }\r\n                    else {\r\n                        event.currentTarget.classList.add(\"_active\");\r\n                        this.selectedFile.push(file)\r\n                    }\r\n                } else {\r\n\r\n                    this.selectedFile = [];\r\n                    if (event.currentTarget.classList.contains('_active')) {\r\n                        event.currentTarget.classList.remove('_active');\r\n\r\n                    }\r\n                    else {\r\n                        let items = document.querySelectorAll('.item');\r\n                        items.forEach(function (item) {\r\n                            item.classList.remove('_active');\r\n                        });\r\n                        event.currentTarget.classList.add(\"_active\");\r\n                        this.selectedFile.push(file)\r\n                    }\r\n                }\r\n            },\r\n\r\n            FileManagerOpen(param) {\r\n                this.selectType = param;\r\n                this.$refs[\"file-manager-modal\"].show();\r\n                this.fmFileGetAll({\r\n                    pageIndex: this.currentPage,\r\n                    pageSize: this.pageSize\r\n                });\r\n            },\r\n\r\n            hideFileManagerModal() {\r\n                this.$refs[\"file-manager-modal\"].hide();\r\n            },\r\n            attackFile() {\r\n               // EventBus.$emit(\"FileSelected\", this.selectedFile);\r\n                this.$emit(\"handleAttackFile\", this.selectedFile);\r\n                this.$refs[\"file-manager-modal\"].hide();\r\n                this.selectedFile = [];\r\n            },\r\n            \r\n            toggleFileModal() {\r\n                // We pass the ID of the button that we want to return focus to\r\n                // when the modal has hidden\r\n                this.$refs[\"file-manager-modal\"].toggle(\"#toggle-btn\");\r\n            }\r\n        },\r\n        mounted() {\r\n            //   this.fmFileGetAll();\r\n        },\r\n        watch: {\r\n            currentPage() {\r\n                this.fmFileGetAll({\r\n                    pageIndex: this.currentPage,\r\n                    pageSize: this.pageSize\r\n                });\r\n            }\r\n        }\r\n    };\r\n</script>\r\n<style scoped>\r\n    #file-manager { padding: 0; }\r\n    #file-manager___BV_modal_body_ { padding: 0; }\r\n\r\n    #fm-container-dialog { padding: 0; overflow: hidden !important; }\r\n    #fm-container { font-size: 12px; font-family: sans-serif; padding: 0 }\r\n    #fm-toolbar ul { float: left; margin: 3.5px 0 3px 10px; padding: 1px 2px; border-radius: 0 }\r\n    #fm-toolbar ul li { border: 1px solid #fff; cursor: pointer; float: left; line-height: 26px; list-style-type: none; padding: 0 8px; text-align: center; }\r\n    #fm-toolbar ul li:hover { background: #eaeaea; }\r\n    #fm-toolbar ul li.active { background: #eaeaea; }\r\n    #fm-container { }\r\n    #fm-toolbar { }\r\n    #fm-footer { border-top: 1px solid #eaeaea; padding-top: 10px }\r\n    #fm-toolbar .tool-items { clear: both; padding-right: 6px; }\r\n    #fm-toolbar .tools { float: left; }\r\n    #fm-main { margin-top: 8px; border-top: 1px solid #eaeaea; }\r\n    #fm-sidebar .fm-header { height: 35px; line-height: 35px; background-color: #39c; overflow: hidden; font-size: 17px; color: #ecf3f9; text-align: center; }\r\n    #fm-sidebar { border-right: 1px solid #eaeaea }\r\n    #fm-content { background-color: white; cursor: default; /*z-index: 0; box-shadow: 0 2px 4px rgba(0,0,0,0.04), 0 1px 4px rgba(0,0,0,0.12);*/ height: 100%; }\r\n    #fm-data-wrapper { overflow: hidden; }\r\n    #fm-file-view { border-left: 1px solid #eaeaea; float: right; height: 100%; width: 250px; padding: 9px; background: #fff; }\r\n    #fm-file-view .file-thumb { text-align: center; max-height: 250px; max-width: 230px; overflow: hidden; }\r\n    #fm-file-view .header { font-weight: bold; margin-bottom: 12px; padding: 2px 0; text-align: center; text-transform: uppercase; }\r\n    #fm-file-view .details { padding-top: 16px; }\r\n    #fm-file-view .details div { line-height: 21px; }\r\n    #fm-file-view .details .uploaded,\r\n    #fm-file-view .details .file-size,\r\n    #fm-file-view .details .dimensions { line-height: 21px; }\r\n    #fm-file-view .file-thumb img { border: 1px solid #eaeaea; padding: 4px; /*max-height: 220px; max-width: 228px;*/ }\r\n    .attachment-info .filename { font-weight: 600; color: #444; word-wrap: break-word; }\r\n    .btn-attack { background: #fff none repeat scroll 0 0; padding: 3px 3px 3px 0; width: 230px; float: right; clear: both; }\r\n    .btn-attack button#btn-attack { color: #fff; background: #0085ba; border: 0; padding: 4px 6px; }\r\n    .btn-attack button#dl-close { background: #eaeaea; border: 0; padding: 4px 6px; float: right; clear: both; }\r\n    #fm-content table._list { width: 100%; }\r\n    #fm-content table._list tr { }\r\n    #fm-content table._list tr th { text-align: center; font-weight: bold; }\r\n    #fm-content table._list tr td { padding: 5px 4px; }\r\n    #fm-content table._list tr._active { background: #48adff; }\r\n    #fm-toolbar ul li i { background-repeat: no-repeat; display: inline-block; height: 16px; vertical-align: middle; width: 16px; }\r\n    #fm-toolbar ul li i { }\r\n    li i.create-folder { background-image: url(./../../assets/img/icon/folder_add.png); }\r\n    li i.upload { background-image: url(\"./../../assets/img/icon/upload.png\"); }\r\n    li i.list { background-image: url(\"./../../assets/img/icon/application_view_list.png\"); }\r\n    li i.grid { background-image: url(\"./../../assets/img/icon/application_view_icons.png\"); }\r\n    li i.iclose { background-image: url(\"./../../assets/img/icon/reset.png\"); }\r\n    li i.crop { background-image: url(\"./../../assets/img/icon/crop.png\"); }\r\n    li i.remove { background-image: url(\"./../../assets/img/icon/bin_closed.png\"); }\r\n    li i.close:hover { }\r\n    #fm-footer { min-height: 32px; }\r\n    #fm-data-wrapper { height: 100%; }\r\n    .clear { clear: both; }\r\n    #_list ._active { background: #48adff; }\r\n    #fm-content { }\r\n    #fm-content table._list table thead tr th { text-align: center; }\r\n    #fm-content table._list tr { font-size: 11px; }\r\n    #fm-content table._list tr:hover { background: #c1edff; cursor: pointer; }\r\n    #fm-content table._list tr td { }\r\n    #fm-content table._list tbody tr td.name { display: inline-flex; }\r\n    #fm-content table._list tbody tr td.name .list-icon { display: inline-block; float: left; padding-right: 20px; }\r\n    /* #fm-content table._list tbody tr td.name .png, #fm-content table tbody tr td.name .jpg, #fm-content table tbody tr td.name .jpeg, #fm-content table tbody tr td.name .gif,\r\n    #fm-content table._list tbody tr td.name .bmp { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/picture.png) no-repeat; width: 16px; height: 16px; }\r\n    #fm-content table._list tbody tr td.name .doc, #fm-content table tbody tr td.name .docx { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/doc.png) no-repeat; width: 16px; height: 16px; }\r\n    #fm-content table._list tbody tr td.name .xlsx, #fm-content table tbody tr td.name .xls { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/xls.png) no-repeat; width: 16px; height: 16px; }\r\n    #fm-content table._list tbody tr td.name .zip, #fm-content table tbody tr td.name .rar { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/zip.png) no-repeat; width: 16px; height: 16px; }\r\n    #fm-content table._list tbody tr td.name .txt { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/txt.png) no-repeat; width: 16px; height: 16px; }\r\n    #fm-content table._list tbody tr td.name .pdf { background: url(/CMS/Modules/FileManager/Libs/Images/filetree/pdf.png) no-repeat; width: 16px; height: 16px; }\r\n    #fm-content table._list tbody tr td.name { width: 274px; display: inline-block; overflow: hidden; }\r\n    #fm-content table._list tbody tr td.name .fname { display: inline-flex; }\r\n    #fm-content table._list tbody tr td.type, #fm-content table tbody tr td.size, #fm-content table tbody tr td.dimensions, #fm-content table tbody tr td.date { overflow: hidden; width: 98px; }\r\n    #fm-content table._list tbody tr td.size { width: 70px; }\r\n    #fm-content table._list tbody tr td.dimensions { width: 80px; }\r\n    #fm-content table._list tbody tr td.date { width: 115px; } */\r\n\r\n    #fm-grid, #fm-folder ul, #fm-grid li .info p { padding: 0; margin: 0; }\r\n    #fm-grid li { position: relative; vertical-align: middle; display: table-cell; border: 1px solid #eaeaea; cursor: pointer; float: left; height: 100px; min-width: 114px; list-style-type: none; margin: 4px; padding: 2px; width: 15.78%; position: relative; overflow: hidden; text-align: center; }\r\n    #fm-grid li .info { font-size: 10px; background: rgba(109,105,105,0.45882); position: absolute; bottom: 0; width: 100%; height: 0 }\r\n    #fm-grid li:hover .info { height: auto }\r\n    .fm-list li img { width: 100% }\r\n    .fm-list li.not_img img { width: 50%; padding-top: 10px }\r\n    #fm-grid li img.thumb { width: 60%; margin: 0 auto; }\r\n    #fm-grid li i { position: absolute; z-index: 999; right: 4px; background: #fff; border-radius: 11px; color: #0085ba; padding: 1px; top: 3px; display: none; }\r\n    #fm-grid li._active i { display: block; }\r\n    #fm-grid li._active { border: 1px solid #0085ba; }\r\n    #fm-folder ul li { list-style-type: none; padding: 8px 0; cursor: pointer }\r\n    #fm-folder ul li i { background-repeat: no-repeat; display: inline-block; height: 16px; vertical-align: middle; width: 16px; }\r\n    .plupload_filelist_header { height: 20px; }\r\n    .plupload_filelist_footer { padding: 6px 20px; height: 33px; }\r\n    .plupload_scroll .plupload_filelist { height: 172px !important; }\r\n    .plupload_filelist_footer { padding: 6px 20px; }\r\n    .plupload_filelist:empty,\r\n    .plupload_filelist li.plupload_droptext { height: 140px; }\r\n\r\n    .plupload_filelist:empty::before,\r\n    .plupload_filelist li.plupload_droptext::before { font-size: 52px; height: 75px; left: 49%; margin-left: -40px; padding-top: 43px; }\r\n    .items-action { padding-top: 20px; display: inline-block; width: 100%; }\r\n    .items-action .file-action { border: 0 none; color: #fff; padding: 4px 10px; }\r\n    .items-action .file-action { background: rosybrown; float: left; clear: both; }\r\n    /*.items-action .file-action:last-child { background: #0085ba;float: right;}*/\r\n    #fm-toolbar .Mi.ipagination a:first-child { border: 0; border-radius: 0; }\r\n    #fm-toolbar .Mi.ipagination input { border: medium none; float: left; font-family: arial; font-size: 11px; height: 30px; margin: 0; outline: medium none; padding: 0; text-align: center; vertical-align: middle; width: 84px; }\r\n    #fm-toolbar .Mi.ipagination a { border: 0; border-radius: 0; background: #fff; }\r\n    #fm-toolbar .ipagination.iweb { margin-top: 3.45px; }\r\n    #frowInTotals { display: inline-block; float: left; line-height: 39px; padding-right: 3px; vertical-align: baseline; }\r\n    label#progressall { display: none; padding: 7px 7px; }\r\n    .Cdiv { padding-left: 12px; height: 17px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; display: inline-block; font-weight: normal; }\r\n    #btn-fm-upload label { margin-bottom: 0 !important; }\r\n    /* Extra small devices (phones, 600px and down) */\r\n\r\n    @media only screen and (max-width: 600px) {\r\n        #fm-grid li { width: 47.65%; }\r\n    }\r\n\r\n    /* Small devices (portrait tablets and large phones, 600px and up) */\r\n\r\n    @media only screen and (min-width: 600px) {\r\n    }\r\n\r\n    /* Medium devices (landscape tablets, 768px and up) */\r\n\r\n    @media only screen and (min-width: 768px) {\r\n    }\r\n\r\n    /* Large devices (laptops/desktops, 992px and up) */\r\n\r\n    @media only screen and (min-width: 992px) {\r\n    }\r\n\r\n    /* Extra large devices (large laptops and desktops, 1200px and up) */\r\n\r\n    @media only screen and (min-width: 1200px) {\r\n    }\r\n</style>\r\n"],"sourceRoot":""}]);

// exports


/***/ }),

/***/ 953:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(786);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (obj, key, value) {
  if (key in obj) {
    (0, _defineProperty2.default)(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

/***/ }),

/***/ 964:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _vue = __webpack_require__(27);

var _vue2 = _interopRequireDefault(_vue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EventBus = new _vue2.default();
exports.default = EventBus;

/***/ }),

/***/ 965:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(385), __esModule: true };

/***/ }),

/***/ 967:
/***/ (function(module, exports) {

module.exports = {"AppSettings":{"Debug":true,"Version":"1.0.0.0","Domain":"https://vietlab.migroup.asia","UploadFolder":"uploads","FileUploadMaxSize":5000,"FileUploadSubFix":false,"ImageScaleWidth":300,"ImageScaleHeight":0,"ImageAllowUpload":".jpg,.jpeg,.png,.gif,.bit,.webp","DocumentAllowUpload":".doc,.docx,.pdf,.xls,.xlsx,.zip,.rar","FoderImg":"https://vietlab.migroup.asia","BaseDomain":"https://vietlab.migroup.asia","CacheEnable":false,"ESEnable":false,"NodeES":"http://127.0.0.1:9200","IndexES":"product_suggest"},"Redis":{"ConnectionString":"127.0.0.1:6379","DefaultDatabase":8,"InstanceName":"PLM_","CachingExpireMinute":15},"ConnectionStrings":{"DefaultConnection":"Server=118.70.185.130;Database=vietlab_main;Trusted_Connection=False;User Id=ndev;password=asd123!@##@"},"Cors":{"WithOrigin":"http://localhost:60099/"},"Logging":{"IncludeScopes":false,"LogLevel":{"Default":"Debug","System":"Information","Microsoft":"Information"}},"Tokens":{"Key":"0123456789ABCDEF","Issuer":"https://vietlab.migroup.asia/"}}

/***/ }),

/***/ 968:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAJUlEQVQ4jWNgoBSkpaX9pwRTxwBsriJabHAYMBoGo2FAcRhQCgAhhv41B1NcjwAAAABJRU5ErkJggg=="

/***/ }),

/***/ 969:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAHElEQVQ4jWNgGPogLS3tPyV41AWjLhguLqAUAABjQNJhHOFYqgAAAABJRU5ErkJggg=="

/***/ }),

/***/ 970:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAg0lEQVQ4ja3QwQ3AIAgF0J6dwyVcwxUI4hZ1C6dwDZdwDs/tqYlRTKEpCRcjD/3HsakQwjX27t62xqFXYN4m7V+B9OGL53zQFds7JzYF0DigKoDKAUUBlAVAxCwFEDGzqSpekBaAiKIUIKLIAV4BeA5wCsAtAABYKQAAlgvRKEI0z9wNbkpkPnEUO00AAAAASUVORK5CYII="

/***/ }),

/***/ 971:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUFFNEVGRTcyNzI5MTFFN0E4MTJDODUxMTY5OTBDRTMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUFFNEVGRTgyNzI5MTFFN0E4MTJDODUxMTY5OTBDRTMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBQUU0RUZFNTI3MjkxMUU3QTgxMkM4NTExNjk5MENFMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBQUU0RUZFNjI3MjkxMUU3QTgxMkM4NTExNjk5MENFMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoebdJQAAAEfSURBVHjalNI9SwMxGMDxXHutaEXwHZdCEaSLQ71Fuujoojg4Cv0K4ioUStd+BpcOfgGh6Obo0uLsoJNQsC6+0evL9R94DkLotXcP/LiEJM8lT+J4nqckznGFY5UgUkY7jQ2VMFwUsYQ9ZHEwZ80Qb/gOEzRQwiJW8CgTHWthIN9lXOA+TFBBBme4xmnEn8MELeTMI/Skrb//eJ1zhJ+oIrpWP07hZy7YT3qNOsZYQA3PaGJbCro+rbiuleAPR6hK/xJlfGATJ+jPShDIPX/JbnRsYUf6JSn0ICrBGt7lSgNrTG+9i1XksQvfTPCLAp70QETNMrKbOm7QNhPohYdTdmXHHW7xoGtmTtZv+yXGzfny2Doq5sOx4xOjsDMRYADeTjM37KwM6gAAAABJRU5ErkJggg=="

/***/ }),

/***/ 972:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAlElEQVQ4jWNgGHCQlpb2Hw3fz8jIUKDEANIMwWEAXkwTAx4T7WdsXkhNTV2dnp6uT4ZrLjCkpaX9T09PL0pPT59AqgGpqan5MBfYp6WlvSLRgF+JiYmiDGlpad/T09PDybB9AywMjqampm4g1YD09HR/mAGL0tLSfpFowKu0tDRWmAGHybB9AnI0viHDAH2y0w06AACLVFch1Ej7rgAAAABJRU5ErkJggg=="

/***/ }),

/***/ 973:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA6UlEQVQ4ja2TUU7DQAxE/UO5SwPH2SP4TX5yFKIgisRZijhFyyXyGfHXVuVnUzmrBNGCpZFWlme0s541Kwp4lNQBn8BXxl5S5+5VOX+ppmnuJb0CJ+C8gBOwSSmtJuSU0krSxw/EEu8TEeDtCvKIjZmZ1XX9EK9tZubuFdCH4X70H3pHd68MeI7K42AQ6YtevEVrwL5oTggzgnF2Z8Aw46+PK1sgn4HhXwT+ZiGn7qZHdPencY3HW9YoaW1mZjnCVwVJUjeJsrtvf0t29+3sfwBeop0ZHHLw7hZ/paQ10AK7vOIhn9uL51DfTreZQOo3Kt0AAAAASUVORK5CYII="

/***/ }),

/***/ 974:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAATUlEQVQ4jWNgoBXIyMhQSE1N3Z+RkaFAlua0tLT7aWlp/9PS0u6TZAia5v8kGYJDM2mGwACyZqI1jRpAJsARZURhsgxAtnhgXED1wAUASBXMx2KfmeMAAAAASUVORK5CYII="

/***/ }),

/***/ 975:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(978)

var Component = __webpack_require__(372)(
  /* script */
  __webpack_require__(976),
  /* template */
  __webpack_require__(977),
  /* scopeId */
  "data-v-d3b1cad6",
  /* cssModules */
  null
)
Component.options.__file = "C:\\Users\\Administrator\\Desktop\\vietlab\\VietLab.CMS\\ClientApp\\components\\fileManager\\list.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] list.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (true) {(function () {
  var hotAPI = __webpack_require__(177)
  hotAPI.install(__webpack_require__(27), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d3b1cad6", Component.options)
  } else {
    hotAPI.reload("data-v-d3b1cad6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 976:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _from = __webpack_require__(965);

var _from2 = _interopRequireDefault(_from);

var _extends2 = __webpack_require__(8);

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(953);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

__webpack_require__(780);

var _constant = __webpack_require__(781);

var _constant2 = _interopRequireDefault(_constant);

var _vuex = __webpack_require__(179);

var _vueLoadingOverlay = __webpack_require__(373);

var _vueLoadingOverlay2 = _interopRequireDefault(_vueLoadingOverlay);

var _eventBus = __webpack_require__(964);

var _eventBus2 = _interopRequireDefault(_eventBus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: "FileManager",
    props: {
        miKey: {
            type: String
        }
    },
    components: {
        Loading: _vueLoadingOverlay2.default
    },
    data: function data() {
        var _ref;

        return _ref = {
            isLoading: false,
            fullPage: false,
            color: "#007bff",
            isLoadLang: false,

            currentPage: 1,
            pageSize: 30
        }, (0, _defineProperty3.default)(_ref, "isLoading", false), (0, _defineProperty3.default)(_ref, "MaxFileSize", 3000), (0, _defineProperty3.default)(_ref, "selectedFile", []), (0, _defineProperty3.default)(_ref, "extensions", []), (0, _defineProperty3.default)(_ref, "extImage", []), (0, _defineProperty3.default)(_ref, "isActive", false), (0, _defineProperty3.default)(_ref, "selectType", ''), _ref;
    },
    created: function created() {
        var config = __webpack_require__(967);
        this.extImage = config.AppSettings.ImageAllowUpload;
        this.extensions = config.AppSettings.ImageAllowUpload.concat(config.AppSettings.DocumentAllowUpload);

        _eventBus2.default.$on(this.miKey, this.FileManagerOpen);
    },
    destroyed: function destroyed() {},

    computed: {
        files: function files() {
            return this.$store.getters.files;
        }
    },
    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(["fmFileUpload", "fmFileGetAll"]), {
        mapImageUrl: function mapImageUrl(img, ext) {
            if (this.extImage.indexOf(ext) !== -1) {
                return '/uploads/thumb' + img;
            }
            return './../../ClientApp/assets/fileicons/' + ext.replace('.', '') + '.png';
        },
        checkImage: function checkImage(ext) {
            if (this.extImage.indexOf(ext) == -1) {
                return true;
            }
            return false;
        },
        DoUploadFile: function DoUploadFile(e) {
            var files = e.srcElement.files;

            if (files) {
                var filesTemp = (0, _from2.default)(files);

                var msgFileAllow = '';
                var msgLimitedSize = '';
                for (var i = 0; i < filesTemp.length; i++) {

                    var name = filesTemp[i].name;
                    var type = name.split('.').pop();
                    if (this.extensions.indexOf(type) == -1) {
                        filesTemp.splice(i, 1);
                        if (msgFileAllow.length == 0) {
                            msgFileAllow = name;
                        } else {
                            msgFileAllow += ', ' + name;
                        }
                    }
                    if (msgFileAllow.length > 0) {
                        this.$toast.error(msgFileAllow + ' không hợp lệ !', {});
                    }
                }
                for (var i = 0; i < filesTemp.length; i++) {

                    var size = filesTemp[i].size;
                    var _name = filesTemp[i].name;

                    if (this.MaxFileSize < size / 1024) {
                        filesTemp.splice(i, 1);
                        if (msgLimitedSize.length == 0) {
                            msgLimitedSize = _name;
                        } else {
                            msgLimitedSize += ', ' + _name;
                        }
                    }
                    if (msgLimitedSize.length > 0) {
                        this.$toast.error(msgFileAllow + ' dung lượng quá lớn !', {});
                    }
                }
                if (filesTemp.length) {
                    var fd = new FormData();

                    filesTemp.forEach(function (item) {
                        fd.append('files', item);
                    });
                    this.UploadFileAction(fd);
                }
            }
        },
        UploadFileAction: function UploadFileAction(files) {
            var _this = this;

            this.fmFileUpload(files).then(function (response) {
                if (response.success) {
                    _this.fmFileGetAll({
                        pageIndex: _this.currentPage,
                        pageSize: _this.pageSize
                    });
                    _this.$toast.success(response.Message, {});
                    _this.isLoading = false;
                } else {
                    _this.$toast.error(response.Message, {});
                    _this.isLoading = false;
                }
            }).catch(function (e) {
                _this.$toast.error(_constant2.default.error + ". Error:" + e, {});
                _this.isLoading = false;
            });
        },
        SelectFile: function SelectFile(event, file) {
            if (this.selectType == 'multi') {
                if (event.currentTarget.classList.contains('_active')) {
                    event.currentTarget.classList.remove('_active');

                    this.selectedFile = this.selectedFile.filter(function (obj) {
                        return obj.id != file.id;
                    });
                } else {
                    event.currentTarget.classList.add("_active");
                    this.selectedFile.push(file);
                }
            } else {

                this.selectedFile = [];
                if (event.currentTarget.classList.contains('_active')) {
                    event.currentTarget.classList.remove('_active');
                } else {
                    var items = document.querySelectorAll('.item');
                    items.forEach(function (item) {
                        item.classList.remove('_active');
                    });
                    event.currentTarget.classList.add("_active");
                    this.selectedFile.push(file);
                }
            }
        },
        FileManagerOpen: function FileManagerOpen(param) {
            this.selectType = param;
            this.$refs["file-manager-modal"].show();
            this.fmFileGetAll({
                pageIndex: this.currentPage,
                pageSize: this.pageSize
            });
        },
        hideFileManagerModal: function hideFileManagerModal() {
            this.$refs["file-manager-modal"].hide();
        },
        attackFile: function attackFile() {
            this.$emit("handleAttackFile", this.selectedFile);
            this.$refs["file-manager-modal"].hide();
            this.selectedFile = [];
        },
        toggleFileModal: function toggleFileModal() {
            this.$refs["file-manager-modal"].toggle("#toggle-btn");
        }
    }),
    mounted: function mounted() {},

    watch: {
        currentPage: function currentPage() {
            this.fmFileGetAll({
                pageIndex: this.currentPage,
                pageSize: this.pageSize
            });
        }
    }
};

/***/ }),

/***/ 977:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "file-manager"
  }, [_c('loading', {
    attrs: {
      "active": _vm.isLoading,
      "height": 35,
      "width": 35,
      "color": _vm.color,
      "is-full-page": _vm.fullPage
    },
    on: {
      "update:active": function($event) {
        _vm.isLoading = $event
      }
    }
  }), _vm._v(" "), _c('b-modal', {
    ref: "file-manager-modal",
    attrs: {
      "id": "file-manager",
      "fbody": "xxx",
      "hide-footer": "",
      "hide-header": "",
      "size": "xl"
    }
  }, [_c('div', {
    staticClass: "container-fluid",
    attrs: {
      "id": "fm-container"
    }
  }, [_c('b-row', {
    attrs: {
      "id": "fm-toolbar"
    }
  }, [_c('b-col', {
    attrs: {
      "lg": "2",
      "md": "2",
      "sm": "12"
    }
  }, [_c('b', [_vm._v("Thư viện")])]), _vm._v(" "), _c('b-col', [_c('b-row', {
    attrs: {
      "align-h": "between"
    }
  }, [_c('b-col', {
    attrs: {
      "cols": "4"
    }
  }, [_c('div', {
    staticClass: "tool-items"
  }, [_c('ul', {
    staticClass: "tools"
  }, [_c('li', {
    attrs: {
      "title": "Create folder"
    }
  }, [_c('i', {
    staticClass: "create-folder"
  })])]), _vm._v(" "), _c('ul', {
    staticClass: "tools"
  }, [_c('li', {
    attrs: {
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "remove"
  })])])])]), _vm._v(" "), _c('b-col', {
    staticClass: "bd-0",
    attrs: {
      "cols": "4"
    }
  }, [_c('b-pagination', {
    attrs: {
      "align": "right",
      "size": "sm",
      "limit": 4,
      "total-rows": _vm.files.totals,
      "per-page": _vm.pageSize
    },
    model: {
      value: (_vm.currentPage),
      callback: function($$v) {
        _vm.currentPage = $$v
      },
      expression: "currentPage"
    }
  })], 1), _vm._v(" "), _c('b-col', {
    attrs: {
      "cols": "4"
    }
  }, [_c('div', {
    staticClass: "tool-items float-right"
  }, [_c('ul', {
    staticClass: "tools"
  }, [_c('li', {
    attrs: {
      "title": "Create folder"
    }
  }, [_c('i', {
    staticClass: "create-folder"
  })]), _vm._v(" "), _c('li', {
    attrs: {
      "title": "Upload",
      "id": "btn-fm-upload"
    }
  }, [_c('label', {
    attrs: {
      "for": "fileSingleupload"
    }
  }, [_c('i', {
    staticClass: "upload"
  })]), _vm._v(" "), _c('input', {
    staticStyle: {
      "display": "none"
    },
    attrs: {
      "accept": "image/*,.doc,.docx,.pdf,.xls,.xlsx,.zip,.rar",
      "id": "fileSingleupload",
      "multiple": "",
      "type": "file",
      "name": "files[]"
    },
    on: {
      "change": _vm.DoUploadFile
    }
  })])]), _vm._v(" "), _c('ul', {
    staticClass: "tools"
  }, [_c('li', {
    attrs: {
      "title": "List view"
    }
  }, [_c('i', {
    staticClass: "list"
  })]), _vm._v(" "), _c('li', {
    attrs: {
      "title": "Grid View"
    }
  }, [_c('i', {
    staticClass: "grid"
  })])])])])], 1)], 1)], 1), _vm._v(" "), _c('b-row', {
    attrs: {
      "id": "fm-main"
    }
  }, [_c('b-col', {
    attrs: {
      "id": "fm-sidebar",
      "lg": "2",
      "md": "2",
      "sm": "12"
    }
  }, [_c('div', {
    attrs: {
      "id": "fm-folder"
    }
  }, [_c('ul', [_c('li', [_c('i', {
    staticClass: "create-folder"
  }), _vm._v(" Document")]), _vm._v(" "), _c('li', [_c('i', {
    staticClass: "create-folder"
  }), _vm._v(" Image")]), _vm._v(" "), _c('li', [_c('i', {
    staticClass: "create-folder"
  }), _vm._v(" Icon")])])])]), _vm._v(" "), _c('b-col', {
    attrs: {
      "id": "fm-content",
      "lg": "10",
      "dm": "2",
      "sm": "12"
    }
  }, [_c('div', {
    attrs: {
      "id": "fm-data-wrapper"
    }
  }, [_c('div', {
    staticClass: "fm-list-wrapper"
  }, [_c('div', {
    staticClass: "fm-list"
  }, [_c('ul', {
    attrs: {
      "id": "fm-grid"
    }
  }, _vm._l((_vm.files.files), function(file) {
    return _c('li', {
      key: file.id,
      staticClass: "item",
      class: {
        _active: _vm.isActive, not_img: _vm.checkImage(file.fileExt)
      },
      on: {
        "click": function($event) {
          return _vm.SelectFile($event, {
            path: file.filePath,
            id: file.id
          })
        }
      }
    }, [_c('img', {
      attrs: {
        "src": _vm.mapImageUrl(file.filePath, file.fileExt),
        "alt": ""
      }
    }), _vm._v(" "), _c('i', {
      staticClass: "fa fa-check"
    }), _vm._v(" "), _c('div', {
      staticClass: "info"
    }, [_c('p', {
      staticClass: "name"
    }, [_vm._v(_vm._s(file.name))]), _vm._v(" "), _c('p', {
      staticClass: "dimensions"
    }, [_vm._v(_vm._s(file.dimensions))]), _vm._v(" "), _c('p', {
      staticClass: "size"
    }, [_vm._v(_vm._s(file.fileSize) + "kb")])])])
  }), 0)])])])])], 1), _vm._v(" "), _c('b-row', {
    attrs: {
      "align-h": "end",
      "id": "fm-footer"
    }
  }, [_c('b-col', {
    attrs: {
      "cols": "4"
    }
  }, [_c('div', {
    staticClass: "btn-attack"
  }, [_c('button', {
    attrs: {
      "type": "button",
      "id": "btn-attack"
    },
    on: {
      "click": _vm.attackFile
    }
  }, [_vm._v("Đính kèm")]), _vm._v(" "), _c('button', {
    staticClass: "iclose",
    attrs: {
      "type": "button",
      "id": "dl-close"
    },
    on: {
      "click": _vm.hideFileManagerModal
    }
  }, [_vm._v("Đóng")])])])], 1)], 1)])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (true) {
  module.hot.accept()
  if (module.hot.data) {
     __webpack_require__(177).rerender("data-v-d3b1cad6", module.exports)
  }
}

/***/ }),

/***/ 978:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(926);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(785)("8b3b638a", content, false);
// Hot Module Replacement
if(true) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept(926, function() {
     var newContent = __webpack_require__(926);
     if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});
//# sourceMappingURL=40.js.map