using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VietLab.CMS.ExcelHelper;
using MI.Dal.IDbContext;
using MI.Dapper.Data.Repositories.Interfaces;
using MI.Entity.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nest;


namespace VietLab.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportExportController : ControllerBase
    {
        private readonly IExportUtility _export;
        IProductRepository _productRepository;
        IHostingEnvironment _hostingEnvironment;
        public ImportExportController(IProductRepository productRepository, IHostingEnvironment hostingEnvironment, IExportUtility export)
        {

            _productRepository = productRepository;
            _hostingEnvironment = hostingEnvironment;
            _export = export;

        }
        //[Route("Test")]
        //public IActionResult Test()
        //{

        //    var x = _export.ExportProductPriceLocationExcel();
        //    return Ok(x);
        //}
        //[Route("TestImport")]
        //public IActionResult TestImport()
        //{
        //    var x = _export.ImportProductPriceLocationExcel();
        //    return Ok(x);
        //}
    }
}
