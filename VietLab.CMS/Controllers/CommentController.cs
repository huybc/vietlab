using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Bo.Bussiness;
using MI.Entity.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace VietLab.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        CommentBCL commentBCL;
        CustomerBCL customerBCL;


        public CommentController()
        {
            commentBCL = new CommentBCL();
            customerBCL = new CustomerBCL();
        }
        [HttpPost("Get")]
        public IActionResult Get([FromBody] Utils.FilterComment filter)
        {
            dynamic response = new System.Dynamic.ExpandoObject();
            var total = 0;
            var data = commentBCL.Find(filter, out total);

            var GetChild = commentBCL.FindAll(x => data.Any(d => d.Id == x.ParentId)).GroupBy(x => x.ParentId).ToDictionary(x => x.Key, x => x.Select(d => d).ToList());

            response.listData = data.Select(x => new
            {
                x.Id,
                x.ObjectId,
                x.ObjectType,
                x.ParentId,
                x.Status,
                x.CreatedDate,
                x.CreatedByAdminId,
                x.Content,
                x.Name,
                x.PhoneOrMail,
                x.Type,
                x.Rate,
                Child = GetChild.ContainsKey(x.Id) ? GetChild[x.Id] : new List<Comment>()
            });
            response.total = total;
            return Ok(response);
        }

        [HttpPut("UpdateStatus")]
        public IActionResult UpdateStatus([FromBody] Comment obj)
        {
            dynamic response = new System.Dynamic.ExpandoObject();
            response.Success = false;
            response.Message = "Thất bại";
            var data = commentBCL.UpdateStatus(obj);
            if (data == true)
            {
                response.Success = true;
                response.Message = "Thành công";
            }
            return Ok(response);
        }
        [HttpPost("Add")]
        public IActionResult Add([FromBody] Comment obj)
        {
            dynamic response = new System.Dynamic.ExpandoObject();
            response.Success = false;
            response.Message = "Thất bại";
            obj.ObjectType = obj.ObjectType.ToString();
            var data = commentBCL.Add(obj);
            if (data == true)
            {

                response.Success = true;
                response.Message = "Thành công";
            }
            return Ok(response);
        }
        [HttpPost("Delete")]
        public IActionResult Delete([FromBody] Comment obj)
        {
            dynamic response = new System.Dynamic.ExpandoObject();
            response.Success = false;
            response.Message = "Thất bại";
            var data = commentBCL.Remove(obj);
            if (data == true)
            {
                response.Success = true;
                response.Message = "Thành công";
            }
            return Ok(response);
        }
    }
}
