using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI.Bo.Bussiness;
using MI.Entity.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utils;

namespace VietLab.CMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComBoProductController : ControllerBase
    {
        ProductInProductBCL productInProductBCL;
        ProductInLanguageBCL productInLanguageBCL;

        public ComBoProductController()
        {
            productInLanguageBCL = new ProductInLanguageBCL();
            productInProductBCL = new ProductInProductBCL();
        }
        [HttpPost("Search")]
        public ResponsePaging Search([FromBody] FilterProduct filter)
        {
            ResponsePaging responseData = new ResponsePaging();
            try
            {
                int total = 0;
                var data = productInLanguageBCL.FindAll(filter, out total);

                responseData.ListData = data.Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Avatar,
                    price = x.Price,
                    x.Code
                }).ToList<object>();
                responseData.Total = total;
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }




        [HttpGet("GetByProductId")]
        public ResponseData GetByProductId(int productId)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var ComboProduct = productInProductBCL.FindAll(x => x.ProductId == productId);
                var lstId = ComboProduct.Select(x => (int)x.ProductItemId).ToList();
                var data = productInLanguageBCL.FindByListId(lstId);
                responseData.ListData = data.Select(x => new { id = x.ProductId, name = x.Title, avatar = x.Product.Avatar, price = x.Product.Price }).ToList<object>();

            }
            catch (Exception ex)
            {

            }
            return responseData;
        }
        [HttpPost("Add")]
        public ResponseData Add([FromBody] ViewModel.ProductInProduct obj)
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var Combo = new List<MI.Entity.Models.ProductInProduct>();
                int i = 0;
                foreach (var item in obj.Products)
                {
                    Combo.Add(new MI.Entity.Models.ProductInProduct
                    {
                        ProductId = obj.Id,
                        ProductItemId = item.Id,
                        SortOrder = i,
                        Type = MI.Entity.Enums.TypeComboProduct.combo
                    });
                    i++;
                }
                var isAdd = productInProductBCL.Merge(obj.Id, MI.Entity.Enums.TypeComboProduct.combo, Combo);
                if (isAdd)
                {
                    responseData.Success = true;
                    responseData.Message = "Thành công";

                }
            }
            catch (Exception ex)
            {

            }
            return responseData;
        }
    }
}
