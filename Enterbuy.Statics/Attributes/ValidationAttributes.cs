﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Enterbuy.Statics.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public sealed class ContractAnnotationAttribute : Attribute
    {
        #region Constructors and Destructors

        public ContractAnnotationAttribute([NotNull] string contract)
            : this(contract, false)
        {
        }

        public ContractAnnotationAttribute([NotNull] string contract, bool forceFullStates)
        {
            this.Contract = contract;
            this.ForceFullStates = forceFullStates;
        }

        #endregion Constructors and Destructors

        #region Public Properties

        public string Contract { get; private set; }
        public bool ForceFullStates { get; private set; }

        #endregion Public Properties
    }

    [AttributeUsage(
           AttributeTargets.Method | AttributeTargets.Parameter |
           AttributeTargets.Property | AttributeTargets.Delegate |
           AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class NotNullAttribute : Attribute
    {
    }

    [AttributeUsage(
        AttributeTargets.Method | AttributeTargets.Parameter |
        AttributeTargets.Property | AttributeTargets.Delegate |
        AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class CanBeNullAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ValueInContainterAttribute : ValidationAttribute
    {
        public object[] Container { get; set; }
        public bool AllowBlank { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                if (AllowBlank)
                    return ValidationResult.Success;
                else
                    return new ValidationResult(validationContext.DisplayName, new List<string> { validationContext.MemberName });
            }
            var containerTexts = Container.Select(s => s.ToString()).ToList();
            var attributeValue = value.ToString();
            if (!containerTexts.Contains(attributeValue))
                return new ValidationResult(GetErrorMessage(validationContext), new List<string> { validationContext.MemberName });
            return ValidationResult.Success;
        }

        private string GetErrorMessage(ValidationContext validationContext)
        {
            // Message that was supplied
            if (!string.IsNullOrEmpty(ErrorMessage))
                return ErrorMessage;

            // Use generic message: i.e. The field {0} is invalid
            //return this.FormatErrorMessage(validationContext.DisplayName);

            // Custom message
            return string.Format("{0} phải nhận một trong các giá trị ({1})", validationContext.DisplayName, string.Join(",", Container));
        }
    }

    public class SubListIntegerAttribute : ValidationAttribute
    {
        public object[] Container { get; set; }
        public bool AllowBlank { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!AllowBlank && value == null)
            {
                return ValidationResult.Success;
            }
            var values = (List<int>)value;
            var valueTexts = values.Select(s => s.ToString()).ToList();
            var containerTexts = Container.Select(s => s.ToString()).ToList();
            var attributeValue = value.ToString();
            if (valueTexts.Except(containerTexts).Count() > 0)
                return new ValidationResult(GetErrorMessage(validationContext), new List<string> { validationContext.MemberName });
            return ValidationResult.Success;
        }

        private string GetErrorMessage(ValidationContext validationContext)
        {
            // Message that was supplied
            if (!string.IsNullOrEmpty(ErrorMessage))
                return ErrorMessage;

            // Use generic message: i.e. The field {0} is invalid
            //return this.FormatErrorMessage(validationContext.DisplayName);

            // Custom message
            return string.Format("{0} phải nhận một trong các giá trị ({1})", validationContext.DisplayName, string.Join(",", Container));
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class NumberInContainterAttribute : ValidationAttribute
    {
        public double[] Container { get; set; }
        public bool AllowBlank { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!AllowBlank && value == null)
            {
                return ValidationResult.Success;
            }
            var attributeValue = double.Parse(value.ToString());
            if (!Container.Contains(attributeValue))
                return new ValidationResult(GetErrorMessage(validationContext), new List<string> { validationContext.MemberName });
            return ValidationResult.Success;
        }

        private string GetErrorMessage(ValidationContext validationContext)
        {
            // Message that was supplied
            if (!string.IsNullOrEmpty(ErrorMessage))
                return ErrorMessage;

            // Use generic message: i.e. The field {0} is invalid
            //return this.FormatErrorMessage(validationContext.DisplayName);

            // Custom message
            return string.Format("{0} must be in ({1})", validationContext.DisplayName, string.Join(",", Container));
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class SizeAttribute : ValidationAttribute
    {
        public bool AllowBlank { get; set; }
        public int Min { get; set; }

        public int Max { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var valueAsList = (System.Collections.IList)value;
            if (valueAsList == null || valueAsList.Count == 0)
                return new ValidationResult(validationContext.MemberName + " might not be blank", new List<string> { validationContext.MemberName });
            if (Min >= 0 && valueAsList.Count < Min)
                return new ValidationResult(validationContext.MemberName + ".Count might be greats than or equals to " + Min, new List<string> { validationContext.MemberName });
            if (Max >= 0 && valueAsList.Count > Max)
                return new ValidationResult(validationContext.MemberName + ".Count might be less than or equals to " + Max, new List<string> { validationContext.MemberName });
            return ValidationResult.Success;
        }
    }
}