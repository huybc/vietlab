﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Enterbuy.Statics.Configurations
{
    public static class NewtonJsonSerializerSettings
    {
        public static readonly JsonSerializerSettings Snake = new JsonSerializerSettings
        {
            ContractResolver = new DefaultContractResolver
            {
                NamingStrategy = new SnakeCaseNamingStrategy()
            }
        };

        public static readonly JsonSerializerSettings Camel = new JsonSerializerSettings
        {
            ContractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            }
        };
    }
}