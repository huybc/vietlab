﻿using HtmlAgilityPack;
using System.Linq;
using System.Text.RegularExpressions;


namespace Enterbuy.Statics.Helpers
{
    public static class UIHelper
    {
        //public static string StoreFilePath(string path, bool isThumb = true, string store_url = "https://cmsenterbuy.migroup.asia", string root = "/uploads")
        //{
        //    //if (string.IsNullOrEmpty(store_url))
        //    //{
        //    //    store_url = AppSettings.FoderImg;

        //    //}
        //    if (isThumb)
        //        return store_url + root + "/thumb" + path;
        //    else
        //        return store_url + root + path;

        //}
        public static string CutLine(string words, int cuttedLetter = 16, int cuttedWord = 4, string wrapper = "br", bool isNeedClose = false, string class_wrapper = null)
        {
            var countLetter = words.Length;
            if (countLetter >= cuttedLetter)
            {
                var countWord = words.Split(' ').ToList();

                if (countWord.Count > cuttedWord)
                {
                    var result = "";
                    if (isNeedClose == true)
                    {
                        var openWrapper = "<" + wrapper + "class=\"" + class_wrapper + "\"" + ">";
                        var closeWrapper = "</" + wrapper + ">";

                        for (int i = 0; i < countWord.Count; i++)
                        {

                            if ((i + 1) % cuttedWord == 0)
                                result += countWord[i] + openWrapper + closeWrapper;
                            if ((i + 1) % cuttedWord != 0)
                                result += countWord[i] + " ";
                        }

                    }
                    else
                    {
                        wrapper = "<" + wrapper + ">";

                        for (int i = 0; i < countWord.Count; i++)
                        {

                            if ((i + 1) % cuttedWord == 0)
                                result += countWord[i] + wrapper;
                            if ((i + 1) % cuttedWord != 0)
                                result += countWord[i] + " ";
                        }

                    }
                    return result;
                }
                else
                {
                    return words;
                }

            }
            return words;
            //Count word
        }


        public static string FormatNumber(object number)
        {
            if (number != null && (decimal)number > 0)
            {
                return string.Format("{0:n0}", number) + "đ";
            }
            else
            {
                return "Liên hệ";
            }
        }

        public static string ConvertLinkYoutubeVideo(string original)
        {
            string data = original;
            string reg = "\"(h.*?)\"";
            try
            {
                data = Regex.Matches(data, reg)[0].ToString().Replace("\"", "").Replace("\"", "").Replace(@"watch?v=", @"embed\");
            }
            catch
            {
                data = "";
            }

            return data;
        }
        public static string RenderLazyLoadBody(string body)
        {
            HtmlDocument doc = new HtmlDocument();
            if (!string.IsNullOrEmpty(body))
            {
                doc.LoadHtml(body);
                var imgs = doc.DocumentNode.SelectNodes("//img");
                if (imgs != null)
                {
                    foreach (var item in imgs)
                    {
                        var origin = item.GetAttributeValue("src", null);
                        item.SetAttributeValue("src", "~/images/gray.jpg");
                        item.SetAttributeValue("data-src", origin);
                        item.AddClass("lazy");

                    }
                }
                var figures = doc.DocumentNode.SelectNodes("//figure");
                if (figures != null)
                {

                    foreach (var item in figures)
                    {
                        var _img = item.SelectSingleNode(".//img");
                        if (_img != null)
                        {
                            item.SetAttributeValue("style", "");
                            _img.AddClass("cust-ag");
                        }

                    }
                }
                return doc.DocumentNode.OuterHtml;
            }
            return "";
        }
    }
}