﻿using Newtonsoft.Json.Linq;

namespace Enterbuy.Statics.Extensions
{
    public static class JObjectExtensions
    {
        public static bool HasToken(this JObject obj, string path)
        {
            var token = obj.SelectToken(path);
            if (token == null)
                return false;
            return true;
        }
    }
}