﻿using Enterbuy.Data.Sql.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Enterbuy.Data.Sql.Dao
{
    public interface ISqlServerDao<T> where T : BaseEntity
    {
        T GetById(object id);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        Task<T> GetByIdAsync(object id);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);

        Task<List<V>> QueryStatementAsync<V>(string statement, object param);
    }
}