﻿namespace Enterbuy.Data.Sql.Filter
{
    public class BasePagingFilter : BaseFilter
    {
        public int Page { get; set; }
        public int Limit { get; set; }
    }
}