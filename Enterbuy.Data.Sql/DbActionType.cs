﻿namespace Enterbuy.Data.Sql
{
    public enum DbActionType
    {
        Read,
        Write
    }
}