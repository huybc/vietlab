﻿using Dapper;
using Enterbuy.Data.Sql.CustomTypeMap;

namespace Enterbuy.Data.Sql
{
    public class TypeMapRegistrar
    {
        public static void Register()
        {
            SqlMapper.AddTypeHandler(new ArrayTypeHandler());
        }
    }
}