﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Enterbuy.Data.Sql.CustomTypeMap
{
    public class ArrayTypeHandler : SqlMapper.TypeHandler<List<string>>
    {
        public override void SetValue(IDbDataParameter parameter, List<string> value)
        {
            parameter.Value = value;
        }

        public override List<string> Parse(object value)
        {
            var array = (string[])value;
            return array.ToList();
        }
    }
}