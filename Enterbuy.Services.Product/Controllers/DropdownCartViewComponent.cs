﻿using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Product.Controllers
{
    public class DropdownCartViewComponent : ViewComponent
    {
        private readonly IProductDao _productDao;
        private readonly Utils.ICookieLocationUtility _cookieLocation;
        public DropdownCartViewComponent(IProductDao productDao, Utils.ICookieLocationUtility cookieLocation)
        {
            _productDao = productDao;
            _cookieLocation = cookieLocation;
        }

        public async Task<IViewComponentResult> InvokeAsync(string productIds)
        {
            var location = _cookieLocation.SetCookieDefault();

            var model = _productDao.GetProductInListProductsMinifyWithTotalPrice(productIds, location.LocationId, "vi-VN", 1, 2, out _, out var totalPrice);
            //var total_money = _productRepository.GetTotalPriceInListProductsMinify(product_ids, cookie_location.LocationId, CurrentLanguageCode);
            ViewBag.TotalPrice = totalPrice;
            return View(model);
        }
    }
}