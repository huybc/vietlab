﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.Services.Product.Components
{
    public class CartProcessingViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}