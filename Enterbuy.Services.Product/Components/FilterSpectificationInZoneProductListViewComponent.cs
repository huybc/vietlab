﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.Dto;

namespace Enterbuy.Services.Product.Components
{
    public class FilterSpectificationInZoneProductListViewComponent : ViewComponent
    {
       


        public FilterSpectificationInZoneProductListViewComponent()
        {
            
        }

        public IViewComponentResult Invoke(FilterProductBySpectification fp)
        {
           
            ViewBag.fp = fp;
        
            return View();
        }
    }
}