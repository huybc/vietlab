﻿
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enterbuy.Services.Product.Components
{
    public class ComboInProductByLocationViewComponent : ViewComponent
    {
      
        private readonly IProductDao _productRepository;

        public ComboInProductByLocationViewComponent(IProductDao productRepository)
        {
            _productRepository = productRepository;
        }
        public IViewComponentResult Invoke(int product_id, int location_id)
        {
            var total_row_combo = 0;
            var model = _productRepository.GetProductsInProductById(product_id, "com-bo", location_id, Utils.Utility.DefaultLang, 1, 4, out total_row_combo);
            return View(model);
        }
    }
}
