﻿using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enterbuy.Services.Product.Components
{
    public class SEO_ProductViewComponent : ViewComponent
    {
        public SEO_ProductViewComponent()
        {
            
        }
        public IViewComponentResult Invoke(ProductDetail product)
        {
            ViewBag.Detail = product;
            return View();
        }
    }
}
