﻿using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Services.Common.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Enterbuy.Core.Statics;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Services.Common.Models;
using Newtonsoft.Json;

namespace Enterbuy.Services.Common.Services
{
    public class BannerAdsService : IBannerAdsService
    {
        private readonly IBannerAdsDao _bannerAdsDao;

        public BannerAdsService(IBannerAdsDao bannerAdsDao)
        {
            _bannerAdsDao = bannerAdsDao;
        }

        public async Task<BannerAdsViewModel> GetBannerAdsByCode(string langCode, string code)
        {
            var result = await _bannerAdsDao.GetBannerAdsByCode(langCode, code);
            return result;
        }

        public async Task<BannerTwoSlideModel> GetBannerAdsByCode(string langCode)
        {
            var bannerLeft =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.BannerTrangchuDocTrai);
            var bannerRight =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.BannerTrangchuDocPhai);
            var result = new BannerTwoSlideModel
            {
                BannerToLeft = JsonConvert.DeserializeObject<BannerHomePage>(bannerLeft.MetaData),
                BannerToRight = JsonConvert.DeserializeObject<BannerHomePage>(bannerRight.MetaData)
            };
            return result;
        }

        public async Task<TopSlideBannerHomePageViewModel> GetSlideBanner(string langCode)
        {
            var slideBanner = await _bannerAdsDao.GetBannerAdsByCode(langCode,BannerLocationStatic.SlideTrangchuTren);
            var result=new TopSlideBannerHomePageViewModel()
            {
                TopSlideBanner = JsonConvert.DeserializeObject<List<BannerHomePage>>(slideBanner.MetaData)
            };
            return result;
        }

        public async Task<StemHomePage> GetAllStemHomgePage(string langCode)
        {
            var stem = await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.StemTrangChu);
            var result=new StemHomePage()
            {
                BannerHomePages = JsonConvert.DeserializeObject<List<BannerHomePage>>(stem.MetaData)
            };
            return result;
        }

        public string GetConfigByName(string langCode, string name)
        {
            var result = _bannerAdsDao.GetConfigByName(langCode, name);
            return result;
        }
    }
}