﻿using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Services.Common.Models;

namespace Enterbuy.Services.Common.Services.Interfaces
{
    public interface IBannerAdsService
    {
        Task<BannerTwoSlideModel> GetBannerAdsByCode(string langCode);
        Task<TopSlideBannerHomePageViewModel> GetSlideBanner(string langCode);
        Task<StemHomePage> GetAllStemHomgePage(string langCode);
        string GetConfigByName(string langCode, string name);
    }
}