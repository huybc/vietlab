﻿using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Http.Controllers;
using Enterbuy.Services.Common.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Common.Controlllers
{
    public class CommonController : BaseController
    {
        private readonly IArticleDao _articleDao;
        private readonly IZoneDao _zoneDao;
        private readonly IConfigService _configService;

        public CommonController(IZoneDao zoneDao, IConfigService configService, IArticleDao articleDao)
        {
            _zoneDao = zoneDao;
            _configService = configService;
            _articleDao = articleDao;
        }
        [Route("{calias}/{alias}.html")]
        public IActionResult RedirectActionOld(string calias, string alias)
        {
            return RedirectToAction("RedirectAction", new { alias = alias.ToLower() });

        }
        [Route("san-pham/{alias}.html")]
        public async Task<IActionResult> RedirectAction(string alias)
        {
            var zoneTar = await _zoneDao.GetObjectByAlias(alias, Utils.Utility.DefaultLang);
            ViewData["IsExpand"] = false;
            if (zoneTar != null)
            {
                if (alias.Equals(zoneTar.ObjectUrl))
                {
                    ViewBag.ZoneId = zoneTar.ObjectId;
                    ViewBag.Type = zoneTar.ObjectType;
                }
                else
                {
                    return Redirect(Utils.BaseBA.UrlProduct("", zoneTar.ObjectUrl));
                }
            }
            else
            {
                return NotFound();
            }
            return View();

        }
        [HttpPost]
        public IActionResult CreateComment(int objectId, int objectType, string name, string phoneOrEmail, string avatar, string content, string type, int rate, int parentId)
        {
            var result = 0;
            try
            {
                result = _configService.CreateComment(objectId, objectType, name, phoneOrEmail, avatar, content, type, rate, "vi-VN", parentId);
            }
            catch (System.Exception ex)
            {

                throw;
            }
            return Ok(result);
        }
        public IActionResult ModalServiceSlideBanner(int articleId)
        {
            ArticleDetail articleDetail = new ArticleDetail();
            try
            {
                articleDetail = _articleDao.GetArticleDetail(articleId, "vi-VN");
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
            return View(articleDetail);
        }
        [HttpPost]
        public IActionResult CreateRating(int objectId, int objectType, int rate)
        {
            var result = _configService.CreateRating(objectId, objectType, rate);
            return Ok(result);
        }
        [HttpPost]
        public IActionResult CreateViewCount(int objectId, string type)
        {
            var result = _configService.CreateViewCount(objectId, type);
            return Ok(result);
        }
        [HttpPost]
        public IActionResult CreateServiceTicket(ServiceTicket ticket)
        {

            var result = _configService.CreateContact(ticket);


            return Ok(result);
        }
        [HttpPost]
        public IActionResult GetReplyComment(int id, int obj_id, int obj_type, string fullName, string email)
        {
            if (obj_id > 0)
            {
                ViewBag.Id = id;
                ViewBag.ObjId = obj_id;
                ViewBag.ObjType = obj_type;
                ViewBag.FullName = fullName;
                ViewBag.Email = email;
                return View();
            }
            return BadRequest();

        }

        [HttpPost]
        public IActionResult GetCommentList(int object_id, int object_type, int? pageIndex)
        {
            pageIndex = pageIndex ?? 1;
            return ViewComponent("Comment", new { object_id = object_id, object_type = object_type, pageIndex = pageIndex });
        }

    }
}
