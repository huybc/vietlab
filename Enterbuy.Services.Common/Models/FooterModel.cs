﻿using System.Collections.Generic;

namespace Enterbuy.Services.Common.Models
{
    public class FooterModel
    {
        public List<BannerHomePage> Column1 { get; set; }
        public List<BannerHomePage> Column2 { get; set; }
        public List<BannerHomePage> Column3 { get; set; }
        public List<BannerHomePage> ColumnDepartment { get; set; }
    }
}