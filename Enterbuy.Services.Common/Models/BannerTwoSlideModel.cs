﻿using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Services.Common.Models
{
    public class BannerTwoSlideModel
    {
        public BannerHomePage BannerToLeft { get; set; }
        public BannerHomePage BannerToRight { get; set; }
    }
}