﻿using System.Collections.Generic;

namespace Enterbuy.Services.Common.Models
{
    public class StemHomePage
    {
        public List<BannerHomePage> BannerHomePages { get; set; }
    }
}