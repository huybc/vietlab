﻿using System.Collections.Generic;

namespace Enterbuy.Services.Common.Models
{
    public class TopSlideBannerHomePageViewModel
    {
        public List<BannerHomePage> TopSlideBanner { get; set; }
    }
}