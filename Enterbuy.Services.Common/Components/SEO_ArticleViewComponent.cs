﻿using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Common.Components
{
    public class SEO_ArticleViewComponent : ViewComponent
    {
        
        public SEO_ArticleViewComponent()
        {
            
        }
        public IViewComponentResult Invoke(ArticleDetail article)
        {
            ViewBag.Detail = article;
            return View();
        }
    }
}
