﻿using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class SEO_ZoneViewComponent : ViewComponent
    {
        public SEO_ZoneViewComponent()
        {

        }
        public IViewComponentResult Invoke(ZoneDetail zone)
        {
            ViewBag.Detail = zone;
            return View();
        }
    }
}
