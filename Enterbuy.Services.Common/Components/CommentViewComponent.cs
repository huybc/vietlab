﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class CommentViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(int object_id, int object_type, int? pageIndex)
        {
            pageIndex = pageIndex ?? 1;
            var pageSize = 10;
            ViewBag.ObjId = object_id;
            ViewBag.ObjType = object_type;
            ViewBag.Index = pageIndex.Value;
            ViewBag.Size = pageSize;
            return View();
        }
    }
}