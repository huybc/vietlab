﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Services.Common.Services.Interfaces;
namespace Enterbuy.Services.Common.Components
{
    public class HeaderWebsiteViewComponent : ViewComponent
    {
        private readonly IMenuZoneServices _menuZoneServices;

        public HeaderWebsiteViewComponent(IMenuZoneServices menuZoneServices)
        {
            _menuZoneServices = menuZoneServices;
        }

        public async Task<IViewComponentResult> InvokeAsync(bool isExpand)
        {            
            var result = await _menuZoneServices.GetZoneByTreeViewMinifies((int)TypeZone.Product, "vi-VN", 0);
            foreach (var item in result)
            {
                item.Url = $"/{item.Url}";
            }
            ViewBag.IsExpand = isExpand;
            return View(result);
        }
    }
}