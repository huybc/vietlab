﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class BlogPagingViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(string alias, int? pageIndex, int? pageSize, int total)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 10;
            ViewBag.Alias = alias;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            ViewBag.Total = total;
            return View();
        }
    }
}