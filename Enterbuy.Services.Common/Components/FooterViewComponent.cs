﻿using Enterbuy.Core.Statics;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Services.Common.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class FooterViewComponent : ViewComponent
    {
        private readonly IBannerAdsDao _bannerAdsDao;

        public FooterViewComponent(IBannerAdsDao bannerAdsDao)
        {
            _bannerAdsDao = bannerAdsDao;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var columnOne = await _bannerAdsDao.GetBannerAdsByCode("vi-VN", BannerLocationStatic.Column1);
            var columnTwo = await _bannerAdsDao.GetBannerAdsByCode("vi-VN", BannerLocationStatic.Column2);
            var columnThree = await _bannerAdsDao.GetBannerAdsByCode("vi-VN", BannerLocationStatic.Column3);
            var columnDepartment = await _bannerAdsDao.GetBannerAdsByCode("vi-VN", BannerLocationStatic.ColumnDepartment);
            var model = new FooterModel()
            {
                Column1 = JsonConvert.DeserializeObject<List<BannerHomePage>>(columnOne.MetaData),
                Column2 = JsonConvert.DeserializeObject<List<BannerHomePage>>(columnTwo.MetaData),
                Column3 = JsonConvert.DeserializeObject<List<BannerHomePage>>(columnThree.MetaData),
                ColumnDepartment = JsonConvert.DeserializeObject<List<BannerHomePage>>(columnDepartment.MetaData),
            };
            return View(model);
        }
    }
}