﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class CategoryMobilePopupViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}