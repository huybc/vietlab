﻿using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Common.Components
{
    public class RatingStarProductDetailsViewComponent : ViewComponent
    {
        private readonly IProductDao _productDao;

        public RatingStarProductDetailsViewComponent(IProductDao productDao)
        {
            _productDao = productDao;
        }

        public async Task<IViewComponentResult> InvokeAsync(int objectId, int objectType, string hTag)
        {
            var rate = _productDao.GetRatingByObjectId(objectId, objectType);
            ViewBag.ObjId = objectId;
            ViewBag.ObjType = objectType;
            ViewBag.HTag = hTag;
            ViewBag.Rate = rate;
            return View();
        }
    }
}