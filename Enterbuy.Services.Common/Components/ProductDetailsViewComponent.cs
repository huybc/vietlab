﻿using System.Linq;
using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using Utils;

namespace Enterbuy.Services.Common.Components
{
    public class ProductDetailsViewComponent : ViewComponent
    {
        private readonly IProductDao _productDao;
        private readonly ICookieLocationUtility _cookieLocation;
        public ProductDetailsViewComponent(IProductDao productDao, ICookieLocationUtility cookieLocation)
        {
            _productDao = productDao;
            _cookieLocation = cookieLocation;
        }

        public async Task<IViewComponentResult> InvokeAsync(ProductDetail productInfomatinDetail)
        {
            if (productInfomatinDetail != null)
            {
                var location = _cookieLocation.SetCookieDefault();

                var productSpectificationDetail = await _productDao.GetProductSpectificationDetail(productInfomatinDetail.Id, Utility.DefaultLang);
                var productPriceDetail = await _productDao.GetProductPriceInLocationDetail(productInfomatinDetail.Id, Utility.DefaultLang);
                
                var totalRowCombo = 0;
                var productsInProductCombo = _productDao.GetProductsInProductById(productInfomatinDetail.Id, "com-bo", location.LocationId, Utility.DefaultLang, 1, 4, out totalRowCombo);
                var promotionsInProduct = await _productDao.GetPromotionInProduct(productInfomatinDetail.Id,Utility.DefaultLang);
                
                var sameZoneTotal = 0;
                var productSameZone = _productDao.GetProductInZoneByZoneIdMinify(productInfomatinDetail.ZoneId, location.LocationId, Utility.DefaultLang, 1, 6, out sameZoneTotal);
                ViewBag.Infomation = productInfomatinDetail;
                ViewBag.Zone = productInfomatinDetail.ZoneId;
                ViewBag.ZoneUrl = productInfomatinDetail.ZoneUrl;
                ViewBag.Spectification = productSpectificationDetail;
                ViewBag.SameZone = productSameZone;
                ViewBag.SameTotal = sameZoneTotal;
                ViewBag.LocationName = location.LocationName;
            
                var defaultPriceItem = productPriceDetail.FirstOrDefault(r => r.LocationId == location.LocationId);
                if (defaultPriceItem != null)
                {
                    ViewBag.DefaultLocationPrice = defaultPriceItem;
                }
                ViewBag.ListLocation = productPriceDetail;
                ViewBag.Combo = productsInProductCombo;
                ViewBag.Promotion = promotionsInProduct;

                var listComment = await _productDao.GetCommentPublisedByObjectId(productInfomatinDetail.Id, (int)CommentType.Product);
                ViewBag.Comments = listComment;
            }
            return View();
        }
    }
}