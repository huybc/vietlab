﻿using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Common.Components
{
    public class ProductListInArticleViewComponent : ViewComponent
    {
        private readonly IProductDao _productDao;

        public ProductListInArticleViewComponent(IProductDao productDao)
        {
            _productDao = productDao;
        }

        public async Task<IViewComponentResult> InvokeAsync(string product_ids, int location_id)
        {
            var total_row = 0;
            var model = _productDao.GetProductInListProductsMinify(product_ids, location_id, "vi-VN", 1, 4, out total_row);
            ViewBag.Total = total_row;
            return View(model);
        }
    }
}
