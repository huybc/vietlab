﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Services.Common.Services.Interfaces;
using Utils;

namespace Enterbuy.Services.Common.Components
{
    public class ServiceInSlideBannerViewComponent : ViewComponent
    {
        private readonly IBannerAdsService _bannerAdsService;

        public ServiceInSlideBannerViewComponent(IBannerAdsService bannerAdsService)
        {
            _bannerAdsService = bannerAdsService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await _bannerAdsService.GetAllStemHomgePage("vi-VN");
            foreach (var item in result.BannerHomePages)
            {
                item.Image = UIHelper.StoreFilePath(item.Image, false);
            }
            return View(result);
        }
    }
}