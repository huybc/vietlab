﻿using Enterbuy.Services.Common.Services.Interfaces;
using Utils;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class BannerTwoSideViewComponent : ViewComponent
    {
        private readonly IBannerAdsService _bannerAdsService;

        public BannerTwoSideViewComponent(IBannerAdsService bannerAdsService)
        {
            _bannerAdsService = bannerAdsService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await _bannerAdsService.GetBannerAdsByCode("vi-VN");
            result.BannerToRight.Image = UIHelper.StoreFilePath(result.BannerToRight.Image, false);
            result.BannerToLeft.Image = UIHelper.StoreFilePath(result.BannerToLeft.Image, false);
            return View(result);
        }
    }
}