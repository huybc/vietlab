﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Services.Common.Services.Interfaces;
using Utils;

namespace Enterbuy.Services.Common.Components
{
    public class SlideBannerViewComponent : ViewComponent
    {
        private readonly IBannerAdsService _bannerAdsService;

        public SlideBannerViewComponent(IBannerAdsService bannerAdsService)
        {
            _bannerAdsService = bannerAdsService;
        }

        public async Task<IViewComponentResult> InvokeAsync(bool isService)
        {
            var result = await _bannerAdsService.GetSlideBanner("vi-VN");
            foreach (var item in result.TopSlideBanner.OrderBy(n => n.Order))
            {
                item.Image = UIHelper.StoreFilePath(item.Image, false);
            }
            ViewBag.IsService = isService;
            return View(result);
        }
    }
}