﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Utils;
using MI.Dapper.Data.Repositories.Interfaces;

namespace Tucky.Controllers
{
    public class CouponController : Controller
    {
        private readonly ICouponRepository _Couponrepo;
        public CouponController(ICouponRepository repo)
        {
            _Couponrepo = repo;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] FilterQuery filter)
        {
            try
            {
                var data = await _Couponrepo.GetAllPaging(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
            }
            return Ok();
        }
        [HttpPost]
        public IActionResult CheckCode(string Name,string Code)
        {
            try
            {
                var Check = _Couponrepo.CheckCode(Name, Code);
                return Ok(Check);

            }
            catch
            {

            }
            return Ok();
        }
         public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var result = await _Couponrepo.GetById(id);
                return Ok(result);

            }
            catch
            {

            }
            return Ok();
        }

        [HttpPost]
        public IActionResult UpdateUseCode(string Ma)
        {
            var rs = _Couponrepo.UpdateCouPonChill(Ma);
            return Ok(rs);
        }
    }

}