﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tucky.Services.Coupon.ViewModel
{
    public class CouponViewModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int? DiscountOption { get; set; }
        public int? NumberOfUsed { get; set; }
        public int? QuantityDiscount { get; set; }
        public bool? Locked { get; set; }
        public string ValueDiscount { get; set; }
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
