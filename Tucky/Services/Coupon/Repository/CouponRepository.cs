﻿using MI.Dal.IDbContext;
using MI.Entity.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tucky.Services.Coupon.Repository
{
    public interface ICouponRepository
    {
        IQueryable<MI.Entity.Models.Coupon> GetAll();
        bool CheckSeri(string Ma, string Code);
    }
    public class CouponRepository : ICouponRepository
    {
        private IDbContext _dbContext;
        DbSet<MI.Entity.Models.Coupon> _entity;

        public CouponRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
            _entity = _dbContext.Set<MI.Entity.Models.Coupon>();
        }
        public IQueryable<MI.Entity.Models.Coupon> GetAll()
        {
            return _entity.AsTracking();
        }
        public bool CheckSeri(string Ma,string Code)
        {
            if(!string.IsNullOrEmpty(Ma) && !string.IsNullOrEmpty(Ma))
            {
                var resull = _entity.Where(x => x.Name == Ma && x.Code == Code);
                if (resull.Count() > 0)
                {
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}
