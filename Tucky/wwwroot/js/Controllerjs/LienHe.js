﻿var R = {
    Init: function () {
        R.RegisterEvent();
    },
    RegisterEvent: function () {
        $('#form-lien-he').off('submit').on('submit', function () {
            R.GetParamLienHe('#form-lien-he');
            R.RefreshForm('#form-lien-he')

            return false;
        })
        $('#formMuaHang').off('submit').on('submit', function () {
            var el = $('#formMuaHang');
            R.SenDatHang(el);
            $('#modal-close-outside').modal("hide");
            return false;
        })
        $('#DatHangMaSeri').off('submit').on('submit', function () {
            var el = $('#DatHangMaSeri');
            R.SenDatHang(el);
            return false;
        })
        $('#CheckCode').off('click').on('click', function () {
            var el = $('#DatHangMaSeri');
            var ma = el.find('.Nameseri').val();
            var code = el.find('.codeseri').val();
            var Param = {
                Name: ma,
                Code: code
            }
            R.CheckCodeAlert(Param, el);
        })
        $('#CheckCodeModa').off('click').on('click', function () {
            var el = $('#formMuaHang');
            var ma = el.find('.Nameseri').val();
            var code = el.find('.codeseri').val();
            var Param = {
                Name: ma,
                Code: code
            }
            R.CheckCodeAlert(Param, el);
        })
    },
    SenDatHang: function (el) {
        var ma = el.find('.Nameseri').val();
        var code = el.find('.codeseri').val();
        var Params = {
            Name: ma,
            Code: code
        }
        var urls = '/Coupon/CheckCode';
        var url = 'https://vietlab.migroup.asia/Order/CreateOrder'
        var OderCode = 'TucKy_Order';
        var pr = R.GetParam(el);
        var prprms = {
            PromotionId: 1,
            LogType: "1",
            LogName: "TucKy",
            LogValue: 1

        }
        $.post(urls, Params, function (reponcheck) {
            if (reponcheck > 0) {
                $.post('/Coupon/GetById', { id: reponcheck }, function (repon2) {
                    if (repon2 != null) {
                        var disc = repon2.discountOption;
                        var disvl = repon2.valueDiscount
                        var prProduct = {
                            ProductId: 1,
                            Name: "TucKy",
                            LogPrice: 2300000,
                            Quantity: 1,
                            OrderSourceType: 1,
                            OrderSourceId: 1,
                            Voucher: ma,
                            VoucherType: disc,
                            VoucherPrice: disvl,
                            VoucherMeta: code,
                            Promotions: [prprms]
                        };
                        var param = {
                            OrderCode: OderCode,
                            Customer: pr,
                            Products: [prProduct],
                            Extras: ["empty"]
                        }
                        var urlUpdate = '/Coupon/UpdateUseCode';
                        var body = '<div>Tên khách hàng : ' + pr.Name + '</div>' +
                            '<div>Số điện thoại : ' + pr.PhoneNumber + '</div>' +
                            '<div>địa chỉ : ' + pr.Address + '</div>' +
                            '<div>Email : ' + pr.Email + '</div>';
                        var paramSenmail = {
                            type: 2,
                            body: body,
                            KhuVuc: 'Hà Nội'
                        }
                        $.post(url, param, function (repon) {
                            if (repon > 0) {
                                alert("Đặt hàng thành công chúng tôi sẽ liên hệ với bạn sớm nhất!")
                                R.SendMail(paramSenmail)
                                R.RefreshForm(el);
                                $.post(urlUpdate, { Ma: code }, function (rp) {
                                    if (!rp) {
                                        alert('Có lỗi khi áp dụng má vui lòng liên hệ với cửa hàng!')
                                    }
                                })
                            }
                        })
                    }
                })
              
            }
            else {
                var prProduct = {
                    ProductId: 1,
                    Name: "TucKy",
                    LogPrice: 10,
                    Quantity: 1,
                    OrderSourceType: 1,
                    OrderSourceId: 1,
                    Voucher: 'Trống',
                    VoucherType: 0,
                    VoucherPrice: 0,
                    VoucherMeta: 'Trống',
                    Promotions: [prprms]
                };
                var param = {
                    OrderCode: OderCode,
                    Customer: pr,
                    Products: [prProduct],
                    Extras: ["empty"]
                }
                var urlUpdate = '/Coupon/UpdateUseCode';
                var body = '<div>Tên khách hàng : ' + pr.Name + '</div>' +
                    '<div>Số điện thoại : ' + pr.PhoneNumber + '</div>' +
                    '<div>địa chỉ : ' + pr.Address + '</div>' +
                    '<div>Email : ' + pr.Email + '</div>';
                var paramSenmail = {
                    type: 2,
                    body: body,
                    KhuVuc: 'Hà Nội'
                }
                $.post(url, param, function (repon) {
                    if (repon > 0) {
                        alert("Đặt hàng thành công chúng tôi sẽ liên hệ với bạn sớm nhất!")
                        R.SendMail(paramSenmail)
                        R.RefreshForm(el);
                    }
                })
            }
        })
    },
    GetParam: function (el) {
        var name = $(el).find('.name-input').val();
        var phone = $(el).find('.phone-input').val();
        var email = $(el).find('.mail-input').val();
        var add = $(el).find('.address').val();
        var params = {
            Name: name,
            PhoneNumber: phone,
            Address: add,
            Email: email,
            Note: "",
            Gender: ""
        }
        return params;

    },
    GetParamLienHe: function (el) {
        var name = $(el).find('.name-input').val();
        var phone = $(el).find('.phone-input').val();
        var email = $(el).find('.mail-input').val();
        var add = $(el).find('.address').val();
        var company = $(el).find('.company-input').val();
        var checkedValue = false;
        var type = $(el).data('type')
        var nd = $(el).find('.mess-input').val();
        var KhachHang = "Liên hệ Tucky";
        var yc = "Khách hàng liên hệ Tucky";
        var body = '<div>Tên khách hàng : ' + name + '</div>' +
            '<div>Số điện thoại : ' + phone + '</div>' +
            '<div>Nội dung khách gửi : ' + nd + '</div>';
        var param = {
            type: KhachHang,
            body: body,
            KhuVuc: 'Hà Nội'
        }
        var params = {
            Name: name,
            Phone: phone,
            Address: add,
            Email: email,
            Company: company,
            KhuVuc: 'Hà Nội',
            Title: yc,
            Content: nd,
            Type: type,
            Source: 'web',

        }

        R.SendContact(params, param);
    },
    SendContact: function (params, param) {
        var url = "https://vietlab.migroup.asia/Extra/CreateServiceTicket";
        $.post(url, params, function (response) {
            alert("Thư của bạn đã được gửi chúng tôi sẽ liên hệ với bạn sớm nhất!")
            //$('#modal-xn').modal('show');
            // R.Contact.CloseModalAndClearText();
            R.SendMail(param);
            //return false;
        })
    },
    SendMail: function (param) {
        var urlSenMail = "https://vietlab.migroup.asia/Extras/SendMail"
        $.post(urlSenMail, param, function (response) {
            console.log(response)
            //$('#modal-xn').modal('show');
            //R.CloseModalAndClearText();
            //return false;
        })
    },
    RefreshForm: function (el) {
        $(el).find('.name-input').val('');
        $(el).find('.phone-input').val('');
        $(el).find('.mail-input').val('');
        $(el).find('.address').val('');
        $(el).find('.Nameseri').val('');
        $(el).find('.codeseri').val('');
        el.find('#mess-voucher').css('display', 'none');
        el.find('#mess-voucher-success').css('display', 'none');
    },
    CheckCodeAlert: function (pr, el) {
        var url = '/Coupon/CheckCode';
        $.post(url, pr, function (repon) {
            if (repon) {
                el.find('#mess-voucher').css('display', 'none')
                el.find('#mess-voucher-success').css('display', 'block')
            }
            else {
                el.find('#mess-voucher-success').css('display', 'none')
                el.find('#mess-voucher').css('display', 'block')

            }
        })
    }
}
$(function () {
    R.Init()
}) 
