﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class BannerAdsDao : BaseSqlServerDao<Ads>, IBannerAdsDao
    {
        public BannerAdsDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }


        public async Task<BannerAdsViewModel> GetBannerAdsByCode(string langCode, string code)
        {
            try
            {
                var spName = "usp_Web_Get_BannerAds_By_Code";
                var param = new DynamicParameters();
                param.Add("@langCode", langCode);
                param.Add("@code", code);
                var result = await QuerySPAsync<BannerAdsViewModel>(spName, param);
                return result.FirstOrDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public string GetConfigByName(string lang_code, string name)
        {

            try
            {
                var p = new DynamicParameters();
                var spName = "usp_Web_GetConfigByName";
                p.Add("@lang_code", lang_code);
                p.Add("@configName", name);
                var r = QuerySP<string>(spName, p);
                if (r != null && r.Any())
                    return r.FirstOrDefault();
                else return "";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "";
            }



        }
    }
}