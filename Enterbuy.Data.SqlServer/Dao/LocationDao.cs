﻿using Dapper;
using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class LocationDao : BaseSqlServerDao<StoreViewModel>, ILocationDao
    {
        public LocationDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {

        }
        public List<StoreResponse> GetNearLocation(float Longitude, float Latitude, int distance, string langCode, int sortOrder, out int totalRows)
        {
            var p = new DynamicParameters();
            var commandText = "NearLocationFullProperty";
            p.Add("@Longitude", Longitude);
            p.Add("@Latitude", Latitude);
            p.Add("@distance", distance);
            p.Add("@LanguageCode", langCode);
            p.Add("@SortOrder", sortOrder);
            p.Add("@TotalRows", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<StoreResponse>(commandText, p);
            totalRows = p.Get<int>("@TotalRows");
            return result;
        }
        public LocationViewModel GetLocationFirst(string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetLocations";
            p.Add("@lang_code", lang_code);
            var result = QuerySP<LocationViewModel>(commandText, p);
            if (result != null && result.Count > 0)
                return result.FirstOrDefault();
            return new LocationViewModel();
        }

        public List<LocationViewModel> GetLocations(string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetLocations";
            p.Add("@lang_code", lang_code);
            var result = QuerySP<LocationViewModel>(commandText, p);
            return result;
        }
    }
}
