﻿using Enterbuy.Data.Sql.Dto;

namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class Manufacturer:BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
    }
}
