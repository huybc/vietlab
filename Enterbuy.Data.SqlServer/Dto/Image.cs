﻿namespace Enterbuy.Data.SqlServer.Dto
{
    public class Image
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
