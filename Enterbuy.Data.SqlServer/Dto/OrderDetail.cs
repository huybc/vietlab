﻿using System.Collections.Generic;

namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class OrderDetail
    {
        public OrderDetail()
        {
            OrderPromotionDetail = new HashSet<OrderPromotionDetail>();
        }

        public int Id { get; set; }
        public int? OrderId { get; set; }
        public int? ProductId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? LogPrice { get; set; }

        public Orders Order { get; set; }
        public ICollection<OrderPromotionDetail> OrderPromotionDetail { get; set; }
    }
}
