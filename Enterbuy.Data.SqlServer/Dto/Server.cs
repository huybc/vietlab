﻿using System;

namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class Server
    {
        public string Id { get; set; }
        public string Data { get; set; }
        public DateTime LastHeartbeat { get; set; }
    }
}
