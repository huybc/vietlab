﻿using System;
using System.Collections.Generic;
using Enterbuy.Data.Sql.Dto;

namespace Enterbuy.Data.SqlServer.Dto
{
    public class Product : BaseEntity
    {
        public int Id { get; set; }
        public int? Status { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
        public string AvatarArray { get; set; }
        public double? Price { get; set; }
        public double? DiscountPrice { get; set; }
        public string Warranty { get; set; }
        public int? ManufacturerId { get; set; }
        public string Code { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public string Unit { get; set; }
        public int? Quantity { get; set; }
        public string PropertyId { get; set; }
        public string Color { get; set; }
        public string Name { get; set; }
        public string Guarantee { get; set; }
        public int? MaterialType { get; set; }
        public double DiscountPercent { get; set; }
        public string MetaFile { get; set; }

    }


    public class FilterAreaCooked
    {
        public int SpectificationId { get; set; }
        public string Name { get; set; }
        public List<FilterArea> Values { get; set; }
    }

    public class FilterArea
    {
        public int SpectificationId { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public string Value { get; set; }
        public int ZoneId { get; set; }
    }

    public class FilterProductBySpectification
    {
        public int parentId { get; set; }
        public string lang_code { get; set; }
        public int locationId { get; set; }
        public int manufacture_id { get; set; }
        public int min_price { get; set; }
        public int max_price { get; set; }
        public int sort_price { get; set; }
        public int sort_rate { get; set; }
        //public string color_code { get; set; } = "";
        //List<FilterSpectification> filter
        public List<FilterSpectification> filter { get; set; }
        public string filter_text { get; set; }
        public int material_type { get; set; }
        public int pageNumber { get; set; }
        public int pageSize { get; set; }


    }
    public class FilterSpectification
    {
        public int SpectificationId { get; set; }
        public string Value { get; set; }
        public FilterSpectification()
        {
            this.SpectificationId = 0;
            this.Value = "";
        }
    }

}
