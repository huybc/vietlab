﻿namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class ProductInProduct
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? ProductItemId { get; set; }
        public string Type { get; set; }
        public int? SortOrder { get; set; }
    }
}
