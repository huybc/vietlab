﻿using System;

namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class AspNetRoleClaims
    {
        public int Id { get; set; }
        public Guid RoleId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }

        public AspNetRoles Role { get; set; }
    }
}
