﻿namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class MaintainSpectifications
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
    }
}
