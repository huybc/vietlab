﻿using Enterbuy.Data.Sql.Dto;
using System.Collections.Generic;

namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class Property
    {
       

        public int Id { get; set; }
        public byte GroupId { get; set; }
        public string Thumb { get; set; }
        public string Position { get; set; }
        public string Name { get; set; }

       
    }
}
