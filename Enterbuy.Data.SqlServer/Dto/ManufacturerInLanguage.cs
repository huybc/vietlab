﻿namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class ManufacturerInLanguage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string LanguageCode { get; set; }
        public int ManufacturerId { get; set; }
    }
}
