﻿using System;

namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class InfoCard
    {
        public string MonthNumber { get; set; }
        public string InterestRate { get; set; }
    }
}
