﻿namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class UserPermission
    {
        public int UserId { get; set; }
        public int PermissionId { get; set; }
        public int ZoneId { get; set; }
    }
}
