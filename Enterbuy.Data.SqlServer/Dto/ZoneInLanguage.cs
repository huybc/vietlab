﻿namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class ZoneInLanguage
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Url { get; set; }
        public string BannerLink { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string LanguageCode { get; set; }
        public int ZoneId { get; set; }

        public Language LanguageCodeNavigation { get; set; }
        public Zone Zone { get; set; }
    }
}
