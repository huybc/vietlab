﻿using System.Collections.Generic;
using Enterbuy.Data.Sql.Dto;

namespace Enterbuy.Data.SqlServer.Dto
{
    public class Actions:BaseEntity
    {
        public Actions()
        {
            ActionInFunctions = new HashSet<ActionInFunctions>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public int? SortOrder { get; set; }
        public bool? IsActive { get; set; }

        public ICollection<ActionInFunctions> ActionInFunctions { get; set; }
    }
}
