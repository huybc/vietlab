﻿using Enterbuy.Data.Sql.Dto;

namespace Enterbuy.Data.SqlServer.Dto
{
    public class ActionInFunctions:BaseEntity
    {
        public string FunctionId { get; set; }
        public string ActionId { get; set; }

        public Actions Action { get; set; }
        public Functions Function { get; set; }
    }
}
