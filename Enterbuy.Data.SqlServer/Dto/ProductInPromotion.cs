﻿namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class ProductInPromotion
    {
        public int Id { get; set; }
        public int? PromotionId { get; set; }
        public int? ProductId { get; set; }

        public Product Product { get; set; }
    }
}
