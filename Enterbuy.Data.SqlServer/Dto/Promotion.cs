﻿using Enterbuy.Data.Sql.Dto;

namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class Promotion : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool? IsDiscountPrice { get; set; }
        public decimal? Value { get; set; }
        public byte Status { get; set; }
    }
}
