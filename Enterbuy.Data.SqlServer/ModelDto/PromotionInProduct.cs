﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class PromotionInProduct
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        public decimal Value { get; set; }
        public int IsDiscountPrice { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProductId { get; set; }
    }
}
