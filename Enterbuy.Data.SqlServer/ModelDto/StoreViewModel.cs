﻿using Enterbuy.Data.Sql.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class StoreViewModel : BaseEntity
    {
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public int Distance { get; set; }
        public string LanguageCode { get; set; }
        public int SortOrder { get; set; }
    }
    public class StoreResponse
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Url { get; set; }
        public string LanguageCode { get; set; }
        public string Email { get; set; }
        public string Hotline { get; set; }
        public string Distance { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public int LocationId { get; set; }
    }
}
