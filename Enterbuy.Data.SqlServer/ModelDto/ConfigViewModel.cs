﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ConfigViewModel
    {
        public string Content { get; set; }
    }
    public class ConfigView
    {
        public string Content { get; set; }
        public string Page { get; set; }
        public string ConfigName { get; set; }
        public string LanguageCode { get; set; }
    }
}
