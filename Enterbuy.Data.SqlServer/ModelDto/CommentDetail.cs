﻿using System;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class CommentDetail
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedByAdminId { get; set; }
        public string Content { get; set; }
        public int ObjectId { get; set; }
        public int ObjectType { get; set; }
        public string LanguageCode { get; set; }
        public string Name { get; set; }
        public string PhoneOrMail { get; set; }
        public string Avatar { get; set; }
        public string Type { get; set; }
        public int Rate { get; set; }
    }
    public class ServiceTicket
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Type { get; set; }
        public string Source { get; set; }
    }
}