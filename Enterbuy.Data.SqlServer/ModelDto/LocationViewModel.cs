﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class LocationViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
