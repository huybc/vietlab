﻿namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ProductSaleInMonth : ProductMinify
    {
        public decimal FlashSalePrice { get; set; }
        public int FlashSaleQuantity { get; set; }
        public int CountSellInFlashSale { get; set; }
        public int FlashSaleQuantityRemain { get; set; }
    }
}