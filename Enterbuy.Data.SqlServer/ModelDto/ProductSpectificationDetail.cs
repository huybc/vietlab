﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ProductSpectificationDetail
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
