﻿namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ManufactureModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
    }
}