﻿using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dto;
using Enterbuy.Data.SqlServer.DbContexts;
using Enterbuy.Statics.Configurations;
using Microsoft.EntityFrameworkCore;
using System;

namespace Enterbuy.Data.SqlServer
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private readonly EnterbuyDbContext _enterbuyDbContext;

        public DatabaseFactory(EnterbuyDbContext enterbuyDbContext)
        {
            this._enterbuyDbContext = enterbuyDbContext;
        }

        public DbContext GetDbContext<T>() where T : BaseDto
        {
            return _enterbuyDbContext;
        }

        public string GetConnectionString(Type type, DbActionType dbActionType = DbActionType.Write)
        {
            if (dbActionType == DbActionType.Write)
                return AppSettings.Get<string>("Databases:SqlServer:ConnectionStrings:Write");
            else if (dbActionType == DbActionType.Read)
                return AppSettings.Get<string>("Databases:SqlServer:ConnectionStrings:Read");
            return AppSettings.Get<string>("Databases:SqlServer:ConnectionStrings:Default");
        }
    }
}