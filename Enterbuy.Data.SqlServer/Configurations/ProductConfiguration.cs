﻿using Enterbuy.Data.SqlServer.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Enterbuy.Data.SqlServer.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.Property(e => e.Avatar)
                .HasMaxLength(300)
                .IsUnicode(false);

            builder.Property(e => e.AvatarArray)
                .HasMaxLength(300)
                .IsUnicode(false);

            builder.Property(e => e.Code).HasMaxLength(20);

            builder.Property(e => e.Color).HasMaxLength(500);

            builder.Property(e => e.CreatedBy)
                .HasMaxLength(250)
                .IsUnicode(false);

            builder.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.Guarantee).HasMaxLength(500);

            builder.Property(e => e.ModifyBy)
                .HasMaxLength(250)
                .IsUnicode(false);

            builder.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.Name).HasMaxLength(500);

            builder.Property(e => e.PropertyId).HasMaxLength(500);

            builder.Property(e => e.Status).HasDefaultValueSql("((2))");

            builder.Property(e => e.Unit).HasMaxLength(20);

            builder.Property(e => e.Url)
                .HasMaxLength(500)
                .IsUnicode(false);

            builder.Property(e => e.Warranty).HasMaxLength(50);
        }
    }
}