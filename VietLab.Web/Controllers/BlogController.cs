﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VietLab.Web.Services.Article.Repository;
using VietLab.Web.Services.Article.ViewModel;
using VietLab.Web.Services.Product.Repository;
using VietLab.Web.Services.Zone.Repository;
using VietLab.Web.Services.Zone.ViewModal;
using MI.Entity.Enums;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
//using MI.Cache;
//using StackExchange.Redis;

namespace VietLab.Web.Controllers
{
    public class BlogController : Controller
    {
        private readonly IZoneRepository _zoneRepository;
        private readonly IProductRepository _productRepository;
        private readonly IArticleRepository _articleRepository;
        const string CookieLocationId = "_LocationId";
        const string CookieLocationName = "_LocationName";
        private string _currentLanguage;
        private string _currentLanguageCode;

        //cache 
        //private readonly IDistributedCache _distributedCache;
        //private readonly IConfiguration _configuration;
        //private readonly IConnectionMultiplexer _multiplexer;
        // cache end
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        private string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    IRequestCultureFeature feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                }

                return _currentLanguageCode;
            }


        }
        //public BlogController(IZoneRepository zoneRepository, IProductRepository productRepository, IArticleRepository articleRepository, IDistributedCache distributedCache, IConfiguration configuration, IConnectionMultiplexer multiplexer)
        public BlogController(IZoneRepository zoneRepository, IProductRepository productRepository, IArticleRepository articleRepository)
        {
            _zoneRepository = zoneRepository;
            _productRepository = productRepository;
            _articleRepository = articleRepository;
            //cache
            //_distributedCache = distributedCache;
            //_configuration = configuration;
            //_multiplexer = multiplexer;
            // cache end
        }

        //[HttpGet("getalltagv2")]
        //public async Task<IActionResult> GetAlltagCache()
        //{
        //    var tags = await _productRepository.getxxxx();

        //    var keyCache = string.Format("", 1, 2, 2);
        //    var result = _distributedCache.GetOrSetCache(keyCache, () => tags, _configuration);
        //    return Ok(result);
        //}

        //public bool RemoveCache(string key)
        //{
        //    RedisUtils.DeleteAllCacheAsyn(_multiplexer, _configuration, key);
        //    return true;
        //}

        public IActionResult BlogList1(string alias, int zone_id)
        {
            var model = _zoneRepository.GetZoneByTreeViewMinifies(2, CurrentLanguageCode, 0);
            var z = 0;
            if(zone_id > 0)
            {
                z = zone_id;
            }
            else
            {
                if(model != null)
                {
                    z = model.FirstOrDefault().Id;
                }
            }
            ViewBag.zone_id = z;
            
            return View(model);
        }
        public IActionResult RedirectAction(string alias)
        {
            alias = alias.Replace(".html", "");
            var zone_tar = _articleRepository.GetObjectByAlias(alias, CurrentLanguageCode);
            if (zone_tar != null)
            {
                ViewBag.ZoneId = zone_tar.ObjectId;
                ViewBag.Type = zone_tar.ObjectType;
                //ViewBag.Parent = zone_tar.ParentId;
            }
            return View();

        }

        public IActionResult BlogList2(string alias, int zoneId, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 5;
            //Get ra zone by Id
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            if (zoneId > 0)
            {
                var zone_target = _zoneRepository.GetZoneDetail(zoneId, CurrentLanguageCode);
                if (zone_target != null)
                {

                    ViewBag.ZoneTarget = zone_target;
                }
            }
            return View();
        }

        public IActionResult BlogDetail(string alias, int articleId)
        {
            ArticleDetail articleDetail = new ArticleDetail();
            List<ArticleMinify> model = new List<ArticleMinify>();
            try
            {
                var totalRow = 0;
                //zone_id,(int)TypeZone.Article, CurrentLanguageCode,"",1,7,out totalRow
                model = _articleRepository.GetArticlesInZoneId_Minify_FullFilter(0, 2, 0, 2, CurrentLanguageCode, "", 1, 7, out totalRow);
                articleDetail = _articleRepository.GetArticleDetail(articleId, CurrentLanguageCode);
                ViewBag.ActicaliType = model.Where(x => x.Id == articleDetail.Id);
            }
            catch (Exception ex)
            {

            }
            ViewBag.Detail = articleDetail;
            ViewBag.ListActical = model;
            return View();
        }
        public IActionResult FilterArticleByTag(string tag, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 10;
            var total = 0;
            var model = _articleRepository.GetArticlesSameTag(tag, CurrentLanguageCode, pageIndex, pageSize, out total);
            ViewBag.Total = total;
            ViewBag.Tag = tag;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            return View(model);
        }
        //ProductListInArticle
        [HttpPost]
        public IActionResult MoreBlogs(int zone_id, int type, string filter, int page_index, int page_size)
        {
            if (string.IsNullOrEmpty(filter))
                filter = "";
            return ViewComponent("MoreBlog", new { zone_id = zone_id, type = type, filter = filter, page_index = page_index, page_size = page_size });
        }
        [HttpPost]
        public IActionResult ProductsInArticle(string product_ids, int location_id)
        {
            return ViewComponent("ProductListInArticle", new { product_ids = product_ids, location_id = location_id });
        }
        public IActionResult GetListBlogByTypeArticle(int zone_id, int? pageIndex)
        {
            return ViewComponent("BlogPaging", new { zone_id = zone_id, pageIndex = pageIndex });
        }
    }
}