﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VietLab.Web.Services.Article.Repository;
using VietLab.Web.Services.Product.Repository;
using VietLab.Web.Services.Zone.Repository;
using Microsoft.AspNetCore.Mvc;

namespace VietLab.Web.Controllers
{
    public class CategoriesController : BaseController
    {
        private readonly IZoneRepository _zoneRepository;
        private readonly IProductRepository _productRepository;
        private readonly IArticleRepository _articleRepository;
        
        public CategoriesController(IZoneRepository zoneRepository, IProductRepository productRepository, IArticleRepository articleRepository)
        {
            _zoneRepository = zoneRepository;
            _productRepository = productRepository;
            _articleRepository = articleRepository;
        }

        public IActionResult CategoriesList1()
        {
            return View();
        }

        public IActionResult CategoriesList(int zoneId, int? pageIndex, int? pageSize)
        {
            var zone_parent = _zoneRepository.GetZoneByTreeViewMinifies(1, CurrentLanguageCode, zoneId).FirstOrDefault();
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 10;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            ViewBag.CateId = zoneId;
            ViewBag.ZoneParent = zone_parent;
            return View();
        }
        public IActionResult ProducResult(int zone_id, int Location_id, int PageIndex)
        {
            return ViewComponent("ProductMenu", new { zone_id = zone_id,Location_id = Location_id,PageIndex = PageIndex });
        }
    }
}