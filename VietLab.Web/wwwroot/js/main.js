        // ====================Scroll to top========================
        jQuery(document).ready(function($) {

            var visible = false;
            //Check to see if the window is top if not then display button
            $(window).scroll(function() {
                var scrollTop = $(this).scrollTop();
                if (!visible && scrollTop > 100) {
                    $(".scrollToTop").fadeIn();
                    visible = true;
                } else if (visible && scrollTop <= 100) {
                    $(".scrollToTop").fadeOut();
                    visible = false;
                }
            });
            //Click event to scroll to top
            $(".scrollToTop").click(function() {
                $("html, body").animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        });
        // ===============typeit=====================
        new TypeIt('.toppic', {
            speed: 60,
            breakLines: false,
            waitUntilVisible: true,
            lifeLike: true,
            cursor: false,
        }).go();
        $(".scrollToTop").click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        // <!--------------------wow------------->
        new WOW({
            boxClass: 'wow',
            mobile: false
        }).init();
        // ===============SLide=Top===================
        $(document).ready(function() {
            $('.slide').slick({
                autoplay: true,
                autoplaySpeed: 3000,
                arrows: false
            });

            $(".item-dot").click(function() {
                let slideIndex = $(this).attr('data-i');
                $('.slide').slick('slickGoTo', slideIndex);
                $('.item-dot').removeClass('acv');
                $('.item-dot').children('.cycle').removeClass('aatt');
                $(this).children('.cycle').addClass('aatt')
                $(this).addClass('acv')
            });

            $('.slide').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                if (currentSlide === parseInt(currentSlide, 10)) {
                    $('.item-dot').removeClass('acv');
                    $('.item-dot').children('.cycle').removeClass('aatt');
                    $('.item-dot[data-i=' + currentSlide + ']').addClass('acv');
                    $('.item-dot[data-i=' + currentSlide + ']').children('.cycle').addClass('aatt');
                };
            });
            $('.slide-next').click(function() {
                $(".slide").slick('slickNext');
            });
            $('.slide-pre').click(function() {
                $(".slide").slick('slickPrev');
            });
        });
        // ==============Slide=Customer=============
        $(document).ready(function() {
            $('.slide-customer').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                arrows: false,
                infinite: true,
                dots: false,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });
            $('.slide-next').click(function() {
                $(".slide-customer").slick('slickNext');
            });
            $('.slide-pre').click(function() {
                $(".slide-customer").slick('slickPrev');
            });
        });
        // ============Slide=partner==============
        $(document).ready(function() {
            $('.slide-partner').slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                arrows: false,
                infinite: true,
                dots: false,

                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }]
            });
        });