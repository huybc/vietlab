﻿
R.Contact = {
    Init: function () {
        R.Contact.RegisterEvent();
    },
    RegisterEvent: function () {
        $('._picking_location').off('click').on('click', function () {
            console.log(1);
            var loc_id = $(this).data('id');
            var _post = $(this).data('position');
            //Dong tat ca cac binding
            $('._binding_departments').hide();
            R.Contact.ShowDepartment(loc_id, _post)
        })
        $('#form-lien-he').off('submit').on('submit', function () {
            var name = $('#name-input').val();
            var phone = $('#phone-input').val();
            var email = $('#mail-input').val();
            var add = $('#address').val();
            var company = $('#company-input').val();
            var form = $("#form-lien-he");
            var checkedValue = form.find("input[name=tinhthanh]:checked").val();
            var type = $('#form-lien-he').data('type')
            var nd = $('#mess-input').val();
            var kyc = $('#yeucau').val();

            var KhachHang = "";
            if (parseInt(type) == 3) {
                KhachHang = "Liên hệ ViietLap"
            }
            else if (parseInt(type) == 5) {
                KhachHang = "Liên hệ Tucky"
            }
            else {
                KhachHang ="order ViietLap"
            }

            var body = '<div>Tên khách hàng : ' + name + '</div>' +
                '<div>Số điện thoại : ' + phone + '</div>' +
                '<div>Khách yêu cầu : ' + kyc + '</div>' +
                '<div>Nội dung khách gửi : ' + nd + '</div>';
            var param = {
                type: KhachHang,
                body: body,
                KhuVuc: checkedValue

            }
            var yc = "";
            if (parseInt(type) == 3) {
                yc = "Liên hệ Vietlab"
            }
            else {
                yc = "Liên hệ mua hàng ViietLap"
            }

            var params = {
                Name: name,
                Phone: phone,
                Address: add,
                Email: email,
                Company: company,
                KhuVuc: checkedValue,
                Title: yc,
                Content: nd,
                Type: type,
                Source: 'web',

            }

            R.Contact.SendContact(params, param);
            return false;
        })
    },
    ShowDepartment: function (id, post) {
        var url = "/Contact/GetDepartments"
        var params = {
            loc_id: id
        }
        $.post(url, params, function (response) {
            $('._binding_departments').each(function (element) {
                if ($(this).data('position') == post) {
                    $(this).css('display', 'block');
                    $(this).html('').html(response);
                }
            })
        })
    },
    SendContact: function (params, param) {
        var url = "/Extra/CreateServiceTicket";
        $.post(url, params, function (response) {
            alert("Thư của bạn đã được gửi chúng tôi sẽ liên hệ với bạn sớm nhất!")
            //$('#modal-xn').modal('show');
            R.Contact.CloseModalAndClearText();
            R.Contact.SendMail(param);
            //return false;
        })
    },
    SendMail: function (param) {
        var urlSenMail = "/Extra/SendMail"
        $.post(urlSenMail, param, function (response) {
            console.log(response)
            //$('#modal-xn').modal('show');
            R.Contact.CloseModalAndClearText();
            //return false;
        })
    },
    CloseModalAndClearText: function () {
        $('#name-input').val('');
        $('#phone-input').val('');
        $('#mail-input').val('');
        $('#address').val('');
        $('#mess-input').val('');
    }
}
$(function () {
    R.Contact.Init()
})