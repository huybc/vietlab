﻿R.View = {
    Init: function () {
        R.View.RegisterEvent();
    },
    RegisterEvent: function () {
        $('.see-more1').off('click').on('click', function () {
            $('.list-chucnag').removeClass('styleover');
            var id = $(this).data('id');
            var ListZone = '.ListZone' + id;
            var AddClass = '.AddClass' + id;
            $(ListZone).removeClass('textHide');
            $(AddClass).addClass('textHide');
        })
        $('.see-more2').off('click').on('click', function () {
            $('.list-chucnag').removeClass('styleover');
            var id = $(this).data('id');
            var ListZone = '.ListZone' + id;
            var AddClass = '.AddClass' + id;
            $(ListZone).addClass('textHide');
            $(AddClass).removeClass('textHide');
        })
    }

}
R.View.Init();