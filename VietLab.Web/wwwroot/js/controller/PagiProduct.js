﻿$('.ProductMore').twbsPagination({
    totalPages: Math.ceil((parseInt($('.set-total').data('total'))) / 9),
    first: 'Đầu',
    prev: 'Sau',
    next: 'Trước',
    last: 'Cuối',
    onPageClick: function (evt, page) {
        var url = "/Product/ViewMoreProductList";
        var el = $('#_bindingHtmlProduct');
        var zoneid = parseInt($('.ProductMore').data('zoneid'));
        var locaid = parseInt($('.ProductMore').data('Loca'));
        params = {
            zoneId: zoneid,
            location_id: locaid,
            PageIndex: page
        }
        $.post(url, params, function (response) {
            el.html('');
            el.replaceWith(response);
        })

    }

});