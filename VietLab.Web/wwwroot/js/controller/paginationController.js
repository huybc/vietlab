﻿
$('.clicknextsp').twbsPagination({
    totalPages: Math.ceil((parseInt($('#total').val())) / 6),
    first: 'Đầu',
    prev: 'Sau',
    next: 'Trước',
    last:'Cuối',
    onPageClick: function (evt, page) {
        var url = "/Categories/ProducResult";
        var el = $('#_BindinHtml');
        var zoneid = parseInt($('#IdZoneJs').val());
        var locaid = parseInt($('#IdZoneJs').data('local'));
        params = {
            zone_id: zoneid,
            Location_id: locaid,
            PageIndex: page
        }
        $.post(url, params, function (response) {
            el.html('');
            el.replaceWith(response);
        })
       
    }
    
});
