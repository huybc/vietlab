﻿R.Blog = {
    Init: function () {
        R.Blog.RegisterEvent();
        
    },
    RegisterEvent: function () {
      
    },
  
}
$(function () {
    R.Blog.Init();
})
$('.clicknextblog1').twbsPagination({
    totalPages: Math.ceil((parseInt($('.totalrow1').val())) / 7),
    first: 'Đầu',
    prev: 'Sau',
    next: 'Trước',
    last: 'Cuối',
    onPageClick: function (evt, page) {
        var url = "/Blog/GetListBlogByTypeArticle";
        var zoneid = parseInt($('.clicknextblog1').data('zoneid'));
        var el = $('#_BindinHtmlblog');
        params = {
            zone_id: zoneid,
            pageIndex: page,
        }
        $.post(url, params, function (response) {
            el.html('');
            el.replaceWith(response);
        })
       
    }
});
