﻿using Enterbuy.Services.Common.Models;
using System;
using System.Threading.Tasks;
using Enterbuy.Core.Statics;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;


namespace Enterbuy.Services.Home.Services
{
    public class BannerHomePageService : IBannerHomePageService
    {
        private readonly IBannerAdsDao _bannerAdsDao;

        public BannerHomePageService(IBannerAdsDao bannerAdsDao)
        {
            _bannerAdsDao = bannerAdsDao;
        }

        public async Task<BannerHomePage> GetBannerInBodyFirst(string langCode)
        {
            var bannerAdsViewModel =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.BannerTrangchuBodyTrenTrai1);
            var result = JsonConvert.DeserializeObject<BannerHomePage>(bannerAdsViewModel.MetaData);            
            return result;

        }

        public async Task<BannerHomePage> GetBannerInBodySecond(string langCode)
        {
            var bannerAdsViewModel =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.BannerTrangchuBodyTrenTrai1);
            var result = JsonConvert.DeserializeObject<BannerHomePage>(bannerAdsViewModel.MetaData);
            return result;
        }

        public async Task<BannerHomePage> GetBannerInBodyThird(string langCode)
        {
            var bannerAdsViewModel =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.BannerTrangchuBodyTrenTrai1);
            var result = JsonConvert.DeserializeObject<BannerHomePage>(bannerAdsViewModel.MetaData);
            return result;
        }


        public async Task<BannerHomePage> BannerTrangchuTrencung(string langCode)
        {
            var bannerAdsViewModel =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.Banner_Trangchu_Trencung);
            var result = JsonConvert.DeserializeObject<BannerHomePage>(bannerAdsViewModel.MetaData);
            return result;
        }

        public async Task<List<BannerHomePage>> StemMenu(string langCode)
        {
            var bannerAdsViewModel =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.Stem_Menu);
            var result = JsonConvert.DeserializeObject<List<BannerHomePage>>(bannerAdsViewModel.MetaData);
            return result;
        }
        public async Task<BannerHomePage> BannerTrangchuNgang2(string langCode)
        {
            var bannerAdsViewModel =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.Banner_Trangchu_Ngang2);
            var result = JsonConvert.DeserializeObject<BannerHomePage>(bannerAdsViewModel.MetaData);
            return result;
        }
        public async Task<BannerHomePage> BannerHethongdaily(string langCode)
        {
            var bannerAdsViewModel =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.Banner_Hethongdaily);
            var result = JsonConvert.DeserializeObject<BannerHomePage>(bannerAdsViewModel.MetaData);
            return result;
        }
        public async Task<BannerHomePage> BannerKhuyenmaiTrangchu(string langCode)
        {
            var bannerAdsViewModel =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.Banner_Khuyenmai_Trangchu);
            var result = JsonConvert.DeserializeObject<BannerHomePage>(bannerAdsViewModel.MetaData);
            return result;
        }
        public async Task<List<BannerHomePage>> SlideBody(string langCode)
        {
            var bannerAdsViewModel =
                await _bannerAdsDao.GetBannerAdsByCode(langCode, BannerLocationStatic.Slide_Body);
            var result = JsonConvert.DeserializeObject<List<BannerHomePage>>(bannerAdsViewModel.MetaData);
            return result;
        }
    }
}