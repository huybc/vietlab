﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Services.Home.Services
{
    public interface IProductSaleInMonthService
    {
        List<ProductSaleInMonth> GetProductInFlashSale(int fSaleId, int locationId, string lanngCode, int pageIndex,
            int pageSize, out int total);
        Task<List<DiscountProgram>> GetFlashSaleByTime(DateTime time);
    }
}
