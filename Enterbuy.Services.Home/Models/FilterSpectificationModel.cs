﻿namespace Enterbuy.Services.Home.Models
{
    public class FilterSpectificationModel
    {
        public int SpectificationId { get; set; }
        public string Value { get; set; }
    }
}