﻿using System.Collections.Generic;

namespace Enterbuy.Services.Home.Models
{
    public class FilterProductBySpectificationModel
    {
        public int ParentId { get; set; }
        public string LangCode { get; set; }
        public int LocationId { get; set; }
        public int ManufactureId { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public int SortPrice { get; set; }
        public int SortRate { get; set; }
        public string ColorCode { get; set; } = "";
        public List<FilterSpectificationModel> Filter { get; set; }
        public string FilterText { get; set; } = "";
        public int MaterialType { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}