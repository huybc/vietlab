﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using System.Linq;

namespace Enterbuy.Services.Home.Components
{
    public class NewsHomePageViewComponent : ViewComponent
    {
        private readonly IArticleDao _articleDao;

        public NewsHomePageViewComponent(IArticleDao articleDao)
        {
            _articleDao = articleDao;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var newsShowHome = _articleDao.GetArticlesInZoneId_Minify_AddFilterHot(0, (int)TypeZone.AllButProduct, 1, "vi-VN", "", 1, 10, out _);

           

            return View(newsShowHome);
        }
    }
}