﻿using Enterbuy.Services.Home.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.Services.Home.Components
{
    public class CategoryHomePageViewComponent : ViewComponent
    {
        private readonly IBannerHomePageService _bannerHomePageService;

        public CategoryHomePageViewComponent(IBannerHomePageService bannerHomePageService)
        {
            _bannerHomePageService = bannerHomePageService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await _bannerHomePageService.StemMenu("vi-VN");
            return View(result);
        }
    }
}