﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Services.Home.Services;
using Utils;

namespace Enterbuy.Services.Home.Components
{
    public class FirstBannerHomeViewComponent : ViewComponent
    {
        private readonly IBannerHomePageService _bannerHomePageService;

        public FirstBannerHomeViewComponent(IBannerHomePageService bannerHomePageService)
        {
            _bannerHomePageService = bannerHomePageService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result =  await _bannerHomePageService.GetBannerInBodyFirst("vi-VN");
            result.Image = UIHelper.StoreFilePath(result.Image, false);
            return View(result);
        }
    }
}