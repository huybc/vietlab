﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Services.Home.Models;

namespace Enterbuy.Services.Home.Components
{
    public class FirstProductCategoryViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneDao;

        public FirstProductCategoryViewComponent(IZoneDao zoneDao)
        {
            _zoneDao = zoneDao;
        }

        public async Task<IViewComponentResult> InvokeAsync(List<ZoneByTreeViewMinify> listZoneTypeRegion, int order)
        {
            var result = listZoneTypeRegion.Where(r => r.ParentId == 0).OrderBy(r => r.SortOrder).ToList();
            var resultListProductRegion = new List<ZoneByTreeViewMinify>();

            if (result.Any(x => x.SortOrder == order))
                resultListProductRegion = listZoneTypeRegion.Where(x => x.ParentId == result.FirstOrDefault(d => d.SortOrder == order).Id).ToList();


            ViewBag.RegionParent = result.Any(x=>x.SortOrder== order) ? result.FirstOrDefault(x=>x.SortOrder == order) : new ZoneByTreeViewMinify();
          

            ViewBag.Banner = string.Empty;
            ViewBag.Slide = 2;
            return View(resultListProductRegion);
        }
    }
}
