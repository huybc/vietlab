﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Services.Home.Services;
using Utils;

namespace Enterbuy.Services.Home.Components
{
    public class SecondBannerViewComponent : ViewComponent
    {
        private readonly IBannerHomePageService _bannerHomePageService;

        public SecondBannerViewComponent(IBannerHomePageService bannerHomePageService)
        {
            _bannerHomePageService = bannerHomePageService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await _bannerHomePageService.GetBannerInBodySecond("vi-VN");
            result.Image = UIHelper.StoreFilePath(result.Image, false);
            return View(result);
        }
    }
}