﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Core.Statics;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Services.Common.Models;
using Newtonsoft.Json;

namespace Enterbuy.Services.Home.Components
{
    public class ServiceFooterViewComponent : ViewComponent
    {
        private readonly IBannerAdsDao _bannerAdsDao;

        public ServiceFooterViewComponent(IBannerAdsDao bannerAdsDao)
        {
            _bannerAdsDao = bannerAdsDao;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var bannerAdsDao = await _bannerAdsDao.GetBannerAdsByCode("vi-VN", BannerLocationStatic.StemChantrang);
            var result = JsonConvert.DeserializeObject<List<BannerHomePage>>(bannerAdsDao.MetaData);

            return View(result);
        }
    }
}