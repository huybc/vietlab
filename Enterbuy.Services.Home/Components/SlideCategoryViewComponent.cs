﻿using Enterbuy.Services.Home.Services;
using Enterbuy.Statics.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.Services.Home.Components
{
    public class SlideCategoryViewComponent : ViewComponent
    {
        private readonly IBannerHomePageService _bannerHomePageService;

        public SlideCategoryViewComponent(IBannerHomePageService bannerHomePageService)
        {
            _bannerHomePageService = bannerHomePageService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await _bannerHomePageService.SlideBody("vi-VN");
            return View(result);
        }
    }
}