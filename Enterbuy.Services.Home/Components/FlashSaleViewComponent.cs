﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Services.Home.Services;
using Enterbuy.Statics.Utilities;

namespace Enterbuy.Services.Home.Components
{
    public class FlashSaleViewComponent : ViewComponent
    {

        private readonly IZoneDao _zoneDao;
        private readonly IBannerHomePageService _bannerHomePageService;
        public FlashSaleViewComponent(IZoneDao zoneDao, IBannerHomePageService bannerHomePageService)
        {
            _zoneDao = zoneDao;
            _bannerHomePageService = bannerHomePageService;
        }

        public async Task<IViewComponentResult> InvokeAsync(List<ZoneByTreeViewMinify> listZoneTypeRegion, int order)
        {
            var resultListProductRegion = new List<ZoneByTreeViewMinify>();
            var first = new ZoneByTreeViewMinify();
            if (listZoneTypeRegion != null && listZoneTypeRegion.Count > 0)
            {
                var result = listZoneTypeRegion.Where(r => r.ParentId == 0).OrderBy(r => r.SortOrder).ToList();
                if (result.Any(x=>x.SortOrder == order))
                    resultListProductRegion = listZoneTypeRegion.Where(x => x.ParentId == result.FirstOrDefault(d => d.SortOrder == order).Id).ToList();

                first = result.Any(x => x.SortOrder == order) ? result.FirstOrDefault(x=>x.SortOrder == order)  : new ZoneByTreeViewMinify();
            }
            var resultBanner = await _bannerHomePageService.BannerKhuyenmaiTrangchu("vi-VN");
            ViewBag.RegionParent = first;
            ViewBag.Banner = resultBanner.Image;
            ViewBag.Slide = 2;
            return View(resultListProductRegion);

        }
    }
}