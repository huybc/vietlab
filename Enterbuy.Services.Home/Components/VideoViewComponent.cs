﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;

namespace Enterbuy.Services.Home.Components
{
    public class VideoViewComponent : ViewComponent
    {
        private readonly IArticleDao _articleDao;

        public VideoViewComponent(IArticleDao articleDao)
        {
            _articleDao = articleDao;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var listNewsVideo = _articleDao.GetArticlesInZoneId_Minify_FullFilter(0, (int)TypeZone.AllButProduct, (int)TypeArticle.Video, 2, "vi-VN", "", 1, 6, out _);

            return View(listNewsVideo);
        }
    }
}