﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;


using Microsoft.AspNetCore.Http;
using Enterbuy.Data.SqlServer.ModelDto;
using System.Threading.Tasks;

namespace Enterbuy.Services.Home.Components
{
    public class SwitchRegionViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneDao;
        private string _currentLanguage;
        private string _currentLanguageCode;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        private string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                }

                return _currentLanguageCode;
            }


        }
        public SwitchRegionViewComponent(IZoneDao zoneDao)
        {
            _zoneDao = zoneDao;
        }
        public async Task<IViewComponentResult> InvokeAsync(int region_id)
        {

            var resultListProductRegion = new ZoneDetail();
            resultListProductRegion = await _zoneDao.GetZoneInfo(region_id, "vi-VN");
            ViewBag.RegionParent = resultListProductRegion;
            //try
            //{

            //    var base_uri_culture = "/" + CurrentLanguageCode;
            //    var domain = HttpContext.Request.Host.ToString();
            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}

            //cookieUtility.SetCookieDefault();
            //var location_id = int.Parse(HttpContextAccessor.HttpContext.Request.Cookies["_LocationId"]);
            //var location_name = HttpContextAccessor.HttpContext.Request.Cookies["_LocationName"];

            return View();
        }
    }
}
