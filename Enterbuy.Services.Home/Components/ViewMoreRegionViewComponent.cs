﻿
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JanHome.Web.ViewComponents
{
    public class ViewMoreRegionViewComponent : ViewComponent
    {
 
        private readonly IProductDao _productRepository;
        private readonly Utils.ICookieLocationUtility _cookieLocation;
        public ViewMoreRegionViewComponent(IProductDao productRepository, Utils.ICookieLocationUtility cookieLocation)
        {
            _productRepository = productRepository;
            _cookieLocation = cookieLocation;
        }

        public IViewComponentResult Invoke(int zone_parent_id, int locationId, int skip, int size)
        {
            var location = _cookieLocation.SetCookieDefault();

            var total = size;
            ViewBag.Total = size;
            ViewBag.Id = zone_parent_id;
            var model = _productRepository.GetProductsInRegionByZoneParentIdSkipping(zone_parent_id, Utils.Utility.DefaultLang, location.LocationId, skip, size);
            return View(model);
        }
    }
}
