﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.WEB.Views.AgentRegistration.Components
{
    public class AgentRegistrationFormViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}