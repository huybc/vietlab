﻿using Enterbuy.Data.Sql;
using Enterbuy.Data.SqlServer;
using Enterbuy.Data.SqlServer.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.DbContexts;
using Enterbuy.Services.Common.Services;
using Enterbuy.Services.Common.Services.Interfaces;
using Enterbuy.Services.Product.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Enterbuy.WEB.Registrations
{
    public class DependencyRegistrar
    {
        public static void RegisterTypes(IServiceCollection services)
        {
            services.AddTransient<EnterbuyDbContext>();

            services.AddTransient<IDatabaseFactory, DatabaseFactory>();

            services.AddTransient<IProductDao, ProductDao>();

            services.AddTransient<IProductService, ProductService>();

            services.AddTransient<IBannerAdsDao, BannerAdsDao>();

            services.AddTransient<IBannerAdsService, BannerAdsService>();
        }
    }
}