﻿using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Services.Contact.Services;
using System;

namespace Enterbuy.Services.AgentRegistration.Controllers
{
    public class AgentRegistrationController : BaseController
    {
        private readonly IContactService _contactService;
        private class Response
        {
            public string Code { get; set; }
            public string Mes { get; set; }
        }
        public AgentRegistrationController(IContactService contactService)
        {
            _contactService = contactService;
        }

        public IActionResult Index()
        {
            ViewData["IsExpand"] = false;
            return View();
        }

        [HttpPost]
        public IActionResult UpdateFeedBack(ContactViewModel contactViewModel)
        {
            ViewData["IsExpand"] = false;
            Response response = new Response();
            int result = 0;
            try
            {
                if (contactViewModel.AgentName != null)
                {
                    foreach (var item in contactViewModel.AgentName)
                    {
                        contactViewModel.Note += ", " + item;
                    }
                }
                contactViewModel.Type = (int)TypeContact.RegisterAgent;
                contactViewModel.Status = 1;
                result = _contactService.Insert(contactViewModel);
                if (result > 0)
                {
                    response.Code = "00";
                    response.Mes = "Thành công.";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Ok(response);
        }
    }
}