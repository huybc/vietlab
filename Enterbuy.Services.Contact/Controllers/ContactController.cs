﻿using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Http.Controllers;
using Enterbuy.Services.Contact.Services;
using Enterbuy.Services.Contact.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Enterbuy.Services.Contact.Controllers
{
    public class ContactController : BaseController
    {
        private readonly IContactService _contactService;
        private readonly ILocationService _locationService;

        private class Response
        {
            public string Code { get; set; }
            public string Mes { get; set; }
        }

        public ContactController(IContactService contactService, ILocationService locationService)
        {
            _contactService = contactService;
            _locationService = locationService;
        }
       
        [Route("lien-he/{alias}")]
        public IActionResult Index()
        {
            ViewData["IsExpand"] = false;
            return View();
        }

        [HttpPost]
        public IActionResult UpdateFeedBack(ContactViewModel contactViewModel)
        {
            ViewData["IsExpand"] = false;
            int result = 0;
            Response response = new Response();
            try
            {
                contactViewModel.Type = (int)TypeContact.Guarantee;
                contactViewModel.Status = 1;
                result = _contactService.Insert(contactViewModel);
                if(result > 0)
                {
                    response.Code = "00";
                    response.Mes = "Thành công.";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Ok(response);
        }
        [HttpPost]
        public IActionResult UpdateAdvisory(ContactViewModel contactViewModel)
        {
            ViewData["IsExpand"] = false;
            int result = 0;
            Response response = new Response();
            try
            {
                contactViewModel.Type = (int)TypeContact.RegisterAdvisory;
                contactViewModel.Status = 1;
                result = _contactService.Insert(contactViewModel);
                if (result > 0)
                {
                    response.Code = "00";
                    response.Mes = "Thành công.";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Ok(response);
        }
        [HttpPost]
        public IActionResult NearLocation(StoreViewModel storeViewModel)
        {
            List<StoreResponse> storeViewModels = new List<StoreResponse>();
            int total = 0;
            try
            {
                storeViewModels = _locationService.GetNearLocation(storeViewModel.Longitude, storeViewModel.Latitude, storeViewModel.Distance, storeViewModel.LanguageCode, storeViewModel.SortOrder, out total);
            }
            catch (Exception ex)
            {

            }
            return Ok(storeViewModels);
        }
        [HttpPost]
        public IActionResult CreateServiceTicket(ContactViewModel contactViewModel)
        {
            contactViewModel.Type = (int)TypeContact.Guarantee;
            contactViewModel.Status = 1;
            var result = _contactService.Insert(contactViewModel);


            return Ok(result);
        }
    }
}
