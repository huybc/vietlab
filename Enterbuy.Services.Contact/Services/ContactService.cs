﻿using System;
using System.Collections.Generic;
using System.Text;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Services.Contact.Services
{
    public class ContactService : IContactService
    {
        private readonly IContactDao _contactDao;
        public ContactService(IContactDao contactDao)
        {
            _contactDao = contactDao;
        }
        public int Insert(ContactViewModel contactViewModel)
        {
            int result = 0;
            try
            {
                result = _contactDao.Insert(contactViewModel);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
    }
}
