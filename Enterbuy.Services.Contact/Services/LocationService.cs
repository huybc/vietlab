﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Services.Contact.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Contact.Services
{
    public class LocationService : ILocationService
    {
        private readonly ILocationDao _locationDao;
        public LocationService(ILocationDao locationDao)
        {
            _locationDao = locationDao;
        }
        public List<StoreResponse> GetNearLocation(float Longitude, float Latitude, int distance, string langCode, int sortOrder, out int totalRows)
        {
            List<StoreResponse> storeResponses = new List<StoreResponse>();
            totalRows = 0;
            try
            {
                storeResponses = _locationDao.GetNearLocation(Longitude, Latitude, distance, langCode, sortOrder, out totalRows);
            }
            catch (Exception ex)
            {

            }
            return storeResponses;
        }
    }
}
