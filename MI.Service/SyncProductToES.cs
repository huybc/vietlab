﻿using MI.Bo.Bussiness;
using MI.ES;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Service
{
    public class SyncProductToES
    {
        //public static List<string> SplitStr(List<string> SplitString)
        //{
        //    List<string> lst = new List<string>();

        //    foreach (var item in SplitString)
        //    {
        //        var str = item.ToLower();
        //        var split = str.Split(" ");
        //        var split2 = Utils.Utility.TiengVietKhongDau(str).Split(" ");
        //        for (int i = 0; i < split.Length; i++)
        //        {
        //            lst.Add(string.Join(" ", split.Skip(i).Take(split.Length)));
        //            lst.Add(string.Join(" ", split2.Skip(i).Take(split.Length)));
        //        }
        //    }
        //    return lst.Distinct().ToList();
        //}

        public static void Run()
        {
            ProductBCL productBCL = new ProductBCL();
            ZoneBCL categoryBCL = new ZoneBCL();
            ArticleBCL articleBCL = new ArticleBCL();
            var product = productBCL.FindAll(x => x.Status == 1, x => x.ProductInLanguage);

            

            var products = product.Select(x => new ProductES
            {
                Id = x.Id,
                Code = x.Code,
                Avatar = x.Avatar,
                Price = Utils.Utility.ConvertCurentce(x.Price.ToString()),
                DiscountPrice = Utils.Utility.ConvertCurentce(x.DiscountPrice.ToString()),
                Unit = String.IsNullOrEmpty(x.Unit) ? "/mm2" : "/" + x.Unit,
                Info = x.ProductInLanguage.Select(d => new InfoES { Lang = d.LanguageCode.Trim(), Name = d.Title, Url = d.Url + ".html" }).ToList(),
                Suggest = new CompletionField()
                {
                    Input = Utils.Utility.SplitStr(x.ProductInLanguage.Select(a => a.Title).ToList())
                }
            }).ToList();

            //var categoryES = category.Select(x => new ProductES
            //{
            //    Id = x.Id,
            //    Code = "",
            //    Avatar = x.Avatar,
            //    Price = "",
            //    DiscountPrice = "",
            //    Unit = "",
            //    Info = x.ZoneInLanguage.Select(d => new InfoES { Lang = d.LanguageCode.Trim(), Name = d.Name, Url = d.Url }).ToList(),
            //    Suggest = new CompletionField()
            //    {
            //        Input = Utils.Utility.SplitStr(x.ZoneInLanguage.Select(a => a.Name).ToList())
            //    }
            //}).ToList();

            //var article = articles.Select(x => new ProductES
            //{
            //    Id = x.Id,
            //    Code = "",
            //    Avatar = x.Avatar,
            //    Price = "",
            //    DiscountPrice = "",
            //    Unit = "",
            //    Info = x.ArticleInLanguage.Select(d => new InfoES { Lang = d.LanguageCode.Trim(), Name = d.Title, Url = d.Url + ".html" }).ToList(),
            //    Suggest = new CompletionField()
            //    {
            //        Input = Utils.Utility.SplitStr(x.ArticleInLanguage.Select(a => a.Title).ToList())
            //    }
            //}).ToList();

            //products = products.Union(categoryES).Union(article).ToList();

            bool isCreated = MI.ES.BCLES.AutocompleteService.CreateIndexAsync().Result;

            if (isCreated)
            {
                var data = MI.ES.BCLES.AutocompleteService.IndexAsync(products);
            }

        }
    }
}
